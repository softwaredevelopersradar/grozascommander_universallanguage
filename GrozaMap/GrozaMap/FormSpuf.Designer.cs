﻿namespace GrozaMap
{
    partial class FormSpuf
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label4 = new System.Windows.Forms.Label();
            this.tbR2 = new System.Windows.Forms.TextBox();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.comboBox2 = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.button1 = new System.Windows.Forms.Button();
            this.bClear = new System.Windows.Forms.Button();
            this.bAccept = new System.Windows.Forms.Button();
            this.lCenterLSR = new System.Windows.Forms.Label();
            this.cbCenterLSR = new System.Windows.Forms.ComboBox();
            this.tbLRad = new System.Windows.Forms.TextBox();
            this.lLRad = new System.Windows.Forms.Label();
            this.tbBRad = new System.Windows.Forms.TextBox();
            this.lBRad = new System.Windows.Forms.Label();
            this.cbChooseSC = new System.Windows.Forms.ComboBox();
            this.lChooseSC = new System.Windows.Forms.Label();
            this.tbYRect = new System.Windows.Forms.TextBox();
            this.lYRect = new System.Windows.Forms.Label();
            this.tbXRect = new System.Windows.Forms.TextBox();
            this.lXRect = new System.Windows.Forms.Label();
            this.gbRect = new System.Windows.Forms.GroupBox();
            this.tbR1 = new System.Windows.Forms.TextBox();
            this.lOwnHeight = new System.Windows.Forms.Label();
            this.tbOwnHeight = new System.Windows.Forms.TextBox();
            this.pCoordPoint = new System.Windows.Forms.Panel();
            this.gbRad = new System.Windows.Forms.GroupBox();
            this.lDistSightRange = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.tbOpponentAntenna = new System.Windows.Forms.TextBox();
            this.label17 = new System.Windows.Forms.Label();
            this.tbHAnt = new System.Windows.Forms.TextBox();
            this.chbS = new System.Windows.Forms.CheckBox();
            this.chbJ = new System.Windows.Forms.CheckBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.gbRect.SuspendLayout();
            this.pCoordPoint.SuspendLayout();
            this.gbRad.SuspendLayout();
            this.SuspendLayout();
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(308, 172);
            this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(188, 16);
            this.label4.TabIndex = 204;
            this.label4.Text = "Radius of the jamming zone, m";
            // 
            // tbR2
            // 
            this.tbR2.BackColor = System.Drawing.SystemColors.HighlightText;
            this.tbR2.Location = new System.Drawing.Point(539, 167);
            this.tbR2.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.tbR2.Name = "tbR2";
            this.tbR2.Size = new System.Drawing.Size(80, 22);
            this.tbR2.TabIndex = 203;
            this.tbR2.TextChanged += new System.EventHandler(this.tbR2_TextChanged);
            // 
            // textBox2
            // 
            this.textBox2.BackColor = System.Drawing.SystemColors.HighlightText;
            this.textBox2.Location = new System.Drawing.Point(541, 5);
            this.textBox2.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new System.Drawing.Size(73, 22);
            this.textBox2.TabIndex = 202;
            this.textBox2.Text = "8";
            // 
            // comboBox2
            // 
            this.comboBox2.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox2.FormattingEnabled = true;
            this.comboBox2.Items.AddRange(new object[] {
            "1.5",
            "7"});
            this.comboBox2.Location = new System.Drawing.Point(541, 33);
            this.comboBox2.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.comboBox2.Name = "comboBox2";
            this.comboBox2.Size = new System.Drawing.Size(73, 24);
            this.comboBox2.TabIndex = 201;
            this.comboBox2.SelectedIndexChanged += new System.EventHandler(this.comboBox2_SelectedIndexChanged);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(273, 34);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(146, 16);
            this.label3.TabIndex = 200;
            this.label3.Text = "Gain.....................................";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(272, 9);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(147, 16);
            this.label2.TabIndex = 199;
            this.label2.Text = "Power, W (0.1-100) .........";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(276, 73);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(183, 16);
            this.label1.TabIndex = 198;
            this.label1.Text = "Spoofing coefficient, dB (7-15)";
            // 
            // textBox1
            // 
            this.textBox1.BackColor = System.Drawing.SystemColors.HighlightText;
            this.textBox1.Location = new System.Drawing.Point(541, 66);
            this.textBox1.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(73, 22);
            this.textBox1.TabIndex = 197;
            this.textBox1.Text = "10";
            // 
            // button1
            // 
            this.button1.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.button1.Location = new System.Drawing.Point(253, 197);
            this.button1.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(136, 28);
            this.button1.TabIndex = 196;
            this.button1.Text = "Center of the zone";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // bClear
            // 
            this.bClear.Location = new System.Drawing.Point(518, 197);
            this.bClear.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.bClear.Name = "bClear";
            this.bClear.Size = new System.Drawing.Size(101, 28);
            this.bClear.TabIndex = 195;
            this.bClear.Text = "Clear";
            this.bClear.UseVisualStyleBackColor = true;
            this.bClear.Click += new System.EventHandler(this.bClear_Click);
            // 
            // bAccept
            // 
            this.bAccept.Location = new System.Drawing.Point(401, 197);
            this.bAccept.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.bAccept.Name = "bAccept";
            this.bAccept.Size = new System.Drawing.Size(109, 28);
            this.bAccept.TabIndex = 194;
            this.bAccept.Text = "Accept";
            this.bAccept.UseVisualStyleBackColor = true;
            this.bAccept.Click += new System.EventHandler(this.bAccept_Click);
            // 
            // lCenterLSR
            // 
            this.lCenterLSR.AutoSize = true;
            this.lCenterLSR.Location = new System.Drawing.Point(3, 10);
            this.lCenterLSR.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lCenterLSR.Name = "lCenterLSR";
            this.lCenterLSR.Size = new System.Drawing.Size(113, 16);
            this.lCenterLSR.TabIndex = 188;
            this.lCenterLSR.Text = "Center of the zone";
            // 
            // cbCenterLSR
            // 
            this.cbCenterLSR.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbCenterLSR.FormattingEnabled = true;
            this.cbCenterLSR.Items.AddRange(new object[] {
            "X,Y",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            ""});
            this.cbCenterLSR.Location = new System.Drawing.Point(152, 4);
            this.cbCenterLSR.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.cbCenterLSR.Name = "cbCenterLSR";
            this.cbCenterLSR.Size = new System.Drawing.Size(85, 24);
            this.cbCenterLSR.TabIndex = 187;
            this.cbCenterLSR.SelectedIndexChanged += new System.EventHandler(this.cbCenterLSR_SelectedIndexChanged);
            // 
            // tbLRad
            // 
            this.tbLRad.Location = new System.Drawing.Point(104, 44);
            this.tbLRad.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.tbLRad.MaxLength = 10;
            this.tbLRad.Name = "tbLRad";
            this.tbLRad.Size = new System.Drawing.Size(99, 22);
            this.tbLRad.TabIndex = 5;
            // 
            // lLRad
            // 
            this.lLRad.AutoSize = true;
            this.lLRad.Location = new System.Drawing.Point(5, 50);
            this.lLRad.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lLRad.Name = "lLRad";
            this.lLRad.Size = new System.Drawing.Size(101, 16);
            this.lLRad.TabIndex = 6;
            this.lLRad.Text = "L, рад...................";
            // 
            // tbBRad
            // 
            this.tbBRad.Location = new System.Drawing.Point(104, 16);
            this.tbBRad.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.tbBRad.MaxLength = 10;
            this.tbBRad.Name = "tbBRad";
            this.tbBRad.Size = new System.Drawing.Size(99, 22);
            this.tbBRad.TabIndex = 4;
            // 
            // lBRad
            // 
            this.lBRad.AutoSize = true;
            this.lBRad.Location = new System.Drawing.Point(5, 22);
            this.lBRad.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lBRad.Name = "lBRad";
            this.lBRad.Size = new System.Drawing.Size(103, 16);
            this.lBRad.TabIndex = 3;
            this.lBRad.Text = "B, рад...................";
            // 
            // cbChooseSC
            // 
            this.cbChooseSC.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbChooseSC.DropDownWidth = 150;
            this.cbChooseSC.FormattingEnabled = true;
            this.cbChooseSC.Items.AddRange(new object[] {
            "Метры на мест-ти",
            "Радианы",
            "Градусы",
            "Град, мин, сек"});
            this.cbChooseSC.Location = new System.Drawing.Point(177, 386);
            this.cbChooseSC.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.cbChooseSC.Name = "cbChooseSC";
            this.cbChooseSC.Size = new System.Drawing.Size(163, 24);
            this.cbChooseSC.TabIndex = 11;
            this.cbChooseSC.Visible = false;
            // 
            // lChooseSC
            // 
            this.lChooseSC.AutoSize = true;
            this.lChooseSC.Location = new System.Drawing.Point(95, 412);
            this.lChooseSC.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lChooseSC.Name = "lChooseSC";
            this.lChooseSC.Size = new System.Drawing.Size(39, 16);
            this.lChooseSC.TabIndex = 12;
            this.lChooseSC.Text = "СК.....";
            this.lChooseSC.Visible = false;
            // 
            // tbYRect
            // 
            this.tbYRect.BackColor = System.Drawing.SystemColors.Window;
            this.tbYRect.Location = new System.Drawing.Point(104, 44);
            this.tbYRect.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.tbYRect.MaxLength = 7;
            this.tbYRect.Name = "tbYRect";
            this.tbYRect.ReadOnly = true;
            this.tbYRect.Size = new System.Drawing.Size(99, 22);
            this.tbYRect.TabIndex = 5;
            // 
            // lYRect
            // 
            this.lYRect.AutoSize = true;
            this.lYRect.Location = new System.Drawing.Point(5, 52);
            this.lYRect.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lYRect.Name = "lYRect";
            this.lYRect.Size = new System.Drawing.Size(85, 16);
            this.lYRect.TabIndex = 6;
            this.lYRect.Text = "Long, deg......";
            // 
            // tbXRect
            // 
            this.tbXRect.BackColor = System.Drawing.SystemColors.Window;
            this.tbXRect.Location = new System.Drawing.Point(104, 16);
            this.tbXRect.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.tbXRect.MaxLength = 7;
            this.tbXRect.Name = "tbXRect";
            this.tbXRect.ReadOnly = true;
            this.tbXRect.Size = new System.Drawing.Size(99, 22);
            this.tbXRect.TabIndex = 4;
            // 
            // lXRect
            // 
            this.lXRect.AutoSize = true;
            this.lXRect.Location = new System.Drawing.Point(5, 25);
            this.lXRect.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lXRect.Name = "lXRect";
            this.lXRect.Size = new System.Drawing.Size(82, 16);
            this.lXRect.TabIndex = 3;
            this.lXRect.Text = "Lat, deg.........";
            // 
            // gbRect
            // 
            this.gbRect.Controls.Add(this.tbYRect);
            this.gbRect.Controls.Add(this.lYRect);
            this.gbRect.Controls.Add(this.tbXRect);
            this.gbRect.Controls.Add(this.lXRect);
            this.gbRect.Location = new System.Drawing.Point(9, 37);
            this.gbRect.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.gbRect.Name = "gbRect";
            this.gbRect.Padding = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.gbRect.Size = new System.Drawing.Size(213, 78);
            this.gbRect.TabIndex = 17;
            this.gbRect.TabStop = false;
            this.gbRect.Visible = false;
            // 
            // tbR1
            // 
            this.tbR1.BackColor = System.Drawing.SystemColors.HighlightText;
            this.tbR1.Location = new System.Drawing.Point(216, 167);
            this.tbR1.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.tbR1.Name = "tbR1";
            this.tbR1.Size = new System.Drawing.Size(82, 22);
            this.tbR1.TabIndex = 192;
            // 
            // lOwnHeight
            // 
            this.lOwnHeight.AutoSize = true;
            this.lOwnHeight.Location = new System.Drawing.Point(12, 12);
            this.lOwnHeight.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lOwnHeight.Name = "lOwnHeight";
            this.lOwnHeight.Size = new System.Drawing.Size(82, 16);
            this.lOwnHeight.TabIndex = 191;
            this.lOwnHeight.Text = "H, m................";
            // 
            // tbOwnHeight
            // 
            this.tbOwnHeight.BackColor = System.Drawing.SystemColors.HighlightText;
            this.tbOwnHeight.Location = new System.Drawing.Point(113, 8);
            this.tbOwnHeight.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.tbOwnHeight.Name = "tbOwnHeight";
            this.tbOwnHeight.ReadOnly = true;
            this.tbOwnHeight.Size = new System.Drawing.Size(101, 22);
            this.tbOwnHeight.TabIndex = 190;
            // 
            // pCoordPoint
            // 
            this.pCoordPoint.Controls.Add(this.gbRect);
            this.pCoordPoint.Controls.Add(this.gbRad);
            this.pCoordPoint.Controls.Add(this.tbOwnHeight);
            this.pCoordPoint.Controls.Add(this.lOwnHeight);
            this.pCoordPoint.Location = new System.Drawing.Point(4, 36);
            this.pCoordPoint.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.pCoordPoint.Name = "pCoordPoint";
            this.pCoordPoint.Size = new System.Drawing.Size(231, 127);
            this.pCoordPoint.TabIndex = 189;
            // 
            // gbRad
            // 
            this.gbRad.Controls.Add(this.tbLRad);
            this.gbRad.Controls.Add(this.lLRad);
            this.gbRad.Controls.Add(this.tbBRad);
            this.gbRad.Controls.Add(this.lBRad);
            this.gbRad.Location = new System.Drawing.Point(1, 46);
            this.gbRad.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.gbRad.Name = "gbRad";
            this.gbRad.Padding = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.gbRad.Size = new System.Drawing.Size(213, 78);
            this.gbRad.TabIndex = 28;
            this.gbRad.TabStop = false;
            this.gbRad.Visible = false;
            // 
            // lDistSightRange
            // 
            this.lDistSightRange.AutoSize = true;
            this.lDistSightRange.Location = new System.Drawing.Point(-4, 172);
            this.lDistSightRange.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lDistSightRange.Name = "lDistSightRange";
            this.lDistSightRange.Size = new System.Drawing.Size(189, 16);
            this.lDistSightRange.TabIndex = 193;
            this.lDistSightRange.Text = "Radius of the spoofing zone, m";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(279, 132);
            this.label11.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(154, 16);
            this.label11.TabIndex = 208;
            this.label11.Text = "Antenna height, m..............";
            // 
            // tbOpponentAntenna
            // 
            this.tbOpponentAntenna.Location = new System.Drawing.Point(540, 128);
            this.tbOpponentAntenna.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.tbOpponentAntenna.Name = "tbOpponentAntenna";
            this.tbOpponentAntenna.Size = new System.Drawing.Size(73, 22);
            this.tbOpponentAntenna.TabIndex = 207;
            this.tbOpponentAntenna.Text = "2";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(277, 100);
            this.label17.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(163, 16);
            this.label17.TabIndex = 206;
            this.label17.Text = "Antenna (JS) height, m........";
            // 
            // tbHAnt
            // 
            this.tbHAnt.Location = new System.Drawing.Point(541, 96);
            this.tbHAnt.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.tbHAnt.Name = "tbHAnt";
            this.tbHAnt.Size = new System.Drawing.Size(73, 22);
            this.tbHAnt.TabIndex = 205;
            this.tbHAnt.Text = "8";
            // 
            // chbS
            // 
            this.chbS.AutoSize = true;
            this.chbS.Location = new System.Drawing.Point(109, 470);
            this.chbS.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.chbS.Name = "chbS";
            this.chbS.Size = new System.Drawing.Size(18, 17);
            this.chbS.TabIndex = 209;
            this.chbS.UseVisualStyleBackColor = true;
            this.chbS.Visible = false;
            this.chbS.CheckedChanged += new System.EventHandler(this.chbS_CheckedChanged);
            // 
            // chbJ
            // 
            this.chbJ.AutoSize = true;
            this.chbJ.Location = new System.Drawing.Point(109, 495);
            this.chbJ.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.chbJ.Name = "chbJ";
            this.chbJ.Size = new System.Drawing.Size(18, 17);
            this.chbJ.TabIndex = 210;
            this.chbJ.UseVisualStyleBackColor = true;
            this.chbJ.Visible = false;
            this.chbJ.CheckedChanged += new System.EventHandler(this.chbJ_CheckedChanged);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(148, 470);
            this.label5.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(99, 16);
            this.label5.TabIndex = 211;
            this.label5.Text = "On/Off spoofing";
            this.label5.Visible = false;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(148, 495);
            this.label6.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(98, 16);
            this.label6.TabIndex = 212;
            this.label6.Text = "On/Off jamming";
            this.label6.Visible = false;
            // 
            // FormSpuf
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(630, 233);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.chbJ);
            this.Controls.Add(this.chbS);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.tbOpponentAntenna);
            this.Controls.Add(this.label17);
            this.Controls.Add(this.tbHAnt);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.lChooseSC);
            this.Controls.Add(this.cbChooseSC);
            this.Controls.Add(this.tbR2);
            this.Controls.Add(this.textBox2);
            this.Controls.Add(this.comboBox2);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.bClear);
            this.Controls.Add(this.bAccept);
            this.Controls.Add(this.lCenterLSR);
            this.Controls.Add(this.cbCenterLSR);
            this.Controls.Add(this.tbR1);
            this.Controls.Add(this.pCoordPoint);
            this.Controls.Add(this.lDistSightRange);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Location = new System.Drawing.Point(900, 100);
            this.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FormSpuf";
            this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
            this.Text = "Spoofing zone";
            this.TopMost = true;
            this.Activated += new System.EventHandler(this.FormSpuf_Activated);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FormSpuf_FormClosing);
            this.Load += new System.EventHandler(this.FormSpuf_Load);
            this.gbRect.ResumeLayout(false);
            this.gbRect.PerformLayout();
            this.pCoordPoint.ResumeLayout(false);
            this.pCoordPoint.PerformLayout();
            this.gbRad.ResumeLayout(false);
            this.gbRad.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        public System.Windows.Forms.TextBox tbR2;
        private System.Windows.Forms.TextBox textBox2;
        public System.Windows.Forms.ComboBox comboBox2;
        private System.Windows.Forms.TextBox textBox1;
        public System.Windows.Forms.ComboBox cbCenterLSR;
        public System.Windows.Forms.TextBox tbLRad;
        private System.Windows.Forms.Label lLRad;
        public System.Windows.Forms.TextBox tbBRad;
        private System.Windows.Forms.Label lBRad;
        public System.Windows.Forms.ComboBox cbChooseSC;
        private System.Windows.Forms.Label lChooseSC;
        public System.Windows.Forms.TextBox tbYRect;
        public System.Windows.Forms.TextBox tbXRect;
        public System.Windows.Forms.GroupBox gbRect;
        public System.Windows.Forms.TextBox tbR1;
        public System.Windows.Forms.TextBox tbOwnHeight;
        private System.Windows.Forms.Panel pCoordPoint;
        public System.Windows.Forms.GroupBox gbRad;
        public System.Windows.Forms.TextBox tbOpponentAntenna;
        public System.Windows.Forms.TextBox tbHAnt;
        public System.Windows.Forms.CheckBox chbS;
        public System.Windows.Forms.CheckBox chbJ;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        public System.Windows.Forms.Label label4;
        public System.Windows.Forms.Label label3;
        public System.Windows.Forms.Label label2;
        public System.Windows.Forms.Label label1;
        public System.Windows.Forms.Button button1;
        public System.Windows.Forms.Button bClear;
        public System.Windows.Forms.Button bAccept;
        public System.Windows.Forms.Label lCenterLSR;
        public System.Windows.Forms.Label lYRect;
        public System.Windows.Forms.Label lXRect;
        public System.Windows.Forms.Label lOwnHeight;
        public System.Windows.Forms.Label lDistSightRange;
        public System.Windows.Forms.Label label11;
        public System.Windows.Forms.Label label17;
    }
}