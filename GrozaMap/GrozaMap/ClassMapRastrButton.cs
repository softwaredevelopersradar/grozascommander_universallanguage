﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.InteropServices;
using System.Windows.Forms;
using System.IO;
using System.Windows.Input;

using Mapsui.Geometries;
using Mapsui.Styles;
using Mapsui.Projection;
using WpfMapControl;
using Bitmap = System.Drawing.Bitmap;
using Point = System.Drawing.Point;

namespace GrozaMap
{
    class ClassMapRastrButton
    {

        // SaveScreen ************************************************************************
        // Обработчик кнопки Button4: save screen

        public static string f_SaveScreen(
                                     // Дочерний элемент MapElementHost -> Элемент, где ложим карту
                                     RasterMapControl objRasterMapControl, Form window
                                    )
        {
            // -------------------------------------------------------------------------------
            //GlobalVarLn.fl_Open_objFormScreen = 1;
            //RasterMapControl.SaveCurrentMapView("file.png");
            //MessageBox.Show("File is saved");
            // -------------------------------------------------------------------------------
            String strLine = "";
            String strLine1 = "";
            String strLine2 = "";
            String path = "";
            String s = "";

            int lng = 0;
            int indStart = 0;
            int indStop = 0;
            int indEnd = 0;
            int iLength = 0;
            int iLength1 = 0;
            int TekPoz = 0;
            // -------------------------------------------------------------------------------
            String time = DateTime.Now.ToString(@"dd/MM/yyyy HH:mm:ss");
            // -------------------------------------------------------------------------------

            TekPoz = 0;
            strLine = time;
            lng = time.Length;
            indEnd = lng - 1;
            indStart = 0;
            indStop = strLine.IndexOf(' ', TekPoz);  // probel
            iLength = indStop - indStart + 1;
            iLength1 = indEnd - indStop + 1;
            strLine1 = strLine.Remove(indStop, iLength1);
            strLine2 = strLine.Remove(indStart, iLength);
            if (strLine1.IndexOf(".") > -1) strLine1 = strLine1.Replace('.', '_');
            if (strLine1.IndexOf("/") > -1) strLine1 = strLine1.Replace('/', '_');
            if (strLine1.IndexOf("\\") > -1) strLine1 = strLine1.Replace('\\', '_');
            if (strLine2.IndexOf(":") > -1) strLine2 = strLine2.Replace(':', '_');
            // -------------------------------------------------------------------------------

            if (Directory.Exists(strLine1) == false)
                Directory.CreateDirectory(Application.StartupPath + "\\MapImages\\" + strLine1);
            path = Application.StartupPath + "\\MapImages\\" + strLine1 + "\\" + strLine2 + ".png";

            //RasterMapControl.SaveCurrentMapView(path);


            //ira
            //objRasterMapControl.SaveCurrentMapView(path);
            // -------------------------------------------------------------------------------
            //MessageBox.Show("File is saved");
            try
            {
                Rectangle bounds = window.Bounds;
                using (Bitmap bitmap = new Bitmap(bounds.Width, bounds.Height))
                {
                    using (Graphics g = Graphics.FromImage(bitmap))
                    {
                        g.CopyFromScreen(new Point(bounds.Left, bounds.Top), Point.Empty, bounds.Size);
                    }

                    bitmap.Save(path, ImageFormat.Jpeg);
                }
                if (GlobalVarLn.Lang == "Eng")
                    s = "File is saved";
                else if (GlobalVarLn.Lang == "Srb")
                    s = "Документ је сачуван";
                else
                    s = "Файл сохранен";
            }
            catch (Exception ex)
            {
                if (GlobalVarLn.Lang == "Eng")
                    s = "Error";
                else if (GlobalVarLn.Lang == "Srb")
                    s = "Грешка!";
                else
                    s = "Ошибка!";
            }


            // -------------------------------------------------------------------------------
            return s;

        }
        // ************************************************************************ SaveScreen



    } // Class
} // NameSpace
