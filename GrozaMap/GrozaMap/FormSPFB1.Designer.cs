﻿namespace GrozaMap
{
    partial class FormSPFB1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.saveFileDialog1 = new System.Windows.Forms.SaveFileDialog();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.Column2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.button2 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.lChooseSC = new System.Windows.Forms.Label();
            this.cbChooseSC = new System.Windows.Forms.ComboBox();
            this.button1 = new System.Windows.Forms.Button();
            this.bClear = new System.Windows.Forms.Button();
            this.bAccept = new System.Windows.Forms.Button();
            this.label15 = new System.Windows.Forms.Label();
            this.chbXY = new System.Windows.Forms.CheckBox();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToAddRows = false;
            this.dataGridView1.AllowUserToDeleteRows = false;
            this.dataGridView1.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dataGridView1.BackgroundColor = System.Drawing.SystemColors.ControlLightLight;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column2,
            this.Column3,
            this.Column4,
            this.Column5});
            this.dataGridView1.Location = new System.Drawing.Point(6, 74);
            this.dataGridView1.Margin = new System.Windows.Forms.Padding(8, 8, 8, 8);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.RowHeadersWidth = 51;
            this.dataGridView1.Size = new System.Drawing.Size(1157, 465);
            this.dataGridView1.TabIndex = 227;
            // 
            // Column2
            // 
            this.Column2.HeaderText = "Fmin,MHz";
            this.Column2.MinimumWidth = 6;
            this.Column2.Name = "Column2";
            this.Column2.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // Column3
            // 
            this.Column3.HeaderText = "Fmax,MHz";
            this.Column3.MinimumWidth = 6;
            this.Column3.Name = "Column3";
            this.Column3.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // Column4
            // 
            this.Column4.HeaderText = "Az.min,deg";
            this.Column4.MinimumWidth = 6;
            this.Column4.Name = "Column4";
            this.Column4.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // Column5
            // 
            this.Column5.HeaderText = "Az.max,deg";
            this.Column5.MinimumWidth = 6;
            this.Column5.Name = "Column5";
            this.Column5.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(192, 8);
            this.button2.Margin = new System.Windows.Forms.Padding(8, 8, 8, 8);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(190, 54);
            this.button2.TabIndex = 230;
            this.button2.Text = "Load";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // button3
            // 
            this.button3.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.button3.Location = new System.Drawing.Point(416, 742);
            this.button3.Margin = new System.Windows.Forms.Padding(8, 8, 8, 8);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(226, 54);
            this.button3.TabIndex = 229;
            this.button3.Text = "Отправить";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Visible = false;
            // 
            // lChooseSC
            // 
            this.lChooseSC.AutoSize = true;
            this.lChooseSC.Location = new System.Drawing.Point(574, 14);
            this.lChooseSC.Margin = new System.Windows.Forms.Padding(8, 0, 8, 0);
            this.lChooseSC.Name = "lChooseSC";
            this.lChooseSC.Size = new System.Drawing.Size(207, 32);
            this.lChooseSC.TabIndex = 224;
            this.lChooseSC.Text = "Jammer station";
            // 
            // cbChooseSC
            // 
            this.cbChooseSC.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbChooseSC.DropDownWidth = 150;
            this.cbChooseSC.FormattingEnabled = true;
            this.cbChooseSC.Items.AddRange(new object[] {
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            ""});
            this.cbChooseSC.Location = new System.Drawing.Point(919, 8);
            this.cbChooseSC.Margin = new System.Windows.Forms.Padding(8, 8, 8, 8);
            this.cbChooseSC.Name = "cbChooseSC";
            this.cbChooseSC.Size = new System.Drawing.Size(238, 39);
            this.cbChooseSC.TabIndex = 223;
            this.cbChooseSC.SelectedIndexChanged += new System.EventHandler(this.cbChooseSC_SelectedIndexChanged);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(80, 742);
            this.button1.Margin = new System.Windows.Forms.Padding(8, 8, 8, 8);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(222, 54);
            this.button1.TabIndex = 228;
            this.button1.Text = "Читать все";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Visible = false;
            // 
            // bClear
            // 
            this.bClear.Location = new System.Drawing.Point(388, 8);
            this.bClear.Margin = new System.Windows.Forms.Padding(8, 8, 8, 8);
            this.bClear.Name = "bClear";
            this.bClear.Size = new System.Drawing.Size(184, 54);
            this.bClear.TabIndex = 226;
            this.bClear.Text = "Clear";
            this.bClear.UseVisualStyleBackColor = true;
            this.bClear.Click += new System.EventHandler(this.bClear_Click);
            // 
            // bAccept
            // 
            this.bAccept.Location = new System.Drawing.Point(2, 8);
            this.bAccept.Margin = new System.Windows.Forms.Padding(8, 8, 8, 8);
            this.bAccept.Name = "bAccept";
            this.bAccept.Size = new System.Drawing.Size(188, 54);
            this.bAccept.TabIndex = 225;
            this.bAccept.Text = "Save";
            this.bAccept.UseVisualStyleBackColor = true;
            this.bAccept.Click += new System.EventHandler(this.bAccept_Click);
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(14, 544);
            this.label15.Margin = new System.Windows.Forms.Padding(8, 0, 8, 0);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(202, 32);
            this.label15.TabIndex = 231;
            this.label15.Text = "Input from map";
            // 
            // chbXY
            // 
            this.chbXY.AutoSize = true;
            this.chbXY.Location = new System.Drawing.Point(234, 544);
            this.chbXY.Margin = new System.Windows.Forms.Padding(8, 8, 8, 8);
            this.chbXY.Name = "chbXY";
            this.chbXY.Size = new System.Drawing.Size(34, 33);
            this.chbXY.TabIndex = 232;
            this.chbXY.UseVisualStyleBackColor = true;
            this.chbXY.CheckedChanged += new System.EventHandler(this.chbXY_CheckedChanged);
            // 
            // FormSPFB1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(16F, 31F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1166, 585);
            this.Controls.Add(this.chbXY);
            this.Controls.Add(this.label15);
            this.Controls.Add(this.dataGridView1);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.lChooseSC);
            this.Controls.Add(this.cbChooseSC);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.bClear);
            this.Controls.Add(this.bAccept);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Location = new System.Drawing.Point(900, 100);
            this.Margin = new System.Windows.Forms.Padding(8, 8, 8, 8);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FormSPFB1";
            this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
            this.Text = "Bands and sectors of jamming ( not more than 10)";
            this.TopMost = true;
            this.Activated += new System.EventHandler(this.FormSPFB1_Activated);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FormSPFB1_FormClosing);
            this.Load += new System.EventHandler(this.FormSPFB1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.SaveFileDialog saveFileDialog1;
        public System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.Button button3;
        public System.Windows.Forms.ComboBox cbChooseSC;
        private System.Windows.Forms.Button button1;
        public System.Windows.Forms.CheckBox chbXY;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column2;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column3;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column4;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column5;
        public System.Windows.Forms.Button button2;
        public System.Windows.Forms.Label lChooseSC;
        public System.Windows.Forms.Button bClear;
        public System.Windows.Forms.Button bAccept;
        public System.Windows.Forms.Label label15;
    }
}