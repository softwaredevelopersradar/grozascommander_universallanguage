﻿using System;
using System.Drawing;
using System.Runtime.InteropServices;
using System.Windows.Forms;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System.Reflection;

using System.Windows.Input;
using Mapsui.Geometries;
using Mapsui.Styles;
using Mapsui.Projection;
using WpfMapControl;
using Bitmap = System.Drawing.Bitmap;

using System.Linq;

namespace GrozaMap
{
    public partial class FormAz1 : Form
    {
        // Переменные VAR VAR VAR VAR VAR VAR VAR VAR VAR VAR VAR VAR VAR VAR VAR 
        private double dchislo;
        private long ichislo;
        //private double LAMBDA;
        // VAR VAR VAR VAR VAR VAR VAR VAR VAR VAR VAR VAR VAR VAR VAR  Переменные

        // Конструктор *********************************************************** 

        public FormAz1()
        {
            InitializeComponent();

            dchislo = 0;
            ichislo = 0;
            //LAMBDA = 300000;

            GlobalVarLn.ListJSChangedEvent += OnListJSChanged;

        } // Конструктор
        // ***********************************************************  Конструктор

        // Загрузка формы *********************************************************

        private void FormAz1_Load(object sender, EventArgs e)
        {

            ClassMapRastrLoadForm.f_Load_FormAzimuth();

        }
        // ********************************************************* Загрузка формы

        // ChangeSP ***************************************************************
        // Перерисовка таблицы при изменении листа СП

        private void OnListJSChanged(object sender, EventArgs e)
        {
            ClassMapRastrDopFunctions.f_Update_datgrid_listJS
                                                             (
                                                             GlobalVarLn.objFormAz1G.dataGridView1
                                                             );

        }
        // *************************************************************** ChangeSP

        // Активизировать форму ***************************************************

        private void FormAz1_Activated(object sender, EventArgs e)
        {
            GlobalVarLn.blOB_az1 = true;
            GlobalVarLn.flEndOB_az1 = 1;

            GlobalVarLn.fl_Open_objFormAz1 = 1;

        }
        // *************************************************** Активизировать форму

        // Закрыть форму **********************************************************

        private void FormAz1_FormClosing(object sender, FormClosingEventArgs e)
        {
            e.Cancel = true;
            Hide();

            // приостановить обработку, если идет
            GlobalVarLn.blOB_az1 = false;

            GlobalVarLn.fl_Open_objFormAz1 = 0;

        }
        // ********************************************************** Закрыть форму

        // Очистка ****************************************************************

        private void bClear_Click(object sender, EventArgs e)
        {
            // -------------------------------------------------------------------
            ClassMapRastrClear.f_Clear_FormAz1();
            // ----------------------------------------------------------------------
            // Убрать с карты

            // CLASS
            ClassMapRastrReDrawAll.REDRAW_MAP_CLASS();
            // -------------------------------------------------------------------

        }
        // **************************************************************** Очистка

        // ************************************************************************
        // Обработчик ComboBox "cbChooseSC": Выбор СК
        // ************************************************************************
        private void cbChooseSC_SelectedIndexChanged(object sender, EventArgs e)
        {
            //ChooseSystemCoord_az1(cbChooseSC.SelectedIndex);

        } // Выбор СК
        // ************************************************************************

        // ************************************************************************
        // Принять (посчитать азимут)
        // ************************************************************************
        private void button2_Click(object sender, EventArgs e)
        {
            f_OB_az1(0, 0);

            var objClassMap6 = new ClassMap();
            var stations = GlobalVarLn.listJS
                .Where(s => s.HasCurrentPosition)
                .ToList();
            

            for (var i = 0; i < stations.Count; i++)
            {
                var X_Coordl3_1 = stations[i].CurrentPosition.x;
                var Y_Coordl3_1 = stations[i].CurrentPosition.y;
                var X_Coordl3_2 = GlobalVarLn.XCenter_az1;
                var Y_Coordl3_2 = GlobalVarLn.YCenter_az1;

                //0209  !!!Y -вверх
                // Разность координат для расчета азимута
                // На карте X - вверх
                //var DXX3 = Y_Coordl3_2 - Y_Coordl3_1;
                //var DYY3 = X_Coordl3_2 - X_Coordl3_1;
                var DXX3 = X_Coordl3_2 - X_Coordl3_1;
                var DYY3 = Y_Coordl3_2 - Y_Coordl3_1;

                // grad
                var azz = objClassMap6.f_Def_Azimuth(DYY3, DXX3);


                ichislo = (long) (azz * 100);
                dchislo = ((double) ichislo) / 100;
                dataGridView1.Rows[i].Cells[3].Value = (int) azz;
            }

            // CLASS
            ClassMapRastrReDrawAll.REDRAW_MAP_CLASS();

        }

        // NEW NEWNEWNEWNEWNEWNEWNEWNEWNEWNEWNEWNEWNEWNEWNEWNEWNEWNEWNEWNEWNEWNEWN
        private void OtobrOB_az1()
        {
            double lat = 0;
            double lon=0;
            // -----------------------------------------------------------------------
            // Метры на местности

            //0209
            //var p = axaxcMapScreen.MapPlaneToRealGeo(GlobalVarLn.XCenter_az1, GlobalVarLn.YCenter_az1);
            var p = Mercator.ToLonLat(GlobalVarLn.XCenter_az1, GlobalVarLn.YCenter_az1);
            lat = p.Y;
            lon = p.X;
            tbXRect.Text = lat.ToString("F3");
            tbYRect.Text = lon.ToString("F3");
            // ----------------------------------------------------------------------

        } // OtobrOB_az1

        // NEW NEWNEWNEWNEWNEWNEWNEWNEWNEWNEWNEWNEWNEWNEWNEWNEWNEWNEWNEWNEWNEWNEWN
  
        // ************************************************************************

        // NEWNEWNEWNEWNEWNEWNEWNEWNEWNEWNEWNEWNEWNEWNEWNEWNEWNEWNEWNEWNEWNEWNEWNEW
        public void f_OB_az1(double X, double Y)
        {

            double lat = 0;
            double lon = 0;
            double xtmp1_ed = 0;
            double ytmp1_ed = 0;

            // ......................................................................
            //var objClassMap3_ed = new ClassMap();
            // ......................................................................
            // !!! реальные координаты на местности карты в м (Plane)

            GlobalVarLn.XCenter_az1 = GlobalVarLn.XXXaz;
            GlobalVarLn.YCenter_az1 = GlobalVarLn.YYYaz;

            //0209
            //GlobalVarLn.objFormWayG.f_Map_Rect_XY2(GlobalVarLn.XCenter_az1, GlobalVarLn.YCenter_az1);

            GlobalVarLn.blOB_az1 = true;
            GlobalVarLn.flEndOB_az1 = 1;
            GlobalVarLn.flCoordOB_az1 = 1;
            // ......................................................................
            // Реальные координаты карты в м -> в долготу и широту WGS84 с помощью функций Панорамы
            // !!! Выход функции(rad) идет на место входных переменных

            var xtmp_ed = GlobalVarLn.XCenter_az1;
            var ytmp_ed = GlobalVarLn.YCenter_az1;

            //0209
            //mapPlaneToGeoWGS84(GlobalVarLn.hmapl, ref xtmp_ed, ref ytmp_ed);
            var p = Mercator.ToLonLat(xtmp_ed, ytmp_ed);
            lat = p.Y;
            lon = p.X;
            xtmp1_ed = lat;
            ytmp1_ed = lon;
            GlobalVarLn.Lat84_az1 = lat;
            GlobalVarLn.Long84_az1 = lon;


            //0209
            // rad(WGS84)->grad(WGS84)
            //var xtmp1_ed = (xtmp_ed * 180) / Math.PI;
            //var ytmp1_ed = (ytmp_ed * 180) / Math.PI;
            // .......................................................................
            // CDel

//0209
/*
            // WGS84,grad
            LatKrG_comm = xtmp1_ed;
            LongKrG_comm = ytmp1_ed;
            // .......................................................................

            LatKrR_comm = (LatKrG_comm * Math.PI) / 180;
            LongKrR_comm = (LongKrG_comm * Math.PI) / 180;

            objClassMap3_ed.f_Grad_GMS
            (
                // Входные параметры (grad)
                LatKrG_comm,

                // Выходные параметры 
                ref Lat_Grad_comm,
                ref Lat_Min_comm,
                ref Lat_Sec_comm

            );

            // Долгота
            objClassMap3_ed.f_Grad_GMS
            (
                // Входные параметры (grad)
                LongKrG_comm,

                // Выходные параметры 
                ref Long_Grad_comm,
                ref Long_Min_comm,
                ref Long_Sec_comm

            );
*/

            OtobrOB_az1();

            // .......................................................................
            // H

            //0209
            //GlobalVarLn.axMapPointGlobalAdd.SetPoint(GlobalVarLn.XXXaz, GlobalVarLn.YYYaz);
            //GlobalVarLn.HCenter_az1 = GlobalVarLn.axMapScreenGlobal.PointHeight_get(GlobalVarLn.axMapPointGlobalAdd);
            GlobalVarLn.HCenter_az1 = GlobalVarLn.H_Rastr;
            tbOwnHeight.Text = Convert.ToString((int) GlobalVarLn.HCenter_az1);
            // .......................................................................

            //0209
            //axaxcMapScreen.Repaint();
            //MapForm.REDRAW_MAP();
        }

        // NEWNEWNEWNEWNEWNEWNEWNEWNEWNEWNEWNEWNEWNEWNEWNEWNEWNEWNEWNEWNEWNEWNEWNEW

        // *************************************************************************************


        // -------------------------------------------------------------------------
        private void label15_Click(object sender, EventArgs e)
        {
            ;
        }
        // -------------------------------------------------------------------------


    } // Class
} // namespace
