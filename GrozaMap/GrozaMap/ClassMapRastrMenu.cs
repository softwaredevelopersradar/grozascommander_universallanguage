﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.InteropServices;
using System.Windows.Forms;
using System.IO;
using System.Windows.Input;

using Mapsui.Geometries;
using Mapsui.Styles;
using Mapsui.Projection;
using WpfMapControl;
using Bitmap = System.Drawing.Bitmap;
using Point = System.Drawing.Point;

namespace GrozaMap
{
    class ClassMapRastrMenu
    {
        // OpenMap ***************************************************************************
        // Открыть карту при загрузке карты (путь - в Setting.ini)

        public static string f_OpenMap(
                                     // Дочерний элемент MapElementHost -> Элемент, где ложим карту
                                     RasterMapControl objRasterMapControl,
                                     ref bool MapOpened
                                    )
        {
            string pathMap = "";

            if (System.IO.File.Exists(Application.StartupPath + "\\Setting.ini"))
            {
                //pathMap = Application.StartupPath + iniRW.get_map_path();
                // Полный путь
                pathMap = iniRW.get_map_path();

                try
                {
                    //mapControl.DataFolder = PathMapTile;
                    //await mapControl.OpenMap(PathMap);
                    objRasterMapControl.OpenMap(pathMap);
                    MapOpened = true;
                    return "";
                }
                catch (Exception ex)
                {
                    MapOpened = false;

                    if (GlobalVarLn.Lang == "Eng")
                        return "Can't load map";
                    else if (GlobalVarLn.Lang == "Srb")
                        return "Није могуће учитати карту";
                    else
                        return "Невозможно загрузить карту";

                }

            }
            else
            {
                MapOpened = false;

                if (GlobalVarLn.Lang == "Eng")
                    return "Can’t open map (no INI file)";
                else if (GlobalVarLn.Lang == "Srb")
                    return "Није могуће отварити карту (нема INI документа)";
                else
                    return "Невозможно открыть карту (нет файла INI)";

            }
        }
        // *************************************************************************** OpenMap

        // OpenMatrix ************************************************************************
        // Открыть матрицу высот при загрузке карты (путь - в Setting.ini)

        public static string f_OpenMatrix(
                                     // Дочерний элемент MapElementHost -> Элемент, где ложим карту
                                     RasterMapControl objRasterMapControl,
                                     ref bool MatrixOpened
                                       )
        {

            string pathMatrix = "";

            if (System.IO.File.Exists(Application.StartupPath + "\\Setting.ini"))
            {
                //pathMatrix = Application.StartupPath + iniRW.get_matrix_path();
                // Полный путь
                pathMatrix = iniRW.get_matrix_path();

                try
                {
                    /////////////***************

                    string[] files = Directory.GetFiles(pathMatrix);

                    //string ext = " ";

                    //for (int i = 0; i < files.Length; i++)
                    //{
                    //    ext = files[i].Substring(files[i].LastIndexOf('.'));

                    //    if (ext == ".dt2" || ext == ".dt1" || ext == ".dt")
                    //        continue;
                    //    else
                    //    {
                    //        List<string> tmp = new List<string>(files);
                    //        tmp.RemoveAt(i);
                    //        files = tmp.ToArray();
                    //    }
                    //}
                    objRasterMapControl.OpenDted(files);
                    //////////////

                    //objRasterMapControl.OpenDted(pathMatrix);
                    MatrixOpened = true;
                    return "";
                }
                catch (Exception ex)
                {
                    MatrixOpened = false;

                    if (GlobalVarLn.Lang == "Eng")
                        return "Can't load digital elevation model";
                    else if (GlobalVarLn.Lang == "Srb")
                        return "Није могуће учитати матрице елевација (висина)";
                    else
                        return "Невозможно загрузить матрицу высот";

                }

            }
            else
            {
                MatrixOpened = false;

                if (GlobalVarLn.Lang == "Eng")
                    return "Can’t open digital elevation model (no INI file)";
                else if (GlobalVarLn.Lang == "Srb")
                    return "Није могуће отворити матрицу елевација (висина) (нема INI документа)";
                else
                    return "Невозможно открыть матрицу высот (нет файла INI)";

            }

        }
        //************************************************************************  OpenMatrix

        // GetResolution *********************************************************************
        // Установить текущий масштаб с Setting ini при загрузке карты

        public static string f_GetResolution(
                                     // Дочерний элемент MapElementHost -> Элемент, где ложим карту
                                     RasterMapControl objRasterMapControl
                                            )
        {
            MapForm.RasterMapControl.Resolution = (double)iniRW.get_scl();

            if (System.IO.File.Exists(Application.StartupPath + "\\Setting.ini"))
            {
                try
                {
                    objRasterMapControl.Resolution = (double)iniRW.get_scl();
                    return "";
                }
                catch (Exception ex)
                {
                    if (GlobalVarLn.Lang == "Eng")
                        return "Can't get scale";
                    else if (GlobalVarLn.Lang == "Srb")
                        return "Није могуће добити размеру";
                    else
                        return "Невозможно получить масштаб";

                }

            }
            else
            {
                if (GlobalVarLn.Lang == "Eng")
                    return "Can’t open INI file";
                else if (GlobalVarLn.Lang == "Srb")
                    return "Није могуће отворити INI документ";
                else
                    return "Невозможно открыть файл INI";

            }

        }
        // ********************************************************************* GetResolution

        // CloseMapMenu **********************************************************************
        // Закрыть карту и матрицу высот -> пункт меню

        public static string f_CloseMapMenu(
                                     // Дочерний элемент MapElementHost -> Элемент, где ложим карту
                                     RasterMapControl objRasterMapControl,
                                     ref bool MapOpened,
                                     ref bool MatrixOpened
                                         )
        {
            // ...............................................................................
            if (MapOpened == true)
            {
                objRasterMapControl.CloseMap();
                MapOpened = false;
            }
            // ...............................................................................
            if (MatrixOpened == true)
            {
                objRasterMapControl.CloseDted();
                MatrixOpened = false;
            }
            // ...............................................................................
            // Сохранить текущий масштаб в Setting.ini

            if (System.IO.File.Exists(Application.StartupPath + "\\Setting.ini"))
            {
                try
                {
                    // Сохранить текущий масштаб в Setting.ini
                    iniRW.write_map_scl((int)objRasterMapControl.Resolution);
                    return "";
                }
                catch (Exception ex)
                {
                    if (GlobalVarLn.Lang == "Eng")
                        return "Can't save scale";
                    else if (GlobalVarLn.Lang == "Srb")
                        return "Није могуће сачувати размеру";
                    else
                        return "Невозможно сохранить масштаб";

                }

            }
            else
            {
                if (GlobalVarLn.Lang == "Eng")
                    return "Can't save scale (no INI file)";
                else if (GlobalVarLn.Lang == "Srb")
                    return "Није могуће сачувати размеру (нема INI документа)";
                else
                    return "Невозможно сохранить масштаб (нет файла INI)";

            }
            // ...............................................................................

        }
        // ********************************************************************** CloseMapMenu

        // OpenMapMenu ***********************************************************************
        // Открыть карту -> пункт меню

        public  static string f_OpenMapMenu(
                                         // Путь к карте
                                         string path,
                                         // Дочерний элемент MapElementHost -> Элемент, где ложим карту
                                         RasterMapControl objRasterMapControl,
                                         ref bool MapOpened
                                          )
        {

            // -----------------------------------------------------------------
            int length = 0;
            int length1 = 0;
            int indStart = 0;
            string s = "";
            string s1 = "";

            s = path;
            length = path.Length;
            if (length < 5)
            {
                if (GlobalVarLn.Lang == "Eng")
                    return "Invalid map format";
                else if (GlobalVarLn.Lang == "Srb")
                    return "Погрешан формат карте";
                else
                    return "Недопустимый формат карты";

            }

            length1 = length-4;
            s1 = s.Remove(indStart, length1);

            if ((s1 != ".tif") && (s1 != ".IMG") && (s1 != ".JP2"))
            {
                if (GlobalVarLn.Lang == "Eng")
                    return "Invalid map format";
                else if (GlobalVarLn.Lang == "Srb")
                    return "Погрешан формат карте";
                else
                    return "Недопустимый формат карты";

            }
            // -----------------------------------------------------------------
            if (MapOpened == false)
            {
               try
               {
                    objRasterMapControl.OpenMap(path);

                    

                    // ...............................................................................
                    // Сохранить путь

                    if (System.IO.File.Exists(Application.StartupPath + "\\Setting.ini"))
                    {
                        try
                        {
                            iniRW.set_map_path(path);
                            MapOpened = true;
                            return "";
                        }
                        catch (Exception ex)
                        {
                            if (GlobalVarLn.Lang == "Eng")
                                return "Can't save path";
                            else if (GlobalVarLn.Lang == "Srb")
                                return "Није могуће сачувати пут";
                            else
                                return "Невозможно сохранить путь";

                        }
                    }
                    else
                    {
                        if (GlobalVarLn.Lang == "Eng")
                            return "Can't save path (no INI file)";
                        else if (GlobalVarLn.Lang == "Srb")
                            return "Није могуће сачувати пут (нема INI докумета)";
                        else
                            return "Невозможно сохранить путь (нет файла INI)";

                    }
                    // ...............................................................................

                }
               catch (Exception ex)
               {
                    if (GlobalVarLn.Lang == "Eng")
                        return "Can't load map";
                    else if (GlobalVarLn.Lang == "Srb")
                        return "Није могуће учитати карту";
                    else
                        return "Невозможно загрузить карту";

                }

            } // MapOpened=false
              // -----------------------------------------------------------------
            else
            {
                if (GlobalVarLn.Lang == "Eng")
                    return "Map is already open";
                else if (GlobalVarLn.Lang == "Srb")
                    return "Карта је већ отворена";
                else
                    return "Карта уже открыта";


            } // MapOpened=true
            // -----------------------------------------------------------------

        }
        // *********************************************************************** OpenMapMenu

        // OpenMatrixMenu ********************************************************************
        // Открыть матрицу высот -> пункт меню

        public static string f_OpenMatrixMenu(
                                         // Путь к матрице высот 
                                         string path,
                                         // Дочерний элемент MapElementHost -> Элемент, где ложим карту
                                         RasterMapControl objRasterMapControl,
                                         ref bool MatrixOpened
                                             )
        {

            // -----------------------------------------------------------------
            int length = 0;
            int length1 = 0;
            int indStart = 0;
            string s = "";
            string s1 = "";

            s = path;
            //length = path.Length;
            //if (length < 5)
            //{
            //    if (GlobalVarLn.Lang == "Eng")
            //        return "Invalid Height Matrix format";
            //    else
            //        return "Недопустимый формат матрицы высот";

            //}

            //length1 = length - 4;
            //s1 = s.Remove(indStart, length1);

            //if ((s1 != ".dt0") && (s1 != ".dt1") && (s1 != ".dt2"))
            //{
            //    if (GlobalVarLn.Lang == "Eng")
            //        return "Invalid Height Matrix format";
            //    else
            //        return "Недопустимый формат матрицы высот";

            //}
            // -----------------------------------------------------------------
            if (MatrixOpened == false)
            {
                try
                {

                    string[] files = Directory.GetFiles(path);

                    //string ext = " ";

                    //for (int i = 0; i < files.Length; i++)
                    //{
                    //    ext = files[i].Substring(files[i].LastIndexOf('.'));

                    //    if (ext == ".dt2" || ext == ".dt1" || ext == ".dt")
                    //        continue;
                    //    else
                    //    {
                    //        List<string> tmp = new List<string>(files);
                    //        tmp.RemoveAt(i);
                    //        files = tmp.ToArray();
                    //    }
                    //}

                    objRasterMapControl.OpenDted(files);
                    //objRasterMapControl.OpenDted(path);

                    // ...............................................................................
                    // Сохранить путь

                    if (System.IO.File.Exists(Application.StartupPath + "\\Setting.ini"))
                    {
                        try
                        {
                            iniRW.set_matrix_path(path);
                            MatrixOpened = true;
                            return "";
                        }
                        catch (Exception ex)
                        {
                            if (GlobalVarLn.Lang == "Eng")
                                return "Can't save path";
                            else if (GlobalVarLn.Lang == "Srb")
                                return "Није могуће сачувати пут";
                            else
                                return "Невозможно сохранить путь";
                        }
                    }
                    else
                    {
                        if (GlobalVarLn.Lang == "Eng")
                            return "Can't save path (no INI file)";
                        else if (GlobalVarLn.Lang == "Srb")
                            return "Није могуће сачувати пут (нема INI документа)";
                        else
                            return "Невозможно сохранить путь (нет файла INI)";
                    }
                    // ...............................................................................

                }
                catch (Exception ex)
                {
                    if (GlobalVarLn.Lang == "Eng")
                        return "Can't load digital elevation model";
                    else if (GlobalVarLn.Lang == "Srb")
                        return "Није могуће учитати матрицу елевација";
                    else
                        return "Невозможно загрузить матрицу высот";

                }

            } // MatrixOpened=false
              // -----------------------------------------------------------------

            else
            {
                if (GlobalVarLn.Lang == "Eng")
                    return "Digital elevation model is already open";
                else if (GlobalVarLn.Lang == "Srb")
                    return "Матрица елевација је већ учитана";
                else
                    return "Матрица высот уже загружена";


            } // MatrixOpened=true
            // -----------------------------------------------------------------

        }
        // ******************************************************************** OpenMatrixMenu

        // ResolutionBase ********************************************************************
        // Исходный масштаб

        public static void f_ResolutionBase(
                                            // Дочерний элемент MapElementHost -> Элемент, где ложим карту
                                            RasterMapControl objRasterMapControl
                                           )
        {
            objRasterMapControl.Resolution = 43942;

        }
        // ******************************************************************** ResolutionBase

        // ResolutionIncrease ****************************************************************
        // Увеличить масштаб

        public static void f_ResolutionIncrease(
                                            // Дочерний элемент MapElementHost -> Элемент, где ложим карту
                                            RasterMapControl objRasterMapControl
                                               )
        {

            //objRasterMapControl.Resolution = objRasterMapControl.Resolution - 10000;

            //if (objRasterMapControl.Resolution <= objRasterMapControl.MinResolution)
            //    objRasterMapControl.Resolution = objRasterMapControl.MinResolution;
            //else if (objRasterMapControl.Resolution == objRasterMapControl.MaxResolution)
            //    objRasterMapControl.Resolution = 73942;
            var newValue = objRasterMapControl.Resolution - 10000;

            if (newValue <= objRasterMapControl.MinResolution)
                objRasterMapControl.Resolution = objRasterMapControl.MinResolution;
            else if (newValue >= objRasterMapControl.MaxResolution)
                objRasterMapControl.Resolution = objRasterMapControl.MaxResolution;
            else
            {
                objRasterMapControl.Resolution = newValue;
            }
            //objRasterMapControl.Resolution = objRasterMapControl.MaxResolution - objRasterMapControl.Resolution
        }
        // **************************************************************** ResolutionIncrease

        // ResolutionDecrease ****************************************************************
        // Уменьшить масштаб

        public static void f_ResolutionDecrease(
                                            // Дочерний элемент MapElementHost -> Элемент, где ложим карту
                                            RasterMapControl objRasterMapControl
                                               )
        {
            //objRasterMapControl.Resolution = objRasterMapControl.Resolution + 10000;

            //if (objRasterMapControl.Resolution >= objRasterMapControl.MaxResolution)
            //    objRasterMapControl.Resolution = objRasterMapControl.MaxResolution;
            //else if (objRasterMapControl.Resolution == objRasterMapControl.MinResolution)
            //    objRasterMapControl.Resolution = 13942;
            var newValue = objRasterMapControl.Resolution + 10000;

            if (newValue <= objRasterMapControl.MinResolution)
                objRasterMapControl.Resolution = objRasterMapControl.MinResolution;
            else if (newValue >= objRasterMapControl.MaxResolution)
                objRasterMapControl.Resolution = objRasterMapControl.MaxResolution;
            else
            {
                objRasterMapControl.Resolution = newValue;
            }

        }
        // **************************************************************** ResolutionDecrease

        // CenterMapTo ***********************************************************************
        // Сцентрировать карту на определенную позицию

        public static string f_CenterMapTo(
                                     // Дочерний элемент MapElementHost -> Элемент, где ложим карту
                                     RasterMapControl objRasterMapControl,
                                     // Меркатор
                                     double x,
                                     double y
                                         )
        {
            try
            {
                double lat;
                double lon;
                Mapsui.Geometries.Point g = new Mapsui.Geometries.Point();

                //var p = Mercator.ToLonLat(GlobalVarLn.listJS[0].CurrentPosition.x, GlobalVarLn.listJS[0].CurrentPosition.y);
                var p = Mercator.ToLonLat(x, y);
                lat = p.Y;
                lon = p.X;

                // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
                // 555
                // Если карта - Меркатор

                if (GlobalVarLn.TypeMap == 0)
                {
                    var pp = Mercator.FromLonLat(lon, lat);
                    lon = pp.X;
                    lat = pp.Y;
                }
                // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>


                //g.X = lon;
                //g.Y = lat;


                g.Y = lon;
                g.X = lat;

                objRasterMapControl.NavigateTo(g);
                return "";
            }
            catch
            {
                if (GlobalVarLn.Lang == "Eng")
                    return "Can't go to this position";
                else if (GlobalVarLn.Lang == "Srb")
                    return "Није могуће прећи на ову позицију";
                else
                    return "Невозможно перейти на эту позицию";

            }

        }
        // *********************************************************************** CenterMapTo


    } // Class
} // Namespace
