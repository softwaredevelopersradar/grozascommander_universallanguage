﻿namespace GrozaMap
{
    partial class FormSupSpuf
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label11 = new System.Windows.Forms.Label();
            this.tbOpponentAntenna = new System.Windows.Forms.TextBox();
            this.label17 = new System.Windows.Forms.Label();
            this.tbHAnt = new System.Windows.Forms.TextBox();
            this.powerTextbox = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.comboBox2 = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.button1 = new System.Windows.Forms.Button();
            this.bClear = new System.Windows.Forms.Button();
            this.bAccept = new System.Windows.Forms.Button();
            this.lDistSightRange = new System.Windows.Forms.Label();
            this.tbDistSightRange = new System.Windows.Forms.TextBox();
            this.pCoordPoint = new System.Windows.Forms.Panel();
            this.gbRect = new System.Windows.Forms.GroupBox();
            this.tbYRect = new System.Windows.Forms.TextBox();
            this.lYRect = new System.Windows.Forms.Label();
            this.tbXRect = new System.Windows.Forms.TextBox();
            this.lXRect = new System.Windows.Forms.Label();
            this.gbRad = new System.Windows.Forms.GroupBox();
            this.tbLRad = new System.Windows.Forms.TextBox();
            this.lLRad = new System.Windows.Forms.Label();
            this.tbBRad = new System.Windows.Forms.TextBox();
            this.lBRad = new System.Windows.Forms.Label();
            this.tbOwnHeight = new System.Windows.Forms.TextBox();
            this.lOwnHeight = new System.Windows.Forms.Label();
            this.lCenterLSR = new System.Windows.Forms.Label();
            this.cbCenterLSR = new System.Windows.Forms.ComboBox();
            this.cbChooseSC = new System.Windows.Forms.ComboBox();
            this.lChooseSC = new System.Windows.Forms.Label();
            this.gbDegMinSec = new System.Windows.Forms.GroupBox();
            this.tbLSec = new System.Windows.Forms.TextBox();
            this.tbBSec = new System.Windows.Forms.TextBox();
            this.lMin4 = new System.Windows.Forms.Label();
            this.lMin3 = new System.Windows.Forms.Label();
            this.tbLMin2 = new System.Windows.Forms.TextBox();
            this.tbBMin2 = new System.Windows.Forms.TextBox();
            this.lSec2 = new System.Windows.Forms.Label();
            this.lSec1 = new System.Windows.Forms.Label();
            this.lDeg4 = new System.Windows.Forms.Label();
            this.lDeg3 = new System.Windows.Forms.Label();
            this.tbLDeg2 = new System.Windows.Forms.TextBox();
            this.lLDegMinSec = new System.Windows.Forms.Label();
            this.tbBDeg2 = new System.Windows.Forms.TextBox();
            this.lBDegMinSec = new System.Windows.Forms.Label();
            this.gbRect42 = new System.Windows.Forms.GroupBox();
            this.tbYRect42 = new System.Windows.Forms.TextBox();
            this.lYRect42 = new System.Windows.Forms.Label();
            this.tbXRect42 = new System.Windows.Forms.TextBox();
            this.lXRect42 = new System.Windows.Forms.Label();
            this.gbDegMin = new System.Windows.Forms.GroupBox();
            this.tbLMin1 = new System.Windows.Forms.TextBox();
            this.tbBMin1 = new System.Windows.Forms.TextBox();
            this.lLDegMin = new System.Windows.Forms.Label();
            this.lBDegMin = new System.Windows.Forms.Label();
            this.pCoordPoint.SuspendLayout();
            this.gbRect.SuspendLayout();
            this.gbRad.SuspendLayout();
            this.gbDegMinSec.SuspendLayout();
            this.gbRect42.SuspendLayout();
            this.gbDegMin.SuspendLayout();
            this.SuspendLayout();
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(488, 267);
            this.label11.Margin = new System.Windows.Forms.Padding(8, 0, 8, 0);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(349, 32);
            this.label11.TabIndex = 178;
            this.label11.Text = "Antenna height, m.............";
            // 
            // tbOpponentAntenna
            // 
            this.tbOpponentAntenna.Location = new System.Drawing.Point(940, 260);
            this.tbOpponentAntenna.Margin = new System.Windows.Forms.Padding(8, 8, 8, 8);
            this.tbOpponentAntenna.Name = "tbOpponentAntenna";
            this.tbOpponentAntenna.Size = new System.Drawing.Size(154, 38);
            this.tbOpponentAntenna.TabIndex = 177;
            this.tbOpponentAntenna.Text = "2";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(488, 207);
            this.label17.Margin = new System.Windows.Forms.Padding(8, 0, 8, 0);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(351, 32);
            this.label17.TabIndex = 176;
            this.label17.Text = "Antenna (JS) height, m......";
            // 
            // tbHAnt
            // 
            this.tbHAnt.Location = new System.Drawing.Point(940, 200);
            this.tbHAnt.Margin = new System.Windows.Forms.Padding(8, 8, 8, 8);
            this.tbHAnt.Name = "tbHAnt";
            this.tbHAnt.Size = new System.Drawing.Size(154, 38);
            this.tbHAnt.TabIndex = 175;
            this.tbHAnt.Text = "8";
            this.tbHAnt.TextChanged += new System.EventHandler(this.tbHAnt_TextChanged);
            // 
            // powerTextbox
            // 
            this.powerTextbox.BackColor = System.Drawing.SystemColors.HighlightText;
            this.powerTextbox.Location = new System.Drawing.Point(944, 12);
            this.powerTextbox.Margin = new System.Windows.Forms.Padding(8, 8, 8, 8);
            this.powerTextbox.Name = "powerTextbox";
            this.powerTextbox.Size = new System.Drawing.Size(150, 38);
            this.powerTextbox.TabIndex = 174;
            this.powerTextbox.Text = "15";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(482, 145);
            this.label4.Margin = new System.Windows.Forms.Padding(8, 0, 8, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(350, 32);
            this.label4.TabIndex = 173;
            this.label4.Text = "JSR, dB (30-50) ................";
            // 
            // comboBox2
            // 
            this.comboBox2.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox2.FormattingEnabled = true;
            this.comboBox2.Items.AddRange(new object[] {
            "1.5",
            "7"});
            this.comboBox2.Location = new System.Drawing.Point(940, 70);
            this.comboBox2.Margin = new System.Windows.Forms.Padding(8, 8, 8, 8);
            this.comboBox2.Name = "comboBox2";
            this.comboBox2.Size = new System.Drawing.Size(154, 39);
            this.comboBox2.TabIndex = 172;
            this.comboBox2.SelectedIndexChanged += new System.EventHandler(this.comboBox2_SelectedIndexChanged);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(488, 70);
            this.label3.Margin = new System.Windows.Forms.Padding(8, 0, 8, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(347, 32);
            this.label3.TabIndex = 171;
            this.label3.Text = "Gain..................................";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(486, 10);
            this.label2.Margin = new System.Windows.Forms.Padding(8, 0, 8, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(344, 32);
            this.label2.TabIndex = 170;
            this.label2.Text = "Power, W (10-100) ...........";
            // 
            // textBox1
            // 
            this.textBox1.BackColor = System.Drawing.SystemColors.HighlightText;
            this.textBox1.Location = new System.Drawing.Point(940, 136);
            this.textBox1.Margin = new System.Windows.Forms.Padding(8, 8, 8, 8);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(154, 38);
            this.textBox1.TabIndex = 166;
            this.textBox1.Text = "35";
            // 
            // button1
            // 
            this.button1.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.button1.Location = new System.Drawing.Point(426, 327);
            this.button1.Margin = new System.Windows.Forms.Padding(8, 8, 8, 8);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(272, 54);
            this.button1.TabIndex = 165;
            this.button1.Text = "Center of the zone";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // bClear
            // 
            this.bClear.Location = new System.Drawing.Point(944, 326);
            this.bClear.Margin = new System.Windows.Forms.Padding(8, 8, 8, 8);
            this.bClear.Name = "bClear";
            this.bClear.Size = new System.Drawing.Size(180, 54);
            this.bClear.TabIndex = 164;
            this.bClear.Text = "Clear";
            this.bClear.UseVisualStyleBackColor = true;
            this.bClear.Click += new System.EventHandler(this.bClear_Click);
            // 
            // bAccept
            // 
            this.bAccept.Location = new System.Drawing.Point(710, 326);
            this.bAccept.Margin = new System.Windows.Forms.Padding(8, 8, 8, 8);
            this.bAccept.Name = "bAccept";
            this.bAccept.Size = new System.Drawing.Size(222, 54);
            this.bAccept.TabIndex = 163;
            this.bAccept.Text = "Accept";
            this.bAccept.UseVisualStyleBackColor = true;
            this.bAccept.Click += new System.EventHandler(this.bAccept_Click);
            // 
            // lDistSightRange
            // 
            this.lDistSightRange.AutoSize = true;
            this.lDistSightRange.Location = new System.Drawing.Point(10, 339);
            this.lDistSightRange.Margin = new System.Windows.Forms.Padding(8, 0, 8, 0);
            this.lDistSightRange.Name = "lDistSightRange";
            this.lDistSightRange.Size = new System.Drawing.Size(181, 32);
            this.lDistSightRange.TabIndex = 157;
            this.lDistSightRange.Text = "Radius, m.....";
            // 
            // tbDistSightRange
            // 
            this.tbDistSightRange.BackColor = System.Drawing.SystemColors.HighlightText;
            this.tbDistSightRange.Location = new System.Drawing.Point(240, 331);
            this.tbDistSightRange.Margin = new System.Windows.Forms.Padding(8, 8, 8, 8);
            this.tbDistSightRange.Name = "tbDistSightRange";
            this.tbDistSightRange.Size = new System.Drawing.Size(170, 38);
            this.tbDistSightRange.TabIndex = 155;
            // 
            // pCoordPoint
            // 
            this.pCoordPoint.Controls.Add(this.gbRect);
            this.pCoordPoint.Controls.Add(this.gbRad);
            this.pCoordPoint.Controls.Add(this.tbOwnHeight);
            this.pCoordPoint.Controls.Add(this.lOwnHeight);
            this.pCoordPoint.Location = new System.Drawing.Point(10, 72);
            this.pCoordPoint.Margin = new System.Windows.Forms.Padding(8, 8, 8, 8);
            this.pCoordPoint.Name = "pCoordPoint";
            this.pCoordPoint.Size = new System.Drawing.Size(462, 246);
            this.pCoordPoint.TabIndex = 138;
            // 
            // gbRect
            // 
            this.gbRect.Controls.Add(this.tbYRect);
            this.gbRect.Controls.Add(this.lYRect);
            this.gbRect.Controls.Add(this.tbXRect);
            this.gbRect.Controls.Add(this.lXRect);
            this.gbRect.Location = new System.Drawing.Point(18, 72);
            this.gbRect.Margin = new System.Windows.Forms.Padding(8, 8, 8, 8);
            this.gbRect.Name = "gbRect";
            this.gbRect.Padding = new System.Windows.Forms.Padding(8, 8, 8, 8);
            this.gbRect.Size = new System.Drawing.Size(426, 151);
            this.gbRect.TabIndex = 17;
            this.gbRect.TabStop = false;
            this.gbRect.Visible = false;
            // 
            // tbYRect
            // 
            this.tbYRect.BackColor = System.Drawing.SystemColors.Window;
            this.tbYRect.Location = new System.Drawing.Point(208, 85);
            this.tbYRect.Margin = new System.Windows.Forms.Padding(8, 8, 8, 8);
            this.tbYRect.MaxLength = 7;
            this.tbYRect.Name = "tbYRect";
            this.tbYRect.ReadOnly = true;
            this.tbYRect.Size = new System.Drawing.Size(194, 38);
            this.tbYRect.TabIndex = 5;
            // 
            // lYRect
            // 
            this.lYRect.AutoSize = true;
            this.lYRect.Location = new System.Drawing.Point(10, 101);
            this.lYRect.Margin = new System.Windows.Forms.Padding(8, 0, 8, 0);
            this.lYRect.Name = "lYRect";
            this.lYRect.Size = new System.Drawing.Size(181, 32);
            this.lYRect.TabIndex = 6;
            this.lYRect.Text = "Long, deg.....";
            // 
            // tbXRect
            // 
            this.tbXRect.BackColor = System.Drawing.SystemColors.Window;
            this.tbXRect.Location = new System.Drawing.Point(208, 31);
            this.tbXRect.Margin = new System.Windows.Forms.Padding(8, 8, 8, 8);
            this.tbXRect.MaxLength = 7;
            this.tbXRect.Name = "tbXRect";
            this.tbXRect.ReadOnly = true;
            this.tbXRect.Size = new System.Drawing.Size(194, 38);
            this.tbXRect.TabIndex = 4;
            // 
            // lXRect
            // 
            this.lXRect.AutoSize = true;
            this.lXRect.Location = new System.Drawing.Point(10, 48);
            this.lXRect.Margin = new System.Windows.Forms.Padding(8, 0, 8, 0);
            this.lXRect.Name = "lXRect";
            this.lXRect.Size = new System.Drawing.Size(181, 32);
            this.lXRect.TabIndex = 3;
            this.lXRect.Text = "Lat, deg........";
            // 
            // gbRad
            // 
            this.gbRad.Controls.Add(this.tbLRad);
            this.gbRad.Controls.Add(this.lLRad);
            this.gbRad.Controls.Add(this.tbBRad);
            this.gbRad.Controls.Add(this.lBRad);
            this.gbRad.Location = new System.Drawing.Point(2, 89);
            this.gbRad.Margin = new System.Windows.Forms.Padding(8, 8, 8, 8);
            this.gbRad.Name = "gbRad";
            this.gbRad.Padding = new System.Windows.Forms.Padding(8, 8, 8, 8);
            this.gbRad.Size = new System.Drawing.Size(426, 151);
            this.gbRad.TabIndex = 28;
            this.gbRad.TabStop = false;
            this.gbRad.Visible = false;
            // 
            // tbLRad
            // 
            this.tbLRad.Location = new System.Drawing.Point(208, 85);
            this.tbLRad.Margin = new System.Windows.Forms.Padding(8, 8, 8, 8);
            this.tbLRad.MaxLength = 10;
            this.tbLRad.Name = "tbLRad";
            this.tbLRad.Size = new System.Drawing.Size(194, 38);
            this.tbLRad.TabIndex = 5;
            // 
            // lLRad
            // 
            this.lLRad.AutoSize = true;
            this.lLRad.Location = new System.Drawing.Point(10, 97);
            this.lLRad.Margin = new System.Windows.Forms.Padding(8, 0, 8, 0);
            this.lLRad.Name = "lLRad";
            this.lLRad.Size = new System.Drawing.Size(245, 32);
            this.lLRad.TabIndex = 6;
            this.lLRad.Text = "L, рад...................";
            // 
            // tbBRad
            // 
            this.tbBRad.Location = new System.Drawing.Point(208, 31);
            this.tbBRad.Margin = new System.Windows.Forms.Padding(8, 8, 8, 8);
            this.tbBRad.MaxLength = 10;
            this.tbBRad.Name = "tbBRad";
            this.tbBRad.Size = new System.Drawing.Size(194, 38);
            this.tbBRad.TabIndex = 4;
            // 
            // lBRad
            // 
            this.lBRad.AutoSize = true;
            this.lBRad.Location = new System.Drawing.Point(10, 43);
            this.lBRad.Margin = new System.Windows.Forms.Padding(8, 0, 8, 0);
            this.lBRad.Name = "lBRad";
            this.lBRad.Size = new System.Drawing.Size(248, 32);
            this.lBRad.TabIndex = 3;
            this.lBRad.Text = "B, рад...................";
            // 
            // tbOwnHeight
            // 
            this.tbOwnHeight.BackColor = System.Drawing.SystemColors.HighlightText;
            this.tbOwnHeight.Location = new System.Drawing.Point(230, 12);
            this.tbOwnHeight.Margin = new System.Windows.Forms.Padding(8, 8, 8, 8);
            this.tbOwnHeight.Name = "tbOwnHeight";
            this.tbOwnHeight.ReadOnly = true;
            this.tbOwnHeight.Size = new System.Drawing.Size(198, 38);
            this.tbOwnHeight.TabIndex = 152;
            // 
            // lOwnHeight
            // 
            this.lOwnHeight.AutoSize = true;
            this.lOwnHeight.Location = new System.Drawing.Point(24, 17);
            this.lOwnHeight.Margin = new System.Windows.Forms.Padding(8, 0, 8, 0);
            this.lOwnHeight.Name = "lOwnHeight";
            this.lOwnHeight.Size = new System.Drawing.Size(184, 32);
            this.lOwnHeight.TabIndex = 153;
            this.lOwnHeight.Text = "H, m..............";
            // 
            // lCenterLSR
            // 
            this.lCenterLSR.AutoSize = true;
            this.lCenterLSR.Location = new System.Drawing.Point(10, 14);
            this.lCenterLSR.Margin = new System.Windows.Forms.Padding(8, 0, 8, 0);
            this.lCenterLSR.Name = "lCenterLSR";
            this.lCenterLSR.Size = new System.Drawing.Size(246, 32);
            this.lCenterLSR.TabIndex = 137;
            this.lCenterLSR.Text = "Center of the zone";
            // 
            // cbCenterLSR
            // 
            this.cbCenterLSR.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbCenterLSR.FormattingEnabled = true;
            this.cbCenterLSR.Items.AddRange(new object[] {
            "X,Y",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            ""});
            this.cbCenterLSR.Location = new System.Drawing.Point(290, 8);
            this.cbCenterLSR.Margin = new System.Windows.Forms.Padding(8, 8, 8, 8);
            this.cbCenterLSR.Name = "cbCenterLSR";
            this.cbCenterLSR.Size = new System.Drawing.Size(166, 39);
            this.cbCenterLSR.TabIndex = 134;
            this.cbCenterLSR.SelectedIndexChanged += new System.EventHandler(this.cbCenterLSR_SelectedIndexChanged);
            // 
            // cbChooseSC
            // 
            this.cbChooseSC.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbChooseSC.DropDownWidth = 150;
            this.cbChooseSC.FormattingEnabled = true;
            this.cbChooseSC.Items.AddRange(new object[] {
            "Метры на мест-ти",
            "Радианы",
            "Градусы",
            "Град, мин, сек"});
            this.cbChooseSC.Location = new System.Drawing.Point(654, 961);
            this.cbChooseSC.Margin = new System.Windows.Forms.Padding(8, 8, 8, 8);
            this.cbChooseSC.Name = "cbChooseSC";
            this.cbChooseSC.Size = new System.Drawing.Size(322, 39);
            this.cbChooseSC.TabIndex = 11;
            this.cbChooseSC.Visible = false;
            this.cbChooseSC.SelectedIndexChanged += new System.EventHandler(this.cbChooseSC_SelectedIndexChanged);
            // 
            // lChooseSC
            // 
            this.lChooseSC.AutoSize = true;
            this.lChooseSC.Location = new System.Drawing.Point(618, 909);
            this.lChooseSC.Margin = new System.Windows.Forms.Padding(8, 0, 8, 0);
            this.lChooseSC.Name = "lChooseSC";
            this.lChooseSC.Size = new System.Drawing.Size(93, 32);
            this.lChooseSC.TabIndex = 12;
            this.lChooseSC.Text = "СК.....";
            this.lChooseSC.Visible = false;
            // 
            // gbDegMinSec
            // 
            this.gbDegMinSec.Controls.Add(this.tbLSec);
            this.gbDegMinSec.Controls.Add(this.tbBSec);
            this.gbDegMinSec.Controls.Add(this.lMin4);
            this.gbDegMinSec.Controls.Add(this.lMin3);
            this.gbDegMinSec.Controls.Add(this.tbLMin2);
            this.gbDegMinSec.Controls.Add(this.tbBMin2);
            this.gbDegMinSec.Controls.Add(this.lSec2);
            this.gbDegMinSec.Controls.Add(this.lSec1);
            this.gbDegMinSec.Controls.Add(this.lDeg4);
            this.gbDegMinSec.Controls.Add(this.lDeg3);
            this.gbDegMinSec.Controls.Add(this.tbLDeg2);
            this.gbDegMinSec.Controls.Add(this.lLDegMinSec);
            this.gbDegMinSec.Controls.Add(this.tbBDeg2);
            this.gbDegMinSec.Controls.Add(this.lBDegMinSec);
            this.gbDegMinSec.Location = new System.Drawing.Point(80, 672);
            this.gbDegMinSec.Margin = new System.Windows.Forms.Padding(8, 8, 8, 8);
            this.gbDegMinSec.Name = "gbDegMinSec";
            this.gbDegMinSec.Padding = new System.Windows.Forms.Padding(8, 8, 8, 8);
            this.gbDegMinSec.Size = new System.Drawing.Size(422, 141);
            this.gbDegMinSec.TabIndex = 30;
            this.gbDegMinSec.TabStop = false;
            this.gbDegMinSec.Visible = false;
            // 
            // tbLSec
            // 
            this.tbLSec.Location = new System.Drawing.Point(318, 81);
            this.tbLSec.Margin = new System.Windows.Forms.Padding(8, 8, 8, 8);
            this.tbLSec.MaxLength = 3;
            this.tbLSec.Name = "tbLSec";
            this.tbLSec.Size = new System.Drawing.Size(60, 38);
            this.tbLSec.TabIndex = 23;
            // 
            // tbBSec
            // 
            this.tbBSec.Location = new System.Drawing.Point(318, 31);
            this.tbBSec.Margin = new System.Windows.Forms.Padding(8, 8, 8, 8);
            this.tbBSec.MaxLength = 3;
            this.tbBSec.Name = "tbBSec";
            this.tbBSec.Size = new System.Drawing.Size(60, 38);
            this.tbBSec.TabIndex = 18;
            // 
            // lMin4
            // 
            this.lMin4.AutoSize = true;
            this.lMin4.Location = new System.Drawing.Point(294, 85);
            this.lMin4.Margin = new System.Windows.Forms.Padding(8, 0, 8, 0);
            this.lMin4.Name = "lMin4";
            this.lMin4.Size = new System.Drawing.Size(19, 32);
            this.lMin4.TabIndex = 27;
            this.lMin4.Text = "\'";
            // 
            // lMin3
            // 
            this.lMin3.AutoSize = true;
            this.lMin3.Location = new System.Drawing.Point(294, 33);
            this.lMin3.Margin = new System.Windows.Forms.Padding(8, 0, 8, 0);
            this.lMin3.Name = "lMin3";
            this.lMin3.Size = new System.Drawing.Size(19, 32);
            this.lMin3.TabIndex = 26;
            this.lMin3.Text = "\'";
            // 
            // tbLMin2
            // 
            this.tbLMin2.Location = new System.Drawing.Point(224, 81);
            this.tbLMin2.Margin = new System.Windows.Forms.Padding(8, 8, 8, 8);
            this.tbLMin2.MaxLength = 2;
            this.tbLMin2.Name = "tbLMin2";
            this.tbLMin2.Size = new System.Drawing.Size(60, 38);
            this.tbLMin2.TabIndex = 22;
            // 
            // tbBMin2
            // 
            this.tbBMin2.Location = new System.Drawing.Point(224, 31);
            this.tbBMin2.Margin = new System.Windows.Forms.Padding(8, 8, 8, 8);
            this.tbBMin2.MaxLength = 2;
            this.tbBMin2.Name = "tbBMin2";
            this.tbBMin2.Size = new System.Drawing.Size(60, 38);
            this.tbBMin2.TabIndex = 16;
            // 
            // lSec2
            // 
            this.lSec2.AutoSize = true;
            this.lSec2.Location = new System.Drawing.Point(386, 83);
            this.lSec2.Margin = new System.Windows.Forms.Padding(8, 0, 8, 0);
            this.lSec2.Name = "lSec2";
            this.lSec2.Size = new System.Drawing.Size(19, 32);
            this.lSec2.TabIndex = 25;
            this.lSec2.Text = "\'";
            // 
            // lSec1
            // 
            this.lSec1.AutoSize = true;
            this.lSec1.Location = new System.Drawing.Point(386, 35);
            this.lSec1.Margin = new System.Windows.Forms.Padding(8, 0, 8, 0);
            this.lSec1.Name = "lSec1";
            this.lSec1.Size = new System.Drawing.Size(19, 32);
            this.lSec1.TabIndex = 24;
            this.lSec1.Text = "\'";
            // 
            // lDeg4
            // 
            this.lDeg4.AutoSize = true;
            this.lDeg4.Location = new System.Drawing.Point(184, 85);
            this.lDeg4.Margin = new System.Windows.Forms.Padding(8, 0, 8, 0);
            this.lDeg4.Name = "lDeg4";
            this.lDeg4.Size = new System.Drawing.Size(27, 32);
            this.lDeg4.TabIndex = 21;
            this.lDeg4.Text = "^";
            // 
            // lDeg3
            // 
            this.lDeg3.AutoSize = true;
            this.lDeg3.Location = new System.Drawing.Point(184, 33);
            this.lDeg3.Margin = new System.Windows.Forms.Padding(8, 0, 8, 0);
            this.lDeg3.Name = "lDeg3";
            this.lDeg3.Size = new System.Drawing.Size(27, 32);
            this.lDeg3.TabIndex = 20;
            this.lDeg3.Text = "^";
            // 
            // tbLDeg2
            // 
            this.tbLDeg2.Location = new System.Drawing.Point(114, 76);
            this.tbLDeg2.Margin = new System.Windows.Forms.Padding(8, 8, 8, 8);
            this.tbLDeg2.MaxLength = 2;
            this.tbLDeg2.Name = "tbLDeg2";
            this.tbLDeg2.Size = new System.Drawing.Size(60, 38);
            this.tbLDeg2.TabIndex = 19;
            // 
            // lLDegMinSec
            // 
            this.lLDegMinSec.AutoSize = true;
            this.lLDegMinSec.Location = new System.Drawing.Point(10, 97);
            this.lLDegMinSec.Margin = new System.Windows.Forms.Padding(8, 0, 8, 0);
            this.lLDegMinSec.Name = "lLDegMinSec";
            this.lLDegMinSec.Size = new System.Drawing.Size(166, 32);
            this.lLDegMinSec.TabIndex = 17;
            this.lLDegMinSec.Text = "L.................";
            // 
            // tbBDeg2
            // 
            this.tbBDeg2.Location = new System.Drawing.Point(114, 35);
            this.tbBDeg2.Margin = new System.Windows.Forms.Padding(8, 8, 8, 8);
            this.tbBDeg2.MaxLength = 2;
            this.tbBDeg2.Name = "tbBDeg2";
            this.tbBDeg2.Size = new System.Drawing.Size(60, 38);
            this.tbBDeg2.TabIndex = 15;
            // 
            // lBDegMinSec
            // 
            this.lBDegMinSec.AutoSize = true;
            this.lBDegMinSec.Location = new System.Drawing.Point(10, 43);
            this.lBDegMinSec.Margin = new System.Windows.Forms.Padding(8, 0, 8, 0);
            this.lBDegMinSec.Name = "lBDegMinSec";
            this.lBDegMinSec.Size = new System.Drawing.Size(153, 32);
            this.lBDegMinSec.TabIndex = 14;
            this.lBDegMinSec.Text = "B...............";
            // 
            // gbRect42
            // 
            this.gbRect42.Controls.Add(this.tbYRect42);
            this.gbRect42.Controls.Add(this.lYRect42);
            this.gbRect42.Controls.Add(this.tbXRect42);
            this.gbRect42.Controls.Add(this.lXRect42);
            this.gbRect42.Location = new System.Drawing.Point(88, 860);
            this.gbRect42.Margin = new System.Windows.Forms.Padding(8, 8, 8, 8);
            this.gbRect42.Name = "gbRect42";
            this.gbRect42.Padding = new System.Windows.Forms.Padding(8, 8, 8, 8);
            this.gbRect42.Size = new System.Drawing.Size(426, 151);
            this.gbRect42.TabIndex = 27;
            this.gbRect42.TabStop = false;
            this.gbRect42.Visible = false;
            // 
            // tbYRect42
            // 
            this.tbYRect42.Location = new System.Drawing.Point(208, 85);
            this.tbYRect42.Margin = new System.Windows.Forms.Padding(8, 8, 8, 8);
            this.tbYRect42.MaxLength = 7;
            this.tbYRect42.Name = "tbYRect42";
            this.tbYRect42.Size = new System.Drawing.Size(194, 38);
            this.tbYRect42.TabIndex = 5;
            // 
            // lYRect42
            // 
            this.lYRect42.AutoSize = true;
            this.lYRect42.Location = new System.Drawing.Point(10, 101);
            this.lYRect42.Margin = new System.Windows.Forms.Padding(8, 0, 8, 0);
            this.lYRect42.Name = "lYRect42";
            this.lYRect42.Size = new System.Drawing.Size(220, 32);
            this.lYRect42.TabIndex = 6;
            this.lYRect42.Text = "Y, м...................";
            // 
            // tbXRect42
            // 
            this.tbXRect42.Location = new System.Drawing.Point(208, 31);
            this.tbXRect42.Margin = new System.Windows.Forms.Padding(8, 8, 8, 8);
            this.tbXRect42.MaxLength = 7;
            this.tbXRect42.Name = "tbXRect42";
            this.tbXRect42.Size = new System.Drawing.Size(194, 38);
            this.tbXRect42.TabIndex = 4;
            // 
            // lXRect42
            // 
            this.lXRect42.AutoSize = true;
            this.lXRect42.Location = new System.Drawing.Point(10, 48);
            this.lXRect42.Margin = new System.Windows.Forms.Padding(8, 0, 8, 0);
            this.lXRect42.Name = "lXRect42";
            this.lXRect42.Size = new System.Drawing.Size(220, 32);
            this.lXRect42.TabIndex = 3;
            this.lXRect42.Text = "X, м...................";
            // 
            // gbDegMin
            // 
            this.gbDegMin.Controls.Add(this.tbLMin1);
            this.gbDegMin.Controls.Add(this.tbBMin1);
            this.gbDegMin.Controls.Add(this.lLDegMin);
            this.gbDegMin.Controls.Add(this.lBDegMin);
            this.gbDegMin.Location = new System.Drawing.Point(546, 672);
            this.gbDegMin.Margin = new System.Windows.Forms.Padding(8, 8, 8, 8);
            this.gbDegMin.Name = "gbDegMin";
            this.gbDegMin.Padding = new System.Windows.Forms.Padding(8, 8, 8, 8);
            this.gbDegMin.Size = new System.Drawing.Size(400, 151);
            this.gbDegMin.TabIndex = 29;
            this.gbDegMin.TabStop = false;
            this.gbDegMin.Visible = false;
            // 
            // tbLMin1
            // 
            this.tbLMin1.Location = new System.Drawing.Point(174, 91);
            this.tbLMin1.Margin = new System.Windows.Forms.Padding(8, 8, 8, 8);
            this.tbLMin1.MaxLength = 8;
            this.tbLMin1.Name = "tbLMin1";
            this.tbLMin1.Size = new System.Drawing.Size(190, 38);
            this.tbLMin1.TabIndex = 15;
            // 
            // tbBMin1
            // 
            this.tbBMin1.Location = new System.Drawing.Point(174, 35);
            this.tbBMin1.Margin = new System.Windows.Forms.Padding(8, 8, 8, 8);
            this.tbBMin1.MaxLength = 8;
            this.tbBMin1.Name = "tbBMin1";
            this.tbBMin1.Size = new System.Drawing.Size(190, 38);
            this.tbBMin1.TabIndex = 13;
            // 
            // lLDegMin
            // 
            this.lLDegMin.AutoSize = true;
            this.lLDegMin.Location = new System.Drawing.Point(10, 97);
            this.lLDegMin.Margin = new System.Windows.Forms.Padding(8, 0, 8, 0);
            this.lLDegMin.Name = "lLDegMin";
            this.lLDegMin.Size = new System.Drawing.Size(166, 32);
            this.lLDegMin.TabIndex = 12;
            this.lLDegMin.Text = "L.................";
            // 
            // lBDegMin
            // 
            this.lBDegMin.AutoSize = true;
            this.lBDegMin.Location = new System.Drawing.Point(10, 43);
            this.lBDegMin.Margin = new System.Windows.Forms.Padding(8, 0, 8, 0);
            this.lBDegMin.Name = "lBDegMin";
            this.lBDegMin.Size = new System.Drawing.Size(153, 32);
            this.lBDegMin.TabIndex = 10;
            this.lBDegMin.Text = "B...............";
            // 
            // FormSupSpuf
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(16F, 31F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1132, 399);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.gbDegMin);
            this.Controls.Add(this.tbOpponentAntenna);
            this.Controls.Add(this.lChooseSC);
            this.Controls.Add(this.label17);
            this.Controls.Add(this.cbChooseSC);
            this.Controls.Add(this.tbHAnt);
            this.Controls.Add(this.gbRect42);
            this.Controls.Add(this.powerTextbox);
            this.Controls.Add(this.gbDegMinSec);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.comboBox2);
            this.Controls.Add(this.lCenterLSR);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.cbCenterLSR);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.pCoordPoint);
            this.Controls.Add(this.tbDistSightRange);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.lDistSightRange);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.bAccept);
            this.Controls.Add(this.bClear);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Location = new System.Drawing.Point(900, 100);
            this.Margin = new System.Windows.Forms.Padding(8, 8, 8, 8);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FormSupSpuf";
            this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
            this.Text = "Navigation jamming zone";
            this.TopMost = true;
            this.Activated += new System.EventHandler(this.FormSupSpuf_Activated);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FormSupSpuf_FormClosing);
            this.Load += new System.EventHandler(this.FormSupSpuf_Load);
            this.pCoordPoint.ResumeLayout(false);
            this.pCoordPoint.PerformLayout();
            this.gbRect.ResumeLayout(false);
            this.gbRect.PerformLayout();
            this.gbRad.ResumeLayout(false);
            this.gbRad.PerformLayout();
            this.gbDegMinSec.ResumeLayout(false);
            this.gbDegMinSec.PerformLayout();
            this.gbRect42.ResumeLayout(false);
            this.gbRect42.PerformLayout();
            this.gbDegMin.ResumeLayout(false);
            this.gbDegMin.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Panel pCoordPoint;
        public System.Windows.Forms.GroupBox gbDegMinSec;
        public System.Windows.Forms.TextBox tbLSec;
        public System.Windows.Forms.TextBox tbBSec;
        private System.Windows.Forms.Label lMin4;
        private System.Windows.Forms.Label lMin3;
        public System.Windows.Forms.TextBox tbLMin2;
        public System.Windows.Forms.TextBox tbBMin2;
        private System.Windows.Forms.Label lSec2;
        private System.Windows.Forms.Label lSec1;
        private System.Windows.Forms.Label lDeg4;
        private System.Windows.Forms.Label lDeg3;
        public System.Windows.Forms.TextBox tbLDeg2;
        private System.Windows.Forms.Label lLDegMinSec;
        public System.Windows.Forms.TextBox tbBDeg2;
        private System.Windows.Forms.Label lBDegMinSec;
        public System.Windows.Forms.GroupBox gbRect42;
        public System.Windows.Forms.TextBox tbYRect42;
        private System.Windows.Forms.Label lYRect42;
        public System.Windows.Forms.TextBox tbXRect42;
        private System.Windows.Forms.Label lXRect42;
        public System.Windows.Forms.GroupBox gbDegMin;
        public System.Windows.Forms.TextBox tbLMin1;
        public System.Windows.Forms.TextBox tbBMin1;
        private System.Windows.Forms.Label lLDegMin;
        private System.Windows.Forms.Label lBDegMin;
        public System.Windows.Forms.GroupBox gbRect;
        public System.Windows.Forms.TextBox tbYRect;
        public System.Windows.Forms.TextBox tbXRect;
        public System.Windows.Forms.ComboBox cbChooseSC;
        private System.Windows.Forms.Label lChooseSC;
        public System.Windows.Forms.GroupBox gbRad;
        public System.Windows.Forms.TextBox tbLRad;
        private System.Windows.Forms.Label lLRad;
        public System.Windows.Forms.TextBox tbBRad;
        private System.Windows.Forms.Label lBRad;
        public System.Windows.Forms.ComboBox cbCenterLSR;
        private System.Windows.Forms.TextBox textBox1;
        public System.Windows.Forms.ComboBox comboBox2;
        public System.Windows.Forms.TextBox tbOwnHeight;
        public System.Windows.Forms.TextBox tbDistSightRange;
        private System.Windows.Forms.TextBox powerTextbox;
        public System.Windows.Forms.TextBox tbHAnt;
        public System.Windows.Forms.TextBox tbOpponentAntenna;
        public System.Windows.Forms.Button button1;
        public System.Windows.Forms.Button bClear;
        public System.Windows.Forms.Button bAccept;
        public System.Windows.Forms.Label lDistSightRange;
        public System.Windows.Forms.Label lOwnHeight;
        public System.Windows.Forms.Label lYRect;
        public System.Windows.Forms.Label lXRect;
        public System.Windows.Forms.Label lCenterLSR;
        public System.Windows.Forms.Label label3;
        public System.Windows.Forms.Label label2;
        public System.Windows.Forms.Label label4;
        public System.Windows.Forms.Label label17;
        public System.Windows.Forms.Label label11;
    }
}