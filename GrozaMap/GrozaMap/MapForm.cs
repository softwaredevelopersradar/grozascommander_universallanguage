﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Runtime.InteropServices;
using System.Windows.Forms;
using System.IO;
using System.Windows.Input;

using System.IO;
using System.Windows.Input;
using Mapsui.Geometries;
using Mapsui.Styles;
using Mapsui.Projection;
using WpfMapControl;

using System.Globalization;
using System.Threading.Tasks;

using PC_DLL;
using System.Reflection;

using ClassLibraryPeleng;
//using GeoCalculator;
// ------------------------------------------------------------------

namespace GrozaMap
{

    public partial class MapForm : Form
    {

        // ------------------------------------------------------------------
        public static RasterMapControl RasterMapControl { get; private set; }

       
        // ------------------------------------------------------------------

        [DllImport("user32.dll")]
        static extern IntPtr GetForegroundWindow();
        // ------------------------------------------------------------------
        // EVENTS

        // sob
        public delegate void UpdateTableReconFWSEventHandler(TReconFWS[] tReconFWS);
        public event UpdateTableReconFWSEventHandler UpdateTableReconFWS;
        // ------------------------------------------------------------------
        // ???????????????????
        // Ybrat

        private bool MapIsOpenned
        {
            get { return RasterMapControl.IsMapLoaded; }
        }
        private bool HeightMatrixIsOpenned = false;
        // ------------------------------------------------------------------

        // Переменные VAR VAR VAR VAR VAR VAR VAR VAR VAR VAR VAR VAR VAR VAR VAR 

        // ------------------------------------------------------------------
        // Создаем объекты форм (инициализируются при загрузке головной формы)
        // для обращения из других файлов

        public FormWay objFormWay;
        public FormSP objFormSP;
        public FormLF objFormLF;
        public FormLF2 objFormLF2;
        public FormZO objFormZO;
        public FormTRO objFormTRO;
        public FormOB1 objFormOB1;
        public FormOB2 objFormOB2;
        public FormSuppression objFormSuppression;
        public FormSPFB objFormSPFB;
        public FormSPFB1 objFormSPFB1;
        public FormSPFB2 objFormSPFB2;
        public FormAz1 objFormAz1;
        public FormLineSightRange objFormLineSightRange;
        public FormSupSpuf objFormSupSpuf;
        public FormTab objFormTab;
        public FormSpuf objFormSpuf;
        public FormScreen objFormScreen;
        // ------------------------------------------------------------------
        // ???????????????????? Убрать
        public static int hmapl1;
        // ------------------------------------------------------------------
        // ?????????????????? Убрать
        public static bool blAirObject = false;
        // ------------------------------------------------------------------

        string path = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location);
        // ------------------------------------------------------------------
        // create client
        public ClientPC clientPC;// = new ClientPC(1);

        
        // ------------------------------------------------------------------

        // VAR VAR VAR VAR VAR VAR VAR VAR VAR VAR VAR VAR VAR VAR VAR Переменные

        // КОНСТРУКТОР **********************************************************
        public MapForm()
        {
            // ------------------------------------------------------------------
            InitializeComponent();
            // ------------------------------------------------------------------

            // Там, где лежит карта
            RasterMapControl = new RasterMapControl();
            MapElementHost.Child = RasterMapControl;

            RasterMapControl.DataFolder = path + "\\data";
            //RasterMapControl.DataFolder = "F:/GrozaS/Update GrozaS/Other/Maps/Tiles";

            RasterMapControl.MouseEnter += RasterMapControl_MouseEnter;
            RasterMapControl.MapMouseMoveEvent += OnMapMapMouseMove;
            RasterMapControl.MapClickEvent += OnMapClick;
            // ------------------------------------------------------------------
            // Объекты форм (инициализируются при загрузке головной формы)
            // для обращения из других файлов

            objFormWay = new FormWay();
            objFormSP = new FormSP();
            objFormLF = new FormLF();
            objFormLF2 = new FormLF2();
            objFormZO = new FormZO();
            objFormTRO = new FormTRO();
            objFormOB1 = new FormOB1();
            objFormOB2 = new FormOB2();
            objFormSuppression = new FormSuppression();
            objFormSPFB = new FormSPFB();
            objFormSPFB1 = new FormSPFB1();
            objFormSPFB2 = new FormSPFB2(this);
            objFormAz1 = new FormAz1();
            objFormLineSightRange = new FormLineSightRange();
            objFormSupSpuf = new FormSupSpuf();
            objFormTab = new FormTab(this);
            // seg1
            objFormSpuf = new FormSpuf();
            objFormScreen = new FormScreen(this);
            // ------------------------------------------------------------------
            // IPAddress, Port, AdressOwn, AdressLinked from Setting.ini

            if (System.IO.File.Exists(Application.StartupPath + "\\Setting.ini"))
            {
                string sIPAddress = iniRW.get_IPaddress();
                tbIP.Text = sIPAddress;

                int iPort = iniRW.get_Port();
                tbPort.Text = iPort.ToString();

                GlobalVarLn.AdressOwn = iniRW.get_AdressOwn();
                GlobalVarLn.AdressLinked = iniRW.get_AdressLinked();

            }
            else
            {
                if (GlobalVarLn.Lang == "Eng")
                    MessageBox.Show("Can’t open INI file! \n", "", MessageBoxButtons.OK, MessageBoxIcon.Error);
                else
                    if (GlobalVarLn.Lang == "Az")
                    MessageBox.Show("Faylı açmaq mümkün deyil INI \n", "", MessageBoxButtons.OK, MessageBoxIcon.Error);
                else
                    if (GlobalVarLn.Lang == "Srb")
                        MessageBox.Show("Није могуће отворити INI документ \n", "", MessageBoxButtons.OK, MessageBoxIcon.Error);
                else
                    MessageBox.Show("Невозможно открыть файл INI! \n", "", MessageBoxButtons.OK, MessageBoxIcon.Error);


            }
            // ------------------------------------------------------------------

            // MAP,MATRIX RESOLUTION *****************************************************************
            // CLASS

            string s = "";
            s=ClassMapRastrMenu.f_OpenMap(
                                        MapForm.RasterMapControl,
                                        ref GlobalVarLn.MapOpened
                                        );
            if(s!="")
                MessageBox.Show(s);

            s=ClassMapRastrMenu.f_OpenMatrix(
                                        MapForm.RasterMapControl,
                                        ref GlobalVarLn.MatrixOpened
                                            );
            if (s != "")
                MessageBox.Show(s);

            s=ClassMapRastrMenu.f_GetResolution(
                                        MapForm.RasterMapControl
                                               );
            if (s != "")
                MessageBox.Show(s);

            // ***************************************************************** MAP,MATRIX RESOLUTION

            // .......................................................................................
            /*
                        /// WCF
                        //создаем сервис
                        var service = new ServiceImplementation();
                        //подписываемся на событие HelloReceived
                        service.AirPlaneReceived += Service_AirPlaneReceived;
                        //стартуем сервер
                        var svh = new ServiceHost(service);
                        svh.AddServiceEndpoint(typeof(Contract.IService), new NetTcpBinding(), "net.tcp://localhost:8000");
                        svh.Open();
             */
            // ........................................................................................


        }

        // *******************
        //0206* ?????
        void RasterMapControl_MouseEnter(object sender, System.Windows.Input.MouseEventArgs e)
        {
            RasterMapControl.InnerMapControl.Focus();
        }
        // ********************

        // ********************************************************************* КОНСТРУКТОР

        // GPS ******************************************************************************
        // Обработчик события

        void clientPC_OnConfirmCoord(object sender, byte bAddress, byte bCodeError, double dLatitudeOwn, double dLongitudeOwn, double dLatitudeLinked, double dLongitudeLinked)
        {
            GlobalVarLn.lt1sp = dLatitudeOwn;
            GlobalVarLn.ln1sp = dLongitudeOwn;
            GlobalVarLn.lt2sp = dLatitudeLinked;
            GlobalVarLn.ln2sp = dLongitudeLinked;

            // Otladka

/*
            double x1 = 2382464;
            double y1 = 5498821;
            mapPlaneToGeo(GlobalVarLn.hmapl, ref x1, ref y1);
            GlobalVarLn.lt1sp = x1 * 180 / Math.PI; // rad->grad
            GlobalVarLn.ln1sp = y1 * 180 / Math.PI;

            double x3 = 2240316;
            double y4 = 5551638;
            mapPlaneToGeo(GlobalVarLn.hmapl, ref x3, ref y4);
            GlobalVarLn.lt2sp = x3 * 180 / Math.PI; // rad->grad
            GlobalVarLn.ln2sp = y4 * 180 / Math.PI;
 */

            GlobalVarLn.flagGPS_SP = 1;
            GlobalVarLn.objFormSPG.BeginInvoke(
                (MethodInvoker) (() => GlobalVarLn.objFormSPG.AcceptGPS()));
        }
        // ****************************************************************************** GPS

        // Обновление листа СП **************************************************************

        public static void UpdateDropDownList(ComboBox dropDownList)
        {
            var stations = GlobalVarLn.listJS;

            var selectedIndex = dropDownList.SelectedIndex;

            dropDownList.Items.Clear();
            dropDownList.Items.Add("X,Y");
            foreach (var station in stations)
            {
                dropDownList.Items.Add(station.Name);
            }
            dropDownList.SelectedIndex = dropDownList.Items.Count > selectedIndex ? selectedIndex : 0;
        }

        public static void UpdateDropDownList1(ComboBox dropDownList)
        {
            var stations = GlobalVarLn.listJS;

            var selectedIndex = dropDownList.SelectedIndex;

            dropDownList.Items.Clear();
            //dropDownList.Items.Add("X,Y");
            foreach (var station in stations)
            {
                dropDownList.Items.Add(station.Name);
            }
            dropDownList.SelectedIndex = dropDownList.Items.Count > selectedIndex ? selectedIndex : 0;
        }
        // ************************************************************** Обновление листа СП

        // OpenMapMenu ********************************************************************************
        // Открытие карты из меню

        private async void openMapToolStripMenuItem1_Click(object sender, EventArgs e)
        {
        // --------------------------------------------------------------------------------------------
         // Путь

         String path = "";
         String s = "";

         DialogResult result;
         result = openFileDialog1.ShowDialog();

        if (result == DialogResult.OK)
         {
             path = openFileDialog1.FileName;
             s = ClassMapRastrMenu.f_OpenMapMenu(
                                                 // Путь к карте
                                                 path,
                                                 // Дочерний элемент MapElementHost -> Элемент, где ложим карту
                                                 MapForm.RasterMapControl,
                                                 ref GlobalVarLn.MapOpened
                                                 );

         } // Dialog=OK

        // В диалоге нажать 'Отмена/Cancel'
        else if (result == DialogResult.Cancel)
         {
           return;

         } // Dialog=Cancel

        else
        {
                if (GlobalVarLn.Lang == "Eng")
                    s = "Can't load map!";
                else if (GlobalVarLn.Lang == "Srb")
                    s = "Није могуће учитати карту!";
                else
                    s = "Невозможно загрузить карту";

            }
            // --------------------------------------------------------------------------------------------
            if (s!="")
           MessageBox.Show(s);
         // --------------------------------------------------------------------------------------------

         } 
        // ******************************************************************************** OpenMapMenu

        // CloseMapMenu *******************************************************************************
        // Закрыть карту и матрицу высот -> пункт меню

        private void closeMapToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            string s = "";
            s=ClassMapRastrMenu.f_CloseMapMenu(
                                     // Дочерний элемент MapElementHost -> Элемент, где ложим карту
                                     MapForm.RasterMapControl,
                                     ref GlobalVarLn.MapOpened,
                                     ref GlobalVarLn.MatrixOpened
                                              );

            if (s != "")
                MessageBox.Show(s);

        }
        // ******************************************************************************* CloseMapMenu

        // OpenMatrixMenu *****************************************************************************
        // Открыть матрицу высот -> пункт меню

        private void openTheHeightMatrixToolStripMenuItem_Click(object sender, EventArgs e)
        {

            // --------------------------------------------------------------------------------------------
            // Путь

            //String path = "";
            String s = "";

            //DialogResult result;
            //result = openFileDialog1.ShowDialog();

            //if (result == DialogResult.OK)
            //{
            //    path = openFileDialog1.FileName;

            //    s = ClassMapRastrMenu.f_OpenMatrixMenu(
            //                                          path,
            //                                          // Дочерний элемент MapElementHost -> Элемент, где ложим карту
            //                                          MapForm.RasterMapControl,
            //                                          ref GlobalVarLn.MatrixOpened
            //                                          );

            //} // Dialog=OK

            //// В диалоге нажать 'Отмена/Cancel'
            //else if (result == DialogResult.Cancel)
            //{
            //    return;

            //} // Dialog=Cancel

            //else
            //{
            //    if (GlobalVarLn.Lang == "Eng")
            //        s = "Can't load the height matrix!";
            //    else
            //        s = "Невозможно загрузить матрицу высот";

            //}









            FolderBrowserDialog fbd = new FolderBrowserDialog();
            fbd.Description = "Custom Description";

            if (fbd.ShowDialog() == DialogResult.OK)
            {
                string sSelectedPath = fbd.SelectedPath;

                s = ClassMapRastrMenu.f_OpenMatrixMenu(
                                                      sSelectedPath,
                                                      // Дочерний элемент MapElementHost -> Элемент, где ложим карту
                                                      MapForm.RasterMapControl,
                                                      ref GlobalVarLn.MatrixOpened
                                                      );
            }
            else
                return;



            // --------------------------------------------------------------------------------------------
            if (s != "")
                MessageBox.Show(s);
            // --------------------------------------------------------------------------------------------

        }
        // OpenMatrixMenu *****************************************************************************

        // Масштаб ************************************************************************************

        //увеличить масштаб
        private void ZoomIn_Click(object sender, EventArgs e)
        {

            ClassMapRastrMenu.f_ResolutionIncrease(
                                               // Дочерний элемент MapElementHost -> Элемент, где ложим карту
                                               MapForm.RasterMapControl
                                                  );
        }

        //уменьшить масштаб
        private void ZoomOut_Click(object sender, EventArgs e)
        {
            ClassMapRastrMenu.f_ResolutionDecrease(
                                               // Дочерний элемент MapElementHost -> Элемент, где ложим карту
                                               MapForm.RasterMapControl
                                                 );

        }

        // исходный масштаб
        private void ZoomInitial_Click(object sender, EventArgs e)
        {

            ClassMapRastrMenu.f_ResolutionBase(
                                               // Дочерний элемент MapElementHost -> Элемент, где ложим карту
                                               MapForm.RasterMapControl
                                              );
        }
       // ************************************************************************************ Масштаб




       // MOUSE_DOWN *********************************************************************************
       //Обработка мыши на карте

        private void OnMapClick(object sender, MapMouseClickEventArgs e)
        {
/*
            // преобразование В меркатор
            var p = Mercator.FromLonLat(e.Location.Longitude, e.Location.Latitude);
            // преобразование из меркатора в wgs84
            var p2 = Mercator.ToLonLat(p.X, p.Y);

            if (RasterMapControl.LastClickedObject != null)
            {
                // if user clicked on a object let's remove it
                RasterMapControl.RemoveObject(RasterMapControl.LastClickedObject);
                return;
            }
            if (RasterMapControl.HoveredObject != null)
            {
                return;
            }
            if (e.ClickedButton.ChangedButton == MouseButton.Left)
            {
                ApplyInstrument(e.Location.ToPoint());
            }
*/

            // ---------------------------------------------------------------------------------
            // NEW
            // 555

            // .................................................................................
            // Geographic

            if (GlobalVarLn.TypeMap == 1)
            {
                if (e.Location.Latitude != null)
                    GlobalVarLn.LAT_Rastr = e.Location.Latitude;

                if (e.Location.Longitude != null)
                    GlobalVarLn.LONG_Rastr = e.Location.Longitude;

                if (e.Location.Altitude != null)
                    GlobalVarLn.H_Rastr = (double)e.Location.Altitude;

                if ((e.Location.Latitude != null) && (e.Location.Longitude != null))
                {
                    // преобразование В меркатор
                    var p = Mercator.FromLonLat(e.Location.Longitude, e.Location.Latitude);
                    GlobalVarLn.X_Rastr = p.X;
                    GlobalVarLn.Y_Rastr = p.Y;
                }

            } // TypeMap==1
            // .................................................................................
            // Mercator

            else
            {
                //if (e.Location.Longitude != null)
                //    GlobalVarLn.X_Rastr = e.Location.Longitude;

                //if (e.Location.Latitude != null)
                //    GlobalVarLn.Y_Rastr = e.Location.Latitude;

                //if (e.Location.Altitude != null)
                //    GlobalVarLn.H_Rastr = (double)e.Location.Altitude;

                ////var p1 = Mercator.ToLonLat(GlobalVarLn.X_Rastr, GlobalVarLn.Y_Rastr);
                //var p1 = Mercator.FromLonLat(GlobalVarLn.X_Rastr, GlobalVarLn.Y_Rastr);



                //GlobalVarLn.LAT_Rastr = p1.Y;
                //GlobalVarLn.LONG_Rastr = p1.X;

                if (e.Location.Longitude != null)
                    GlobalVarLn.LONG_Rastr = e.Location.Longitude;

                if (e.Location.Latitude != null)
                    GlobalVarLn.LAT_Rastr = e.Location.Latitude;

                if (e.Location.Altitude != null)
                    GlobalVarLn.H_Rastr = (double)e.Location.Altitude;

              
                var p1 = Mercator.FromLonLat(e.Location.Longitude, e.Location.Latitude);
                



                GlobalVarLn.X_Rastr = p1.X;
                GlobalVarLn.Y_Rastr = p1.Y;



            } // Mercator
            // .................................................................................

            // ---------------------------------------------------------------------------------
            // OLD

            GlobalVarLn.MousePress = true;

            // SSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSS
            // Для маршрута

            if (GlobalVarLn.fl_Open_objFormWay == 1)
            {
                objFormWay.f_Way(
                               GlobalVarLn.X_Rastr,
                               GlobalVarLn.Y_Rastr
                              );
            }
            // SSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSS


            // -----------------------------------------------------------------------------------
            // SP

            if (GlobalVarLn.fl_Open_objFormSP == 1)
            {

                GlobalVarLn.fclSP = 0;

                objFormSP.f_SP(
                               GlobalVarLn.X_Rastr,
                               GlobalVarLn.Y_Rastr
                              );
            }

            // -----------------------------------------------------------------------------------
            // LF1
            if (GlobalVarLn.fl_Open_objFormLF == 1)
            {
                objFormLF.f_LF1(
                               GlobalVarLn.X_Rastr,
                               GlobalVarLn.Y_Rastr
                              );
            }
            // -----------------------------------------------------------------------------------
            // LF2
            if (GlobalVarLn.fl_Open_objFormLF2 == 1)
            {
                objFormLF2.f_LF2(
                               GlobalVarLn.X_Rastr,
                               GlobalVarLn.Y_Rastr
                              );
            }
            // -----------------------------------------------------------------------------------
            // ZO
            if (GlobalVarLn.fl_Open_objFormZO == 1)
            {
                objFormZO.f_ZO(
                               GlobalVarLn.X_Rastr,
                               GlobalVarLn.Y_Rastr
                              );
            }
            // ---------------------------------------------------------------------------------
            // OB1
            if (GlobalVarLn.fl_Open_objFormOB1 == 1)
            {
                objFormOB1.f_OB1(
                               GlobalVarLn.X_Rastr,
                               GlobalVarLn.Y_Rastr
                              );
            }
            // ---------------------------------------------------------------------------------
            // OB2
            if (GlobalVarLn.fl_Open_objFormOB2 == 1)
            {
                objFormOB2.f_OB2(
                               GlobalVarLn.X_Rastr,
                               GlobalVarLn.Y_Rastr
                              );
            }
            // ---------------------------------------------------------------------------------
            // Az1(Object)

            if (GlobalVarLn.blOB_az1 == true)
            {

                GlobalVarLn.XXXaz = GlobalVarLn.X_Rastr;
                GlobalVarLn.YYYaz = GlobalVarLn.Y_Rastr;

            }
            // ---------------------------------------------------------------------------------
            // SPFBRR

            if (GlobalVarLn.fl_Open_objFormSPFB == 1)
            {
                objFormSPFB.f_SPFB_f1();
            }
            // ---------------------------------------------------------------------------------
            // SPFBPR

            if (GlobalVarLn.fl_Open_objFormSPFB1 == 1)
            {
                objFormSPFB1.f_SPFB_f2();
            }
            // ---------------------------------------------------------------------------------


        } // P/P
          // ********************************************************************************* MOUSE_DOWN

        // MOUSE_MOVE ***********************************************************************************
        // MouseMove

        private void OnMapMapMouseMove(object sender, Location location)
        {

/*
!!! Ne ybirat
 
            // преобразование В меркатор
            // var p = Mercator.FromLonLat(e.Location.Longitude, e.Location.Latitude);
            // преобразование из меркатора в wgs84
            //var p2 = Mercator.ToLonLat(p.X, p.Y);

            double moveX = 0;
            double moveY = 0;

            if ((location.Latitude != null) && (location.Longitude != null))
            {
            //moveX = location.Latitude;
            //moveY = location.Longitude;
            //lLat.Text = string.Format("Latitude = {0}", moveX.ToString("0.000"));
            //lLon.Text = string.Format("Longitude = {0}", moveY.ToString("0.000"));
            }

            Peleng peleng = new Peleng("");
            int iDegreeLat=0;
            int iMinuteLat=0;
            double dSecondLat = 0;
            int iDegreeLong = 0;
            int iMinuteLong = 0;
            double dSecondLong = 0;
            int altitudeSuffix;

            if ((location.Latitude != null) && (location.Longitude != null))
            {
            // преобразование В меркатор
            var p = Mercator.FromLonLat(location.Longitude, location.Latitude);
            moveX = p.X;
            moveY = p.Y;
            //lX.Text = string.Format("X, m = {0}", Convert.ToString((int)moveX));
            //lY.Text = string.Format("Y, m = {0}", Convert.ToString((int)moveY));

            peleng.f_Grad_GMS(location.Latitude, ref iDegreeLat, ref iMinuteLat, ref dSecondLat);
            lLat.Text = string.Format("Latitude: {0}", iDegreeLat.ToString("00") + '\xB0' + " " + iMinuteLat.ToString("00") + "' " + dSecondLat.ToString("00") + "''" + " N");
            peleng.f_Grad_GMS(location.Longitude, ref iDegreeLong, ref iMinuteLong, ref dSecondLong);
            lLon.Text = string.Format("    Longitude: {0}", iDegreeLong.ToString("00") + '\xB0' + " " + iMinuteLong.ToString("00") + "' " + dSecondLong.ToString("00") + "''" + "  E");

            //var ppp1 = location.ToMgrs();

            }

            if (location.Altitude!=null)
            {
            //moveX = (int)location.Altitude;
            //lH.Text = string.Format("H, m = {0}", Convert.ToString((int)moveX));

            altitudeSuffix = (int)location.Altitude;
            lH.Text = string.Format("    Altitude: {0}", altitudeSuffix);
            lX.Text = string.Format("    MGRS: {0}", location.ToMgrs().ToLongString());

            }
*/


// >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
// 555

            double moveX = 0;
            double moveY = 0;

            Peleng peleng = new Peleng("");
            int iDegreeLat = 0;
            int iMinuteLat = 0;
            double dSecondLat = 0;
            int iDegreeLong = 0;
            int iMinuteLong = 0;
            double dSecondLong = 0;
            int altitudeSuffix;
            double lt = 0;
            double lng = 0;

            // .............................................................................
            // H

            if (location.Altitude != null)
            {
                altitudeSuffix = (int)location.Altitude;

                if(GlobalVarLn.Lang=="Eng")
                 lH.Text = string.Format("    Altitude: {0}", altitudeSuffix);
                else if (GlobalVarLn.Lang == "Srb")
                    lH.Text = string.Format("    Висина: {0}", altitudeSuffix);
                else
                    lH.Text = string.Format("    Высота: {0}", altitudeSuffix);

            }
            // .............................................................................
            //  Geographic

            if (GlobalVarLn.TypeMap == 1)
            {
                if ((location.Latitude != null) && (location.Longitude != null))
                {
                    // преобразование В меркатор
                    var p = Mercator.FromLonLat(location.Longitude, location.Latitude);
                    moveX = p.X;
                    moveY = p.Y;

                    peleng.f_Grad_GMS(location.Latitude, ref iDegreeLat, ref iMinuteLat, ref dSecondLat);
                    peleng.f_Grad_GMS(location.Longitude, ref iDegreeLong, ref iMinuteLong, ref dSecondLong);


                    string latL = "";
                    string longL = "";

                    if (GlobalVarLn.Lang == "Eng")
                    {
                        latL = "Latitude: {0}";
                        longL = "    Longitude: {0}";
                        //lLat.Text = string.Format("Latitude: {0}", iDegreeLat.ToString("00") + '\xB0' + " " + iMinuteLat.ToString("00") + "' " + dSecondLat.ToString("00") + "''" + " N");
                        //peleng.f_Grad_GMS(location.Longitude, ref iDegreeLong, ref iMinuteLong, ref dSecondLong);
                        //lLon.Text = string.Format("    Longitude: {0}", iDegreeLong.ToString("00") + '\xB0' + " " + iMinuteLong.ToString("00") + "' " + dSecondLong.ToString("00") + "''" + "  E");
                    }
                    

                    else
                    {
                        if (GlobalVarLn.Lang == "Az")
                        {
                            latL = "Enlik: {0}";
                            longL = "    Uzunluq: {0}";
                        }
                        else if (GlobalVarLn.Lang == "Srb")
                        {
                            latL = "Ширина: {0}";
                            longL = "    Дужина: {0}";
                        }
                        else
                        {
                            latL = "Широта: {0}";
                            longL = "    Долгота: {0}";
                        }

                    }

                    peleng.f_Grad_GMS(location.Latitude, ref iDegreeLat, ref iMinuteLat, ref dSecondLat);
                    lLat.Text = string.Format(latL, iDegreeLat.ToString("00") + '\xB0' + " " + iMinuteLat.ToString("00") + "' " + dSecondLat.ToString("00") + "''" + " N");
                    peleng.f_Grad_GMS(location.Longitude, ref iDegreeLong, ref iMinuteLong, ref dSecondLong);
                    lLon.Text = string.Format(longL, iDegreeLong.ToString("00") + '\xB0' + " " + iMinuteLong.ToString("00") + "' " + dSecondLong.ToString("00") + "''" + "  E");

                    try
                    {
                        lX.Text = string.Format("    MGRS: {0}", location.ToMgrs().ToString());
                    }
                    catch { }
                   

                }

            } // Geographic
            // .............................................................................
            // Mercator

            else
            {
                if (location.Longitude != null)
                    moveX = location.Longitude;
                if (location.Latitude != null)
                    moveY = location.Latitude;


                //var p1 = Mercator.ToLonLat(moveX, moveY);
                //lt = p1.Y;
                //lng = p1.X;

                string latL = "";
                string longL = "";


                
                if (GlobalVarLn.Lang == "Eng")
                {
                    latL = "Latitude: {0}";
                    longL = "    Longitude: {0}";
                    //lLat.Text = string.Format("Latitude: {0}", iDegreeLat.ToString("00") + '\xB0' + " " + iMinuteLat.ToString("00") + "' " + dSecondLat.ToString("00") + "''" + " N");
                    //peleng.f_Grad_GMS(location.Longitude, ref iDegreeLong, ref iMinuteLong, ref dSecondLong);
                    //lLon.Text = string.Format("    Longitude: {0}", iDegreeLong.ToString("00") + '\xB0' + " " + iMinuteLong.ToString("00") + "' " + dSecondLong.ToString("00") + "''" + "  E");
                }


                else
                {
                    if (GlobalVarLn.Lang == "Az")
                    {
                        latL = "Enlik: {0}";
                        longL = "    Uzunluq: {0}";
                    }
                    else if (GlobalVarLn.Lang == "Srb")
                    {
                        latL = "Ширина: {0}";
                        longL = "    Дужина: {0}";
                    }
                    else
                    {
                        latL = "Широта: {0}";
                        longL = "    Долгота: {0}";
                    }

                }

                peleng.f_Grad_GMS(location.Latitude, ref iDegreeLat, ref iMinuteLat, ref dSecondLat);
                lLat.Text = string.Format(latL, iDegreeLat.ToString("00") + '\xB0' + " " + iMinuteLat.ToString("00") + "' " + dSecondLat.ToString("00") + "''" + " N");
                peleng.f_Grad_GMS(location.Longitude, ref iDegreeLong, ref iMinuteLong, ref dSecondLong);
                lLon.Text = string.Format(longL, iDegreeLong.ToString("00") + '\xB0' + " " + iMinuteLong.ToString("00") + "' " + dSecondLong.ToString("00") + "''" + "  E");

                //string ss = "";                  

                try
                {
                    lX.Text = "MGRS: " + location.ToMgrs().ToString(); 
                    //ClassGeoCalculator.f_WGS84_MGRS(moveY, moveX);
                }
                catch { }

            }
            // .............................................................................


// >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>


            // SPFBSPFBSPFBSPFBSPFBSPFBSPFBSPFBSPFBSPFBSPFBSPFBSPFBSPFBSPFBSPFBSPFBSPFBSPFBSPFB

            String strLine2 = "";
            String strLine3 = "";

            int iaz = 0;
            double X_Coordl3_1 = 0;
            double Y_Coordl3_1 = 0;
            double X_Coordl3_2 = 0;
            double Y_Coordl3_2 = 0;
            double DXX3 = 0;
            double DYY3 = 0;
            double azz = 0;
            long ichislo = 0;
            double dchislo = 0;

            ClassMap objClassMap10 = new ClassMap();

            if ((GlobalVarLn.objFormSPFBG.chbXY.Checked == true) &&
                (GlobalVarLn.flF_f1 == 1)

            )
            {
                strLine2 = Convert.ToString(
                    GlobalVarLn.objFormSPFBG.cbChooseSC.Items[
                        GlobalVarLn.objFormSPFBG.cbChooseSC.SelectedIndex]); // Numb

                // if2
                if (!string.IsNullOrEmpty(strLine2))
                {
                    var stations = GlobalVarLn.GetListJSWithCurrentPosition();

                    for (iaz = 0; iaz < stations.Count; iaz++)
                    {
                        var p2 = Mercator.ToLonLat(stations[iaz].CurrentPosition.x, stations[iaz].CurrentPosition.y);
                        X_Coordl3_1 = p2.X;
                        Y_Coordl3_1 = p2.Y;
                        X_Coordl3_2 = moveX;
                        Y_Coordl3_2 = moveY;

                        strLine3 = stations[iaz].Name;

                        // if1
                        if (String.Compare(strLine2, strLine3) == 0)
                        {
                            // Разность координат для расчета азимута
                            // На карте X - вверх
                            //0219 !!!
                            //DXX3 = Y_Coordl3_2 - Y_Coordl3_1;
                            //DYY3 = X_Coordl3_2 - X_Coordl3_1;
                            DXX3 = X_Coordl3_2 - X_Coordl3_1;
                            DYY3 = Y_Coordl3_2 - Y_Coordl3_1;

                            // grad
                            azz = objClassMap10.f_Def_Azimuth(
                                DYY3,
                                DXX3
                            );

                            GlobalVarLn.Az_f1 = azz;

                            ichislo = (long)(azz * 100);
                            dchislo = ((double)ichislo) / 100;
                            lB.Text = string.Format("B,grad  = {0}", Convert.ToString((int)azz));

                            // Убрать с карты
                            // WORK
                            //MapForm.REDRAW_MAP();
                            // CLASS
                            ClassMapRastrReDrawAll.REDRAW_MAP_CLASS();

                            GlobalVarLn.objFormSPFBG.f_LuchReDraw(X_Coordl3_1, Y_Coordl3_1, X_Coordl3_2,
                                Y_Coordl3_2);

                        } // if1

                    } // FOR

                }
            }

            // seg
            if ((GlobalVarLn.objFormSPFBG.chbXY.Checked == false) && (GlobalVarLn.flF_f1 == 1))
            {
                lB.Text = "";
            }
            // SPFBSPFBSPFBSPFBSPFBSPFBSPFBSPFBSPFBSPFBSPFBSPFBSPFBSPFBSPFBSPFBSPFBSPFBSPFBSPFB

            // SPFB1SPFB1SPFB1SPFB1SPFB1SPFB1SPFB1SPFB1SPFB1SPFB1SPFB1SPFB1SPFB1SPFB1SPFB1SPFB1

            if ((GlobalVarLn.objFormSPFB1G.chbXY.Checked == true) &&
                (GlobalVarLn.flF_f2 == 1)

            )
            {
                strLine2 = Convert.ToString(
                    GlobalVarLn.objFormSPFB1G.cbChooseSC.Items[
                        GlobalVarLn.objFormSPFB1G.cbChooseSC.SelectedIndex]); // Numb

                // if2
                if (!string.IsNullOrEmpty(strLine2))
                {
                    var stations = GlobalVarLn.GetListJSWithCurrentPosition();

                    for (iaz = 0; iaz < stations.Count; iaz++)
                    {

                        var p2 = Mercator.ToLonLat(stations[iaz].CurrentPosition.x, stations[iaz].CurrentPosition.y);
                        X_Coordl3_1 = p2.X;
                        Y_Coordl3_1 = p2.Y;
                        X_Coordl3_2 = moveX;
                        Y_Coordl3_2 = moveY;

                        strLine3 = stations[iaz].Name;

                        // if1
                        if (String.Compare(strLine2, strLine3) == 0)
                        {
                            // Разность координат для расчета азимута
                            // На карте X - вверх
                            //0219 !!!
                            //DXX3 = Y_Coordl3_2 - Y_Coordl3_1;
                            //DYY3 = X_Coordl3_2 - X_Coordl3_1;
                            DXX3 = X_Coordl3_2 - X_Coordl3_1;
                            DYY3 = Y_Coordl3_2 - Y_Coordl3_1;

                            // grad
                            azz = objClassMap10.f_Def_Azimuth(
                                DYY3,
                                DXX3
                            );

                            GlobalVarLn.Az_f2 = azz;

                            ichislo = (long)(azz * 100);
                            dchislo = ((double)ichislo) / 100;
                            lB.Text = string.Format("B,grad  = {0}", Convert.ToString((int)azz)); // seg

                            // Убрать с карты
                            //GlobalVarLn.axMapScreenGlobal.Repaint();
                            //GlobalVarLn.objFormAzG.f_Map_Line_XY1(X_Coordl3_1, Y_Coordl3_1, X_Coordl3_2,
                            //    Y_Coordl3_2);

                            // WORK
                            //MapForm.REDRAW_MAP();
                            // CLASS
                            ClassMapRastrReDrawAll.REDRAW_MAP_CLASS();

                            GlobalVarLn.objFormSPFBG.f_LuchReDraw(X_Coordl3_1, Y_Coordl3_1, X_Coordl3_2,
                                Y_Coordl3_2);


                        } // if1

                    } // FOR

                }
            }

            // seg
            if ((GlobalVarLn.objFormSPFB1G.chbXY.Checked == false) && (GlobalVarLn.flF_f2 == 1))
            {
                lB.Text = "";
            }

            // SPFB1SPFB1SPFB1SPFB1SPFB1SPFB1SPFB1SPFB1SPFB1SPFB1SPFB1SPFB1SPFB1SPFB1SPFB1SPFB1

        } // P/P
        // *********************************************************************************** MOUSE_MOVE

        // MOUSE_UP *************************************************************************************
        // MouseUp 

        // ************************************************************************************* MOUSE_UP

        // Загрузка головной формы **********************************************************************

        private void MapForm_Load(object sender, EventArgs e)
        {
            // ------------------------------------------------------------------------------------------
            // Объекты форм для использования в других формах (Инициализируются)

            GlobalVarLn.objFormSPG = objFormSP;
            GlobalVarLn.objFormLFG = objFormLF;
            GlobalVarLn.objFormLF2G = objFormLF2;
            GlobalVarLn.objFormZOG = objFormZO;
            GlobalVarLn.objFormOB1G = objFormOB1;
            GlobalVarLn.objFormOB2G = objFormOB2;
            GlobalVarLn.objFormSuppressionG = objFormSuppression;
            GlobalVarLn.objFormSPFBG = objFormSPFB;
            GlobalVarLn.objFormSPFB1G = objFormSPFB1;
            GlobalVarLn.objFormSPFB2G = objFormSPFB2;
            GlobalVarLn.objFormAz1G = objFormAz1;
            GlobalVarLn.objFormLineSightRangeG = objFormLineSightRange;
            GlobalVarLn.objFormWayG = objFormWay;
            GlobalVarLn.objFormSupSpufG = objFormSupSpuf;
            GlobalVarLn.objFormSpufG = objFormSpuf;
            GlobalVarLn.objFormScreenG = objFormScreen;
            GlobalVarLn.objFormTROG = objFormTRO;
            GlobalVarLn.objFormTabG = objFormTab;
            // ------------------------------------------------------------------------------------------
            GlobalVarLn.LoadListJS();
            GlobalVarLn.flEndSP_stat = 1;
            // ------------------------------------------------------------------------------------------
            // Значки

            String nn;
            String nn1;
            String s;
            long iz;
            // ...........................................................................................
            // OB1

            GlobalVarLn.objFormOB1G.imageList1.Images.Clear();
            try
            {
                s = Application.StartupPath + "\\Images\\OwnObject\\";
                DirectoryInfo di = new DirectoryInfo(@s);

                iz = 0;
                foreach (var fi in di.GetFiles("*.png"))
                {
                    nn = fi.Name;
                    nn1 = fi.DirectoryName + "\\" + fi.Name;
                    GlobalVarLn.objFormOB1G.imageList1.Images.Add(Image.FromFile(nn1));
                    iz += 1;
                }
                foreach (var fi_1 in di.GetFiles("*.bmp"))
                {
                    nn = fi_1.Name;
                    nn1 = fi_1.DirectoryName + "\\" + fi_1.Name;
                    GlobalVarLn.objFormOB1G.imageList1.Images.Add(Image.FromFile(nn1));
                    iz += 1;
                }
                foreach (var fi_1 in di.GetFiles("*.jpeg"))
                {
                    nn = fi_1.Name;
                    nn1 = fi_1.DirectoryName + "\\" + fi_1.Name;
                    GlobalVarLn.objFormOB1G.imageList1.Images.Add(Image.FromFile(nn1));
                    iz += 1;
                }

                if (iz == 0)
                {
                    if (GlobalVarLn.Lang == "Eng")
                        MessageBox.Show("No Friendly Object signs");
                    else
                    if (GlobalVarLn.Lang == "Az")
                        MessageBox.Show("Məntəqələrin nişanları yoxdur");
                    else
                    if (GlobalVarLn.Lang == "Srb")
                        MessageBox.Show("Нема ознака објеката пријатељских снага");
                    else
                        MessageBox.Show("Нет значков объектов");



                }
            }
            catch
            {
                if (GlobalVarLn.Lang == "Eng")
                    MessageBox.Show("No memory");
                else
                    if (GlobalVarLn.Lang == "Az")
                    MessageBox.Show("Yaddaş yoxdur");
                    else
                    if (GlobalVarLn.Lang == "Srb")
                        MessageBox.Show("Нема меморије");
                else
                    MessageBox.Show("Нет памяти");


            }
            // ...........................................................................................
            // OB2

            GlobalVarLn.objFormOB2G.imageList1.Images.Clear();
            try
            {
                s = Application.StartupPath + "\\Images\\EnemyObject\\";
                DirectoryInfo di1 = new DirectoryInfo(@s);

                iz = 0;
                foreach (var fi1 in di1.GetFiles("*.png"))
                {
                    nn = fi1.Name;
                    nn1 = fi1.DirectoryName + "\\" + fi1.Name;
                    GlobalVarLn.objFormOB2G.imageList1.Images.Add(Image.FromFile(nn1));
                    iz += 1;
                }
                foreach (var fi1_1 in di1.GetFiles("*.bmp"))
                {
                    nn = fi1_1.Name;
                    nn1 = fi1_1.DirectoryName + "\\" + fi1_1.Name;
                    GlobalVarLn.objFormOB2G.imageList1.Images.Add(Image.FromFile(nn1));
                    iz += 1;
                }
                foreach (var fi1_1 in di1.GetFiles("*.jpeg"))
                {
                    nn = fi1_1.Name;
                    nn1 = fi1_1.DirectoryName + "\\" + fi1_1.Name;
                    GlobalVarLn.objFormOB2G.imageList1.Images.Add(Image.FromFile(nn1));
                    iz += 1;
                }

                if (iz == 0)
                {
                    if (GlobalVarLn.Lang == "Eng")
                        MessageBox.Show("No EnemyObject signs");
                    else
                    if (GlobalVarLn.Lang == "Az")
                        MessageBox.Show("Düşmən obyektlərinin nişanları yoxdur");
                    else
                    if (GlobalVarLn.Lang == "Srb")
                        MessageBox.Show("Нема ознака објеката непријатељских снага");
                    else
                        MessageBox.Show("Нет значков объектов войск противника");


                }
            }
            catch
            {
                if (GlobalVarLn.Lang == "Eng")
                    MessageBox.Show("No memory");
                else
                    if (GlobalVarLn.Lang == "Az")
                    MessageBox.Show("Yaddaş yoxdur");
                    else
                    if (GlobalVarLn.Lang == "Srb")
                        MessageBox.Show("Нема меморије");
                else
                    MessageBox.Show("Нет памяти");

            }
            // ...........................................................................................
            // SP

            GlobalVarLn.objFormSPG.imageList1.Images.Clear();
            try
            {
                s = Application.StartupPath + "\\Images\\Jammer\\";
                DirectoryInfo di2 = new DirectoryInfo(@s);

                iz = 0;
                foreach (var fi2 in di2.GetFiles("*.png"))
                {
                    nn = fi2.Name;
                    nn1 = fi2.DirectoryName + "\\" + fi2.Name;
                    GlobalVarLn.objFormSPG.imageList1.Images.Add(Image.FromFile(nn1));
                    iz += 1;
                }
                foreach (var fi2_1 in di2.GetFiles("*.bmp"))
                {
                    nn = fi2_1.Name;
                    nn1 = fi2_1.DirectoryName + "\\" + fi2_1.Name;
                    GlobalVarLn.objFormSPG.imageList1.Images.Add(Image.FromFile(nn1));
                    iz += 1;
                }
                foreach (var fi2_1 in di2.GetFiles("*.jpeg"))
                {
                    nn = fi2_1.Name;
                    nn1 = fi2_1.DirectoryName + "\\" + fi2_1.Name;
                    GlobalVarLn.objFormSPG.imageList1.Images.Add(Image.FromFile(nn1));
                    iz += 1;
                }

                if (iz == 0)
                {
                    if (GlobalVarLn.Lang == "Eng")
                        MessageBox.Show("No Jammer signs");
                    else
                    if (GlobalVarLn.Lang == "Az")
                        MessageBox.Show("Stansiyaların nişanları yoxdur");
                    else
                    if (GlobalVarLn.Lang == "Srb")
                        MessageBox.Show("Нема ознака станица ометања");
                    else
                        MessageBox.Show("Нет значков станций");


                }
            }
            catch
            {
                if (GlobalVarLn.Lang == "Eng")
                    MessageBox.Show("No memory");
                else
                    if (GlobalVarLn.Lang == "Az")
                    MessageBox.Show("Yaddaş yoxdur");
                    else
                    if (GlobalVarLn.Lang == "Srb")
                        MessageBox.Show("Нема меморије");
                else
                    MessageBox.Show("Нет памяти");

            }
            // ...........................................................................................
            // SPV

            GlobalVarLn.objFormSPG.imageList1V.Images.Clear();
            try
            {
                s = Application.StartupPath + "\\Images\\JammerPlanned\\";
                DirectoryInfo di3 = new DirectoryInfo(@s);

                iz = 0;
                foreach (var fi3 in di3.GetFiles("*.png"))
                {
                    nn = fi3.Name;
                    nn1 = fi3.DirectoryName + "\\" + fi3.Name;
                    GlobalVarLn.objFormSPG.imageList1V.Images.Add(Image.FromFile(nn1));
                    iz += 1;
                }
                foreach (var fi3_1 in di3.GetFiles("*.bmp"))
                {
                    nn = fi3_1.Name;
                    nn1 = fi3_1.DirectoryName + "\\" + fi3_1.Name;
                    GlobalVarLn.objFormSPG.imageList1V.Images.Add(Image.FromFile(nn1));
                    iz += 1;
                }
                foreach (var fi3_1 in di3.GetFiles("*.jpeg"))
                {
                    nn = fi3_1.Name;
                    nn1 = fi3_1.DirectoryName + "\\" + fi3_1.Name;
                    GlobalVarLn.objFormSPG.imageList1V.Images.Add(Image.FromFile(nn1));
                    iz += 1;
                }

                if (iz == 0)
                {
                    if (GlobalVarLn.Lang == "Eng")
                        MessageBox.Show("No Jammer(planned) signs");
                    else
                    if (GlobalVarLn.Lang == "Az")
                        MessageBox.Show("Planlaşdırılmış stansiyaların yerlərinin nişanı yoxdur");
                    else
                    if (GlobalVarLn.Lang == "Srb")
                        MessageBox.Show("Нема ознака за планиране позиције станица ометања");
                    else
                        MessageBox.Show("Нет значков для планируемых положений станций");


                }
            }
            catch
            {
                if (GlobalVarLn.Lang == "Eng")
                    MessageBox.Show("No memory");
                else
                    if (GlobalVarLn.Lang == "Az")
                    MessageBox.Show("Yaddaş yoxdur");
                    else
                    if (GlobalVarLn.Lang == "Srb")
                        MessageBox.Show("Нема меморије");
                else
                    MessageBox.Show("Нет памяти");

            }
            // ...........................................................................................

            // -------------------------------------------------------------------------------------------
            // Очистка dataGridView1+ Установка 100 строк

            // Очистка dataGridView1
            while (objFormOB2.dataGridView1.Rows.Count != 0)
                objFormOB2.dataGridView1.Rows.Remove(objFormOB2.dataGridView1.Rows[objFormOB2.dataGridView1.Rows.Count - 1]);

            for (int i = 0; i < GlobalVarLn.sizeDatOB2_stat; i++)
            {
                objFormOB2.dataGridView1.Rows.Add("", "", "", "", "");
            }
            // -------------------------------------------------------------------------------------------
            // Очистка dataGridView1+ Установка 100 строк

            // Очистка dataGridView1
            while (objFormOB1.dataGridView1.Rows.Count != 0)
                objFormOB1.dataGridView1.Rows.Remove(objFormOB1.dataGridView1.Rows[objFormOB1.dataGridView1.Rows.Count - 1]);

            for (int iyu = 0; iyu < GlobalVarLn.sizeDatOB1_stat; iyu++)
            {
                objFormOB1.dataGridView1.Rows.Add("", "", "", "");
            }
            // -------------------------------------------------------------------------------------------
            ClassMapRastrReDraw.f_ReDraw_SP(
                                     // Дочерний элемент MapElementHost -> Элемент, где ложим карту
                                     MapForm.RasterMapControl
                                           );
            // -------------------------------------------------------------------------------------------
            // 555
            // Тип карты (географический/Меркатор)

            GlobalVarLn.TypeMap = iniRW.get_TypeMap();
            // -------------------------------------------------------------------------------------------
            // Language
            // 0-engl 1-russ

            GlobalVarLn.Lang = iniRW.get_Lang();


            InterfLang();

            // -------------------------------------------------------------------------------------------

/*
            CultureInfo current = System.Threading.Thread.CurrentThread.CurrentUICulture;
            if (current.TwoLetterISOLanguageName != "fr")
            {
                CultureInfo newCulture = CultureInfo.CreateSpecificCulture("en-US");
                System.Threading.Thread.CurrentThread.CurrentUICulture = newCulture;
                // Make current UI culture consistent with current culture.
                System.Threading.Thread.CurrentThread.CurrentCulture = newCulture;
            }
*/




        }
        // ********************************************************************** Загрузка головной формы




        // LangInterface *******************************************************************************
        // LANG

        public void InterfLang()
        {
            // English >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
            if (GlobalVarLn.Lang == "Eng")
            {
                // ....................................................................................
                // Main

                //MapForm.GlobalVarLn.objFormSPG.button4.Text = "Ввод СП";
                this.toolStripDropDownButton1.Text = "Map";
                this.openMapToolStripMenuItem1.Text = "Open Map";
                this.closeMapToolStripMenuItem1.Text = "Close Map";
                this.openTheHeightMatrixToolStripMenuItem.Text = "Open DEM";
                this.ZoomIn.Text = "Increase scale";
                this.ZoomOut.Text = "Decrease scale";
                this.ZoomInitial.Text = "Base scale";

                this.toolTip1.SetToolTip(this.button11, "Jammer station");
                this.toolTip1.SetToolTip(this.button9, "Friendly force objects");
                this.toolTip1.SetToolTip(this.button15, "Enemy objectives");
                this.toolTip1.SetToolTip(this.button13, "Friendly force front line");
                this.toolTip1.SetToolTip(this.button14, "Enemy force front line");
                this.toolTip1.SetToolTip(this.button12, "Zone of responsibility");
                this.toolTip1.SetToolTip(this.button1, "Tactical and radio environment");
                this.toolTip1.SetToolTip(this.button6, "Route");
                this.toolTip1.SetToolTip(this.button19, "Azimuth");
                this.toolTip1.SetToolTip(this.button10, "Line of sight zone");
                this.toolTip1.SetToolTip(this.bZoneSuppressAvia, "Control links jamming zone");
                this.toolTip1.SetToolTip(this.button7, "Navigation jamming zone");
                this.toolTip1.SetToolTip(this.button8, "Spoofing zone");
                this.toolTip1.SetToolTip(this.button16, "Bands and sectors of reconnaissance");
                this.toolTip1.SetToolTip(this.button17, "Bands and sectors of jamming ");
                this.toolTip1.SetToolTip(this.button18, "Restricted frequencies and frequencies of special attention");
                this.toolTip1.SetToolTip(this.button5, "Table of reconnoitered frequencies");
                this.toolTip1.SetToolTip(this.button4, "ScreenShot");

                this.Text = "Map";

                this.lPC.Text = "PC";
                this.lIPaddr.Text = "IP address";
                this.label13.Text = "Port";

                // ....................................................................................
                // SP

                GlobalVarLn.objFormSPG.Text = "Jammer stations (JS)";
                GlobalVarLn.objFormSPG.button4.Text = "Enter JS";
                GlobalVarLn.objFormSPG.button3.Text = "Delete JS";
                GlobalVarLn.objFormSPG.bAccept.Text = "Save";
                GlobalVarLn.objFormSPG.button1.Text = "Load";
                GlobalVarLn.objFormSPG.bClear.Text = "Clear";
                GlobalVarLn.objFormSPG.bGetFreq.Text = "Accept coordinates";
                GlobalVarLn.objFormSPG.buttonZSP.Text = "Forward";
                GlobalVarLn.objFormSPG.button2.Text = "Backward";

                GlobalVarLn.objFormSPG.spView1.CurrentLabel.Text = "Current location";
                GlobalVarLn.objFormSPG.spView1.PlannedLabel.Text = "Planned location";
                GlobalVarLn.objFormSPG.spView1.NameLabel.Text = "Name";
                GlobalVarLn.objFormSPG.spView1.TypeLabel.Text = "Type";
                GlobalVarLn.objFormSPG.spView1.LatitudeLabel.Text = "Lat, deg";
                GlobalVarLn.objFormSPG.spView1.LongitudeLabel.Text = "Long, deg";
                GlobalVarLn.objFormSPG.spView1.HeightLabel.Text = "H, m";

                GlobalVarLn.objFormSPG.spView2.CurrentLabel.Text = "Current location";
                GlobalVarLn.objFormSPG.spView2.PlannedLabel.Text = "Planned location";
                GlobalVarLn.objFormSPG.spView2.NameLabel.Text = "Name";
                GlobalVarLn.objFormSPG.spView2.TypeLabel.Text = "Type";
                GlobalVarLn.objFormSPG.spView2.LatitudeLabel.Text = "Lat, deg";
                GlobalVarLn.objFormSPG.spView2.LongitudeLabel.Text = "Long, deg";
                GlobalVarLn.objFormSPG.spView2.HeightLabel.Text = "H, m";

                // ....................................................................................
                // OB1

                GlobalVarLn.objFormOB1G.Text = "Friendly force objects";

                GlobalVarLn.objFormOB1G.bAccept.Text = "Save";
                GlobalVarLn.objFormOB1G.button1.Text = "Load";
                GlobalVarLn.objFormOB1G.bClear.Text = "Clear";
                GlobalVarLn.objFormOB1G.button5.Text = "Enter";
                GlobalVarLn.objFormOB1G.button3.Text = "Delete";
                GlobalVarLn.objFormOB1G.buttonZOB1.Text = "Forward";
                GlobalVarLn.objFormOB1G.button4.Text = "Backward";

                GlobalVarLn.objFormOB1G.dataGridView1.Columns[0].HeaderText = "Lat, deg";
                GlobalVarLn.objFormOB1G.dataGridView1.Columns[1].HeaderText = "Long, deg";
                GlobalVarLn.objFormOB1G.dataGridView1.Columns[2].HeaderText = "H, m";
                GlobalVarLn.objFormOB1G.dataGridView1.Columns[3].HeaderText = "Name";

                // ....................................................................................
                // OB2

                GlobalVarLn.objFormOB2G.Text = "Enemy objectives";

                GlobalVarLn.objFormOB2G.bAccept.Text = "Save";
                GlobalVarLn.objFormOB2G.button1.Text = "Load";
                GlobalVarLn.objFormOB2G.bClear.Text = "Clear";
                GlobalVarLn.objFormOB2G.button4.Text = "Enter";
                GlobalVarLn.objFormOB2G.button3.Text = "Delete";
                GlobalVarLn.objFormOB2G.buttonZOB1.Text = "Forward";
                GlobalVarLn.objFormOB2G.button2.Text = "Backward";

                GlobalVarLn.objFormOB2G.dataGridView1.Columns[0].HeaderText = "Lat, deg";
                GlobalVarLn.objFormOB2G.dataGridView1.Columns[1].HeaderText = "Long, deg";
                GlobalVarLn.objFormOB2G.dataGridView1.Columns[2].HeaderText = "H, m";
                GlobalVarLn.objFormOB2G.dataGridView1.Columns[3].HeaderText = "Name";
                GlobalVarLn.objFormOB2G.dataGridView1.Columns[4].HeaderText = "F, MHz";

                // ....................................................................................
                // LF1

                GlobalVarLn.objFormLFG.Text = "Friendly force front line";

                GlobalVarLn.objFormLFG.bAccept.Text = "Save";
                GlobalVarLn.objFormLFG.button1.Text = "Load";
                GlobalVarLn.objFormLFG.bClear.Text = "Clear";

                GlobalVarLn.objFormLFG.dataGridView1.Columns[0].HeaderText = "Lat, deg";
                GlobalVarLn.objFormLFG.dataGridView1.Columns[1].HeaderText = "Long, deg";
                GlobalVarLn.objFormLFG.dataGridView1.Columns[2].HeaderText = "H, m";

                // ....................................................................................
                // LF2

                GlobalVarLn.objFormLF2G.Text = "Enemy force front line";

                GlobalVarLn.objFormLF2G.bAccept.Text = "Save";
                GlobalVarLn.objFormLF2G.button1.Text = "Load";
                GlobalVarLn.objFormLF2G.bClear.Text = "Clear";

                GlobalVarLn.objFormLF2G.dataGridView1.Columns[0].HeaderText = "Lat, deg";
                GlobalVarLn.objFormLF2G.dataGridView1.Columns[1].HeaderText = "Long, deg";
                GlobalVarLn.objFormLF2G.dataGridView1.Columns[2].HeaderText = "H, m";

                // ....................................................................................
                // ZO

                GlobalVarLn.objFormZOG.Text = "Zone of responsibility";

                GlobalVarLn.objFormZOG.bAccept.Text = "Save";
                GlobalVarLn.objFormZOG.button1.Text = "Load";
                GlobalVarLn.objFormZOG.bClear.Text = "Clear";

                GlobalVarLn.objFormZOG.dataGridView1.Columns[0].HeaderText = "Lat, deg";
                GlobalVarLn.objFormZOG.dataGridView1.Columns[1].HeaderText = "Long, deg";
                GlobalVarLn.objFormZOG.dataGridView1.Columns[2].HeaderText = "H, m";

                // ....................................................................................
                // TRO

                GlobalVarLn.objFormTROG.Text = "Tactical and radio environment";

                GlobalVarLn.objFormTROG.button1.Text = "Load";
                GlobalVarLn.objFormTROG.bClear.Text = "Clear";

                // ....................................................................................
                // Azimuth

                GlobalVarLn.objFormAz1G.Text = "Azimuth";

                GlobalVarLn.objFormAz1G.button2.Text = "Accept";
                GlobalVarLn.objFormAz1G.bClear.Text = "Clear";

                GlobalVarLn.objFormAz1G.lMiddleHeight.Text = " Altitude, m";
                GlobalVarLn.objFormAz1G.lXRect.Text = "Lat, deg";
                GlobalVarLn.objFormAz1G.lYRect.Text = "Long, deg";
                GlobalVarLn.objFormAz1G.label15.Text = "Show on map";

                GlobalVarLn.objFormAz1G.dataGridView1.Columns[0].HeaderText = "JS";
                GlobalVarLn.objFormAz1G.dataGridView1.Columns[1].HeaderText = "Lat, deg";
                GlobalVarLn.objFormAz1G.dataGridView1.Columns[2].HeaderText = "Long, deg";
                GlobalVarLn.objFormAz1G.dataGridView1.Columns[3].HeaderText = "Azimuth, deg";

                // ....................................................................................
                // ZPV

                GlobalVarLn.objFormLineSightRangeG.Text = "Line of sight zone (LOS)";

                GlobalVarLn.objFormLineSightRangeG.tabPage1.Text = "LOS";
                GlobalVarLn.objFormLineSightRangeG.tabPage2.Text = "Detection zone";

                GlobalVarLn.objFormLineSightRangeG.lCenterLSR.Text = "Center of the zone";
                GlobalVarLn.objFormLineSightRangeG.label4.Text = "Enter coordinates";
                GlobalVarLn.objFormLineSightRangeG.lXRect.Text = "Lat, deg";
                GlobalVarLn.objFormLineSightRangeG.lYRect.Text = "Long, deg";
                GlobalVarLn.objFormLineSightRangeG.lOwnHeight.Text = "H, m";
                GlobalVarLn.objFormLineSightRangeG.lDistSightRange.Text = "Range, m";
                GlobalVarLn.objFormLineSightRangeG.lMiddleHeight.Text = "Average height, m";
                GlobalVarLn.objFormLineSightRangeG.grbOwnObject.Text = "Jammer";
                GlobalVarLn.objFormLineSightRangeG.label17.Text = "Antenna height, m";
                GlobalVarLn.objFormLineSightRangeG.lHeightOwnObject.Text = "Total height, m";
                GlobalVarLn.objFormLineSightRangeG.grbOpponObject.Text = "Jam victim";
                GlobalVarLn.objFormLineSightRangeG.label1.Text = "Antenna height, m";
                GlobalVarLn.objFormLineSightRangeG.lHeightOpponObject.Text = "Total height, m";
                GlobalVarLn.objFormLineSightRangeG.button1.Text = "LOS center";
                GlobalVarLn.objFormLineSightRangeG.bAccept.Text = "Accept";
                GlobalVarLn.objFormLineSightRangeG.bClear.Text = "Clear";

                GlobalVarLn.objFormLineSightRangeG.lPowerOwn.Text = "Radio transmitter power, W (0.1-100)";
                GlobalVarLn.objFormLineSightRangeG.lFreq.Text = "Radio transmitter frequency, MHz (100-6000)";
                GlobalVarLn.objFormLineSightRangeG.lCoeffTransmitOpponent.Text = "Direction finder antenna gain (1-6)";
                GlobalVarLn.objFormLineSightRangeG.label2.Text = "Direction finder threshold sensitivity, dBW (-140 - -100) ";
                GlobalVarLn.objFormLineSightRangeG.label3.Text = "Radius of the zone, m";
                GlobalVarLn.objFormLineSightRangeG.button2.Text = "Accept";
                GlobalVarLn.objFormLineSightRangeG.button3.Text = "Clear";

                // ....................................................................................
                // Route

                GlobalVarLn.objFormWayG.Text = "Route";

                GlobalVarLn.objFormWayG.label2.Text = "Number of points";
                GlobalVarLn.objFormWayG.label3.Text = "Length of the route, m";
                GlobalVarLn.objFormWayG.button4.Text = "Save";
                GlobalVarLn.objFormWayG.button5.Text = "Load";
                GlobalVarLn.objFormWayG.button3.Text = "Clear";

                GlobalVarLn.objFormWayG.dataGridView1.Columns[0].HeaderText = "Lat, deg";
                GlobalVarLn.objFormWayG.dataGridView1.Columns[1].HeaderText = "Long, deg";
                GlobalVarLn.objFormWayG.dataGridView1.Columns[2].HeaderText = "L, m";

                // ....................................................................................
                // FormSuppression
                // Зона подавления радиолиний управления

                GlobalVarLn.objFormSuppressionG.Text = "Control links jamming zone";

                GlobalVarLn.objFormSuppressionG.tpOwnObject.Text = "Jammer station (JS)";
                GlobalVarLn.objFormSuppressionG.tabPage1.Text = "Jam victim";

                GlobalVarLn.objFormSuppressionG.lCenterLSR.Text = "JS";

                GlobalVarLn.objFormSuppressionG.lXRect.Text = "Lat, deg";
                GlobalVarLn.objFormSuppressionG.lYRect.Text = "Long, deg";
                GlobalVarLn.objFormSuppressionG.lOwnHeight.Text = "H, m";
                GlobalVarLn.objFormSuppressionG.lRadiusZone.Text = "Radius of the jamming zone, m";
                GlobalVarLn.objFormSuppressionG.label13.Text = "Radius of the depletion zone, m";
                GlobalVarLn.objFormSuppressionG.label17.Text = "Antenna height, m";
                GlobalVarLn.objFormSuppressionG.lPowerOwn.Text = "Power, W (10-300)";
                GlobalVarLn.objFormSuppressionG.lCoeffOwn.Text = "Gain (4-8)";
                GlobalVarLn.objFormSuppressionG.lHeightOwnObject.Text = "Total height, m";

                GlobalVarLn.objFormSuppressionG.button1.Text = "JS";
                GlobalVarLn.objFormSuppressionG.button2.Text = "Jam victim";
                GlobalVarLn.objFormSuppressionG.bAccept.Text = "Accept";
                GlobalVarLn.objFormSuppressionG.bClear.Text = "Clear";

                // GlobalVarLn.objFormSuppressionG.gbPoint1.Text = "Object of jam";

                GlobalVarLn.objFormSuppressionG.lPt1XRect.Text = "Lat, deg";
                GlobalVarLn.objFormSuppressionG.lPt1YRect.Text = "Long, deg";
                GlobalVarLn.objFormSuppressionG.lPt1Height.Text = "H, m";
                GlobalVarLn.objFormSuppressionG.label21.Text = "JSR (1-100)";
                GlobalVarLn.objFormSuppressionG.label12.Text = "Power, W (1-100)";
                GlobalVarLn.objFormSuppressionG.label14.Text = "Gain (1-10)";
                GlobalVarLn.objFormSuppressionG.label11.Text = "Antenna height, m";
                GlobalVarLn.objFormSuppressionG.label27.Text = "Antenna height (receiver), m";

                // ....................................................................................
                // FormSupSpuf
                // Зона подавления навигации

                GlobalVarLn.objFormSupSpufG.Text = "Navigation jamming zone";

                GlobalVarLn.objFormSupSpufG.lCenterLSR.Text = "Center of zone";
                GlobalVarLn.objFormSupSpufG.lOwnHeight.Text = "H, m";
                GlobalVarLn.objFormSupSpufG.lXRect.Text = "Lat, deg";
                GlobalVarLn.objFormSupSpufG.lYRect.Text = "Long, deg";
                GlobalVarLn.objFormSupSpufG.lDistSightRange.Text = "Radius, m";
                GlobalVarLn.objFormSupSpufG.label2.Text = "Power, W (10-100)";
                GlobalVarLn.objFormSupSpufG.label3.Text = "Gain";
                GlobalVarLn.objFormSupSpufG.label4.Text = "JSR, dB (30-50)";
                GlobalVarLn.objFormSupSpufG.label17.Text = "Antenna (JS) height, m";
                GlobalVarLn.objFormSupSpufG.label11.Text = "Antenna height, m";
                GlobalVarLn.objFormSupSpufG.button1.Text = "Center of the zone";
                GlobalVarLn.objFormSupSpufG.bAccept.Text = "Accept";
                GlobalVarLn.objFormSupSpufG.bClear.Text = "Clear";

                // ....................................................................................
                // FormSpuf
                // Зона спуфинга

                GlobalVarLn.objFormSpufG.Text = "Spoofing zone";

                GlobalVarLn.objFormSpufG.lCenterLSR.Text = "Center of zone";
                GlobalVarLn.objFormSpufG.lOwnHeight.Text = "H, m";
                GlobalVarLn.objFormSpufG.lXRect.Text = "Lat, deg";
                GlobalVarLn.objFormSpufG.lYRect.Text = "Long, deg";
                GlobalVarLn.objFormSpufG.lDistSightRange.Text = " Radius of the spoofing zone, m";
                GlobalVarLn.objFormSpufG.label4.Text = "Radius of the jamming zone, m";
                GlobalVarLn.objFormSpufG.label2.Text = "Power, W (0.1-100)";
                GlobalVarLn.objFormSpufG.label3.Text = "Gain";
                GlobalVarLn.objFormSpufG.label1.Text = "Spoofing coefficient, dB (7-15)";
                GlobalVarLn.objFormSpufG.label17.Text = "Antenna (JS) height, m";
                GlobalVarLn.objFormSpufG.label11.Text = "Antenna height, m";
                GlobalVarLn.objFormSpufG.button1.Text = "Center of zone";
                GlobalVarLn.objFormSpufG.bAccept.Text = "Accept";
                GlobalVarLn.objFormSpufG.bClear.Text = "Clear";

                // ....................................................................................
                // Диапазоны частот и сектора радиоразведки

                GlobalVarLn.objFormSPFBG.Text = "Bands and sectors of reconnaissance ( not more than 10)";

                GlobalVarLn.objFormSPFBG.bAccept.Text = "Save";
                GlobalVarLn.objFormSPFBG.button2.Text = "Load";
                GlobalVarLn.objFormSPFBG.bClear.Text = "Clear";
                GlobalVarLn.objFormSPFBG.lChooseSC.Text = "Jammer station";
                GlobalVarLn.objFormSPFBG.label15.Text = "Input from map";

                GlobalVarLn.objFormSPFBG.dataGridView1.Columns[0].HeaderText = "Fmin,MHz";
                GlobalVarLn.objFormSPFBG.dataGridView1.Columns[1].HeaderText = "Fmax,MHz";
                GlobalVarLn.objFormSPFBG.dataGridView1.Columns[2].HeaderText = "Az min,deg";
                GlobalVarLn.objFormSPFBG.dataGridView1.Columns[3].HeaderText = "Az max,deg";

                // ....................................................................................
                // Диапазоны частот и сектора радиоподавления

                GlobalVarLn.objFormSPFB1G.Text = "Bands and sectors of jamming ( not more than 10)";

                GlobalVarLn.objFormSPFB1G.bAccept.Text = "Save";
                GlobalVarLn.objFormSPFB1G.button2.Text = "Load";
                GlobalVarLn.objFormSPFB1G.bClear.Text = "Clear";
                GlobalVarLn.objFormSPFB1G.lChooseSC.Text = "Jammer station";
                GlobalVarLn.objFormSPFB1G.label15.Text = "Input from map";

                GlobalVarLn.objFormSPFB1G.dataGridView1.Columns[0].HeaderText = "Fmin,MHz";
                GlobalVarLn.objFormSPFB1G.dataGridView1.Columns[1].HeaderText = "Fmax,MHz";
                GlobalVarLn.objFormSPFB1G.dataGridView1.Columns[2].HeaderText = "Az min,deg";
                GlobalVarLn.objFormSPFB1G.dataGridView1.Columns[3].HeaderText = "Az max,deg";

                // ....................................................................................
                // Запрещенные частоты и частоты особого внимания

                GlobalVarLn.objFormSPFB2G.Text = "Restricted frequencies and frequencies of special attention (not more than 10)";

                GlobalVarLn.objFormSPFB2G.lChooseSC.Text = "JS";
                GlobalVarLn.objFormSPFB2G.label1.Text = "Restricted frequencies and frequency bands";
                GlobalVarLn.objFormSPFB2G.label2.Text = "Frequencies and frequency bands of special attention";

                GlobalVarLn.objFormSPFB2G.bAccept.Text = "Save";
                GlobalVarLn.objFormSPFB2G.button2.Text = "Load";
                GlobalVarLn.objFormSPFB2G.bClear.Text = "Clear";
                GlobalVarLn.objFormSPFB2G.bSendFreqForbid.Text = "Send";
                GlobalVarLn.objFormSPFB2G.bAccept1.Text = "Save";
                GlobalVarLn.objFormSPFB2G.bLoad1.Text = "Load";
                GlobalVarLn.objFormSPFB2G.bClear1.Text = "Clear";
                GlobalVarLn.objFormSPFB2G.bSendFreqForbit1.Text = "Send";

                GlobalVarLn.objFormSPFB2G.dgvFreqForbid.Columns[0].HeaderText = "Fmin,MHz";
                GlobalVarLn.objFormSPFB2G.dgvFreqForbid.Columns[1].HeaderText = "Fmax,MHz";
                GlobalVarLn.objFormSPFB2G.dgvFreqSpec.Columns[0].HeaderText = "Fmin,MHz";
                GlobalVarLn.objFormSPFB2G.dgvFreqSpec.Columns[1].HeaderText = "Fmax,MHz";

                // ....................................................................................
                // Tab

                GlobalVarLn.objFormTabG.Text = "Table of reconnoitered frequencies";

                GlobalVarLn.objFormTabG.bGetFreq.Text = "Accept";
                GlobalVarLn.objFormTabG.bClear.Text = "Clear";

                // ....................................................................................

            }
            // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> English

            else
            {
                // Azerb >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
                if (GlobalVarLn.Lang == "Az")
                {
                    // ....................................................................................
                    // Main

                    //MapForm.GlobalVarLn.objFormSPG.button4.Text = "Ввод СП";
                    this.toolStripDropDownButton1.Text = "Xəritə";
                    this.openMapToolStripMenuItem1.Text = "Xəritəni aç";
                    this.closeMapToolStripMenuItem1.Text = "Xəritəni bağla";
                    this.openTheHeightMatrixToolStripMenuItem.Text = "Hündürlük matrislərini aç";
                    this.ZoomIn.Text = "Miqyası böyüt";
                    this.ZoomOut.Text = "Miqyası kiçild";
                    this.ZoomInitial.Text = "Baza miqyası";

                    this.toolTip1.SetToolTip(this.button11, "Maneə sansiyası ");
                    this.toolTip1.SetToolTip(this.button9, "Öz qoşunlarımızın mövqeyi");
                    this.toolTip1.SetToolTip(this.button15, "Düşmən qoşunlarının mövqeyi");
                    this.toolTip1.SetToolTip(this.button13, "Öz qoşunlarımızın cəbhə xətti");
                    this.toolTip1.SetToolTip(this.button14, "Düşmən qoşunlarının cəbhə xətti");
                    this.toolTip1.SetToolTip(this.button12, "Cavabdehlik rayonu");
                    this.toolTip1.SetToolTip(this.button1, "Taktiki və radioelektron vəziyyət");
                    this.toolTip1.SetToolTip(this.button6, "Marşrut");
                    this.toolTip1.SetToolTip(this.button19, "Azimut");
                    this.toolTip1.SetToolTip(this.button10, "Birbaşa görünüş sahəsi");
                    this.toolTip1.SetToolTip(this.bZoneSuppressAvia, "Radio idarəetmənin susdurulma sahəsi");
                    this.toolTip1.SetToolTip(this.button7, "Naviqasiyanın susdurulma sahəsi");
                    this.toolTip1.SetToolTip(this.button8, "Spufinq sahəsi");
                    this.toolTip1.SetToolTip(this.button16, "Tezlik aralıqları və radiokəşfiyyat sahəsi");
                    this.toolTip1.SetToolTip(this.button17, "Tezlik aralıqları və radiomaneə sahəsi");
                    this.toolTip1.SetToolTip(this.button18, "Xüsusi diqqət və qadağan olunmuş tezliklər");
                    this.toolTip1.SetToolTip(this.button5, "Tezliklərin kəşfiyyatı cədvəli");
                    this.toolTip1.SetToolTip(this.button4, "Ekranın rəsmini çək");

                    this.Text = "Xəritə";

                    this.lPC.Text = "KP";
                    this.lIPaddr.Text = "IP addres";
                    this.label13.Text = "Port";

                    // ....................................................................................
                    // SP

                    GlobalVarLn.objFormSPG.Text = "Maneə sansiyası (MS)";
                    GlobalVarLn.objFormSPG.button4.Text = "MS daxil ol";
                    GlobalVarLn.objFormSPG.button3.Text = "Silmək";
                    GlobalVarLn.objFormSPG.bAccept.Text = "Saxlamaq";
                    GlobalVarLn.objFormSPG.button1.Text = "Yükləyin";
                    GlobalVarLn.objFormSPG.bClear.Text = "Təmizləmək";
                    GlobalVarLn.objFormSPG.bGetFreq.Text = "Koordinatları qəbul et";
                    GlobalVarLn.objFormSPG.buttonZSP.Text = "İrəli";
                    GlobalVarLn.objFormSPG.button2.Text = "Geri";

                    GlobalVarLn.objFormSPG.spView1.CurrentLabel.Text = "Cari vəziyyət";
                    GlobalVarLn.objFormSPG.spView1.PlannedLabel.Text = "Planlaşdırılmış vəziyyət";
                    GlobalVarLn.objFormSPG.spView1.NameLabel.Text = "Adı";
                    GlobalVarLn.objFormSPG.spView1.TypeLabel.Text = "Tip";
                    GlobalVarLn.objFormSPG.spView1.LatitudeLabel.Text = "En, dərəcə";
                    GlobalVarLn.objFormSPG.spView1.LongitudeLabel.Text = "Uzunluq, dərəcə";
                    GlobalVarLn.objFormSPG.spView1.HeightLabel.Text = "Hündürlük, m";

                    GlobalVarLn.objFormSPG.spView2.CurrentLabel.Text = "Cari vəziyyət";
                    GlobalVarLn.objFormSPG.spView2.PlannedLabel.Text = "Planlaşdırılmış vəziyyət";
                    GlobalVarLn.objFormSPG.spView2.NameLabel.Text = "Adı";
                    GlobalVarLn.objFormSPG.spView2.TypeLabel.Text = "Tip";
                    GlobalVarLn.objFormSPG.spView2.LatitudeLabel.Text = "En, dərəcə";
                    GlobalVarLn.objFormSPG.spView2.LongitudeLabel.Text = "Uzunluq, dərəcə";
                    GlobalVarLn.objFormSPG.spView2.HeightLabel.Text = "Hündürlük, m";

                    // ....................................................................................
                    // OB1

                    GlobalVarLn.objFormOB1G.Text = "Öz qoşunlarımızın mövqeyi";

                    GlobalVarLn.objFormOB1G.bAccept.Text = "Saxlamaq";
                    GlobalVarLn.objFormOB1G.button1.Text = "Yükləyin";
                    GlobalVarLn.objFormOB1G.bClear.Text = "Təmizləmək";
                    GlobalVarLn.objFormOB1G.button5.Text = "Daxil ol";
                    GlobalVarLn.objFormOB1G.button3.Text = "Silmək";
                    GlobalVarLn.objFormOB1G.buttonZOB1.Text = "İrəli";
                    GlobalVarLn.objFormOB1G.button4.Text = "Geri";

                    GlobalVarLn.objFormOB1G.dataGridView1.Columns[0].HeaderText = "En, dər";
                    GlobalVarLn.objFormOB1G.dataGridView1.Columns[1].HeaderText = "Uzunluq, dər";
                    GlobalVarLn.objFormOB1G.dataGridView1.Columns[2].HeaderText = "Hündürlük, m";
                    GlobalVarLn.objFormOB1G.dataGridView1.Columns[3].HeaderText = "Adı";

                    // ....................................................................................
                    // OB2

                    GlobalVarLn.objFormOB2G.Text = "Düşmən qoşunlarının mövqeyi";

                    GlobalVarLn.objFormOB2G.bAccept.Text = "Saxlamaq";
                    GlobalVarLn.objFormOB2G.button1.Text = "Yükləyin";
                    GlobalVarLn.objFormOB2G.bClear.Text = "Təmizləmək";
                    GlobalVarLn.objFormOB2G.button4.Text = "Daxil ol";
                    GlobalVarLn.objFormOB2G.button3.Text = "Silmək";
                    GlobalVarLn.objFormOB2G.buttonZOB1.Text = "İrəli";
                    GlobalVarLn.objFormOB2G.button2.Text = "Geri";

                    GlobalVarLn.objFormOB2G.dataGridView1.Columns[0].HeaderText = "En, dər";
                    GlobalVarLn.objFormOB2G.dataGridView1.Columns[1].HeaderText = "Uzunluq, dər";
                    GlobalVarLn.objFormOB2G.dataGridView1.Columns[2].HeaderText = "Hündürlük, m";
                    GlobalVarLn.objFormOB2G.dataGridView1.Columns[3].HeaderText = "Adı";
                    GlobalVarLn.objFormOB2G.dataGridView1.Columns[4].HeaderText = "F, MHs";

                    // ....................................................................................
                    // LF1

                    GlobalVarLn.objFormLFG.Text = "Öz qoşunlarımızın cəbhə xətti";

                    GlobalVarLn.objFormLFG.bAccept.Text = "Saxlamaq";
                    GlobalVarLn.objFormLFG.button1.Text = "Yükləyin";
                    GlobalVarLn.objFormLFG.bClear.Text = "Təmizləmək";

                    GlobalVarLn.objFormLFG.dataGridView1.Columns[0].HeaderText = "En, dər";
                    GlobalVarLn.objFormLFG.dataGridView1.Columns[1].HeaderText = "Uzunluq, dər";
                    GlobalVarLn.objFormLFG.dataGridView1.Columns[2].HeaderText = "Hündürlük, m";

                    // ....................................................................................
                    // LF2

                    GlobalVarLn.objFormLF2G.Text = "Düşmən qoşunlarının cəbhə xətti";

                    GlobalVarLn.objFormLF2G.bAccept.Text = "Saxlamaq";
                    GlobalVarLn.objFormLF2G.button1.Text = "Yükləyin";
                    GlobalVarLn.objFormLF2G.bClear.Text = "Təmizləmək";

                    GlobalVarLn.objFormLF2G.dataGridView1.Columns[0].HeaderText = "En, dər";
                    GlobalVarLn.objFormLF2G.dataGridView1.Columns[1].HeaderText = "Uzunluq, dər";
                    GlobalVarLn.objFormLF2G.dataGridView1.Columns[2].HeaderText = "Hündürlük, m";

                    // ....................................................................................
                    // ZO

                    GlobalVarLn.objFormZOG.Text = "Cavabdehlik rayonu";

                    GlobalVarLn.objFormZOG.bAccept.Text = "Saxlamaq";
                    GlobalVarLn.objFormZOG.button1.Text = "Yükləyin";
                    GlobalVarLn.objFormZOG.bClear.Text = "Təmizləmək";

                    GlobalVarLn.objFormZOG.dataGridView1.Columns[0].HeaderText = "En, dər";
                    GlobalVarLn.objFormZOG.dataGridView1.Columns[1].HeaderText = "Uzunluq, dər";
                    GlobalVarLn.objFormZOG.dataGridView1.Columns[2].HeaderText = "Hündürlük, m";

                    // ....................................................................................
                    // TRO

                    GlobalVarLn.objFormTROG.Text = "Taktiki və radioelektron vəziyyət";

                    GlobalVarLn.objFormTROG.button1.Text = "Yükləyin";
                    GlobalVarLn.objFormTROG.bClear.Text = "Təmizləmək";

                    // ....................................................................................
                    // Azimuth

                    GlobalVarLn.objFormAz1G.Text = "Azimut";

                    GlobalVarLn.objFormAz1G.button2.Text = "Qəbul et";
                    GlobalVarLn.objFormAz1G.bClear.Text = "Təmizləmək";

                    GlobalVarLn.objFormAz1G.lMiddleHeight.Text = " Hündürlük, m";
                    GlobalVarLn.objFormAz1G.lXRect.Text = "En, dər";
                    GlobalVarLn.objFormAz1G.lYRect.Text = "Uzunluq, dər";
                    GlobalVarLn.objFormAz1G.label15.Text = "Show on map";

                    GlobalVarLn.objFormAz1G.dataGridView1.Columns[0].HeaderText = "MS";
                    GlobalVarLn.objFormAz1G.dataGridView1.Columns[1].HeaderText = "En, dər";
                    GlobalVarLn.objFormAz1G.dataGridView1.Columns[2].HeaderText = "Uzunluq, dər";
                    GlobalVarLn.objFormAz1G.dataGridView1.Columns[3].HeaderText = "Azimut, dər";

                    // ....................................................................................
                    // ZPV

                    GlobalVarLn.objFormLineSightRangeG.Text = "Gözləmə sahəsi";

                    GlobalVarLn.objFormLineSightRangeG.tabPage1.Text = "Gözləmə sahəsi";
                    GlobalVarLn.objFormLineSightRangeG.tabPage2.Text = "MS aşkaretmə sahəsi";

                    GlobalVarLn.objFormLineSightRangeG.lCenterLSR.Text = "Sahənin mərkəzi";
                    GlobalVarLn.objFormLineSightRangeG.label4.Text = "Koordinatların daxil edilməsi";
                    GlobalVarLn.objFormLineSightRangeG.lXRect.Text = "En, dər";
                    GlobalVarLn.objFormLineSightRangeG.lYRect.Text = "Uzunluq, dər";
                    GlobalVarLn.objFormLineSightRangeG.lOwnHeight.Text = "Hündürlük, m";
                    GlobalVarLn.objFormLineSightRangeG.lDistSightRange.Text = "Olunmuş , m";
                    GlobalVarLn.objFormLineSightRangeG.lMiddleHeight.Text = "Average height, m";
                    GlobalVarLn.objFormLineSightRangeG.grbOwnObject.Text = "Maneə sansiyası ";
                    GlobalVarLn.objFormLineSightRangeG.label17.Text = "Antenanın hündürlüyü, m";
                    GlobalVarLn.objFormLineSightRangeG.lHeightOwnObject.Text = "Ümumi hündürlük, m";
                    GlobalVarLn.objFormLineSightRangeG.grbOpponObject.Text = "Susdurulacaq obyekt";
                    GlobalVarLn.objFormLineSightRangeG.label1.Text = "Antenanın hündürlüyü, m";
                    GlobalVarLn.objFormLineSightRangeG.lHeightOpponObject.Text = "Ümumi hündürlük, m";
                    GlobalVarLn.objFormLineSightRangeG.button1.Text = "Gözləmə sahənin mərkəzi";
                    GlobalVarLn.objFormLineSightRangeG.bAccept.Text = "Qəbul et";
                    GlobalVarLn.objFormLineSightRangeG.bClear.Text = "Təmizləmək";

                    GlobalVarLn.objFormLineSightRangeG.lPowerOwn.Text = "Ötürücünün gücü Wt (0.1-100)";
                    GlobalVarLn.objFormLineSightRangeG.lFreq.Text = "Ötürücünün tezliyi MHs (100-6000)";
                    GlobalVarLn.objFormLineSightRangeG.lCoeffTransmitOpponent.Text = "Antenna gücləndiricinin koofisienti (1-6)";
                    GlobalVarLn.objFormLineSightRangeG.label2.Text = "Pelenqatorun həssaslıq addımı, dBWt (-140 - -100) ";
                    GlobalVarLn.objFormLineSightRangeG.label3.Text = "Zonanın radiusu, m";
                    GlobalVarLn.objFormLineSightRangeG.button2.Text = "Qəbul et";
                    GlobalVarLn.objFormLineSightRangeG.button3.Text = "Təmizləmək";

                    // ....................................................................................
                    // Route

                    GlobalVarLn.objFormWayG.Text = "Marşrut";

                    GlobalVarLn.objFormWayG.label2.Text = "Məntəqələrin sayı";
                    GlobalVarLn.objFormWayG.label3.Text = "Marşrutun uzunluğu, m";
                    GlobalVarLn.objFormWayG.button4.Text = "Saxlamaq";
                    GlobalVarLn.objFormWayG.button5.Text = "Yükləyin";
                    GlobalVarLn.objFormWayG.button3.Text = "Təmizləmək";

                    GlobalVarLn.objFormWayG.dataGridView1.Columns[0].HeaderText = "En, dər";
                    GlobalVarLn.objFormWayG.dataGridView1.Columns[1].HeaderText = "Uzunluq, dər";
                    GlobalVarLn.objFormWayG.dataGridView1.Columns[2].HeaderText = "L, m";

                    // ....................................................................................
                    // FormSuppression
                    // Зона подавления радиолиний управления

                    GlobalVarLn.objFormSuppressionG.Text = "Radio idarəetmənin susdurulma sahəsi";

                    GlobalVarLn.objFormSuppressionG.tpOwnObject.Text = "Maneə sansiyası (MS)";
                    GlobalVarLn.objFormSuppressionG.tabPage1.Text = "Susdurulacaq obyekt";

                    GlobalVarLn.objFormSuppressionG.lCenterLSR.Text = "MS";

                    GlobalVarLn.objFormSuppressionG.lXRect.Text = "En, dər";
                    GlobalVarLn.objFormSuppressionG.lYRect.Text = "Uzunluq, dər";
                    GlobalVarLn.objFormSuppressionG.lOwnHeight.Text = "Hündürlük, m";
                    GlobalVarLn.objFormSuppressionG.lRadiusZone.Text = "Maneə zonasının radius, m";
                    GlobalVarLn.objFormSuppressionG.label13.Text = "Susdurulmayan zonanın radiusu, m";
                    GlobalVarLn.objFormSuppressionG.label17.Text = "Antenanın hündürlüyü, m";
                    GlobalVarLn.objFormSuppressionG.lPowerOwn.Text = "Güc, W (10-300)";
                    GlobalVarLn.objFormSuppressionG.lCoeffOwn.Text = "Gücləndiricinin koofisienti (4-8)";
                    GlobalVarLn.objFormSuppressionG.lHeightOwnObject.Text = "Ümumi hündürlük, m";

                    GlobalVarLn.objFormSuppressionG.button1.Text = "MS";
                    GlobalVarLn.objFormSuppressionG.button2.Text = "Susdurulacaq obyekt";
                    GlobalVarLn.objFormSuppressionG.bAccept.Text = "Qəbul et";
                    GlobalVarLn.objFormSuppressionG.bClear.Text = "Təmizləmək";

                    // GlobalVarLn.objFormSuppressionG.gbPoint1.Text = "Object of jam";

                    GlobalVarLn.objFormSuppressionG.lPt1XRect.Text = "En, dər";
                    GlobalVarLn.objFormSuppressionG.lPt1YRect.Text = "Uzunluq, dər";
                    GlobalVarLn.objFormSuppressionG.lPt1Height.Text = "Hündürlük, m";
                    GlobalVarLn.objFormSuppressionG.label21.Text = "Maneənin koofisienti (1-100)";
                    GlobalVarLn.objFormSuppressionG.label12.Text = "Güc, W (1-100)";
                    GlobalVarLn.objFormSuppressionG.label14.Text = "Gücləndiricinin koofisienti (1-10)";
                    GlobalVarLn.objFormSuppressionG.label11.Text = "Antenanın hündürlüyü, m";
                    GlobalVarLn.objFormSuppressionG.label27.Text = "Antenanın hündürlüyü (Qəbuledici), m";

                    // ....................................................................................
                    // FormSupSpuf
                    // Зона подавления навигации

                    GlobalVarLn.objFormSupSpufG.Text = "Naviqasiyanın susdurulma sahəsi";

                    GlobalVarLn.objFormSupSpufG.lCenterLSR.Text = "Sahənin mərkəzi";
                    GlobalVarLn.objFormSupSpufG.lOwnHeight.Text = "Hündürlük, m";
                    GlobalVarLn.objFormSupSpufG.lXRect.Text = "En, dər";
                    GlobalVarLn.objFormSupSpufG.lYRect.Text = "Uzunluq, dər";
                    GlobalVarLn.objFormSupSpufG.lDistSightRange.Text = "Radius, m";
                    GlobalVarLn.objFormSupSpufG.label2.Text = "Güc, W (10-100)";
                    GlobalVarLn.objFormSupSpufG.label3.Text = "Gücləndiricinin koofisienti";
                    GlobalVarLn.objFormSupSpufG.label4.Text = "Maneənin koofisienti, dB (30-50)";
                    GlobalVarLn.objFormSupSpufG.label17.Text = "Antenanın hündürlüyü (MS) , m";
                    GlobalVarLn.objFormSupSpufG.label11.Text = "Antenanın hündürlüyü, m";
                    GlobalVarLn.objFormSupSpufG.button1.Text = "Sahənin mərkəzi";
                    GlobalVarLn.objFormSupSpufG.bAccept.Text = "Qəbul et";
                    GlobalVarLn.objFormSupSpufG.bClear.Text = "Təmizləmək";

                    // ....................................................................................
                    // FormSpuf
                    // Зона спуфинга

                    GlobalVarLn.objFormSpufG.Text = "Spoofing zone";

                    GlobalVarLn.objFormSpufG.lCenterLSR.Text = "Center of zone";
                    GlobalVarLn.objFormSpufG.lOwnHeight.Text = "H, m";
                    GlobalVarLn.objFormSpufG.lXRect.Text = "Lat, deg";
                    GlobalVarLn.objFormSpufG.lYRect.Text = "Long, deg";
                    GlobalVarLn.objFormSpufG.lDistSightRange.Text = " Radius of the spoofing zone, m";
                    GlobalVarLn.objFormSpufG.label4.Text = "Radius of the jamming zone, m";
                    GlobalVarLn.objFormSpufG.label2.Text = "Power, W (0.1-100)";
                    GlobalVarLn.objFormSpufG.label3.Text = "Gain";
                    GlobalVarLn.objFormSpufG.label1.Text = "Spoofing coefficient, dB (7-15)";
                    GlobalVarLn.objFormSpufG.label17.Text = "Antenna (JS) height, m";
                    GlobalVarLn.objFormSpufG.label11.Text = "Antenna height, m";
                    GlobalVarLn.objFormSpufG.button1.Text = "Center of zone";
                    GlobalVarLn.objFormSpufG.bAccept.Text = "Accept";
                    GlobalVarLn.objFormSpufG.bClear.Text = "Clear";

                    // ....................................................................................
                    // Диапазоны частот и сектора радиоразведки

                    GlobalVarLn.objFormSPFBG.Text = "Tezlik aralıqları və radiokəşfiyyat sahəsi (10-dan çox ilmayaraq)";

                    GlobalVarLn.objFormSPFBG.bAccept.Text = "Saxlamaq";
                    GlobalVarLn.objFormSPFBG.button2.Text = "Yükləyin";
                    GlobalVarLn.objFormSPFBG.bClear.Text = "Təmizləmək";
                    GlobalVarLn.objFormSPFBG.lChooseSC.Text = "Maneə sansiyası ";
                    GlobalVarLn.objFormSPFBG.label15.Text = "Xəritədən giriş";

                    GlobalVarLn.objFormSPFBG.dataGridView1.Columns[0].HeaderText = "Fmin,MHs";
                    GlobalVarLn.objFormSPFBG.dataGridView1.Columns[1].HeaderText = "Fmax,MHs";
                    GlobalVarLn.objFormSPFBG.dataGridView1.Columns[2].HeaderText = "Az min,dər";
                    GlobalVarLn.objFormSPFBG.dataGridView1.Columns[3].HeaderText = "Az max,dər";

                    // ....................................................................................
                    // Диапазоны частот и сектора радиоподавления

                    GlobalVarLn.objFormSPFB1G.Text = "Tezlik aralıqları və radiomaneə sahəsi (10-dan çox ilmayaraq)";

                    GlobalVarLn.objFormSPFB1G.bAccept.Text = "Saxlamaq";
                    GlobalVarLn.objFormSPFB1G.button2.Text = "Yükləyin";
                    GlobalVarLn.objFormSPFB1G.bClear.Text = "Təmizləmək";
                    GlobalVarLn.objFormSPFB1G.lChooseSC.Text = "Maneə sansiyası ";
                    GlobalVarLn.objFormSPFB1G.label15.Text = "Xəritədən giriş";

                    GlobalVarLn.objFormSPFB1G.dataGridView1.Columns[0].HeaderText = "Fmin,MHs";
                    GlobalVarLn.objFormSPFB1G.dataGridView1.Columns[1].HeaderText = "Fmax,MHs";
                    GlobalVarLn.objFormSPFB1G.dataGridView1.Columns[2].HeaderText = "Az min,dər";
                    GlobalVarLn.objFormSPFB1G.dataGridView1.Columns[3].HeaderText = "Az max,dər";

                    // ....................................................................................
                    // Запрещенные частоты и частоты особого внимания

                    GlobalVarLn.objFormSPFB2G.Text = "Xüsusi diqqət və qadağan olunmuş tezliklər (10-dan çox ilmayaraq)";

                    GlobalVarLn.objFormSPFB2G.lChooseSC.Text = "MS";
                    GlobalVarLn.objFormSPFB2G.label1.Text = "Restricted frequencies and frequency bands";
                    GlobalVarLn.objFormSPFB2G.label2.Text = "Frequencies and frequency bands of special attention";

                    GlobalVarLn.objFormSPFB2G.bAccept.Text = "Saxlamaq";
                    GlobalVarLn.objFormSPFB2G.button2.Text = "Yükləyin";
                    GlobalVarLn.objFormSPFB2G.bClear.Text = "Təmizləmək";
                    GlobalVarLn.objFormSPFB2G.bSendFreqForbid.Text = "Göndərmək";
                    GlobalVarLn.objFormSPFB2G.bAccept1.Text = "Saxlamaq";
                    GlobalVarLn.objFormSPFB2G.bLoad1.Text = "Yükləyin";
                    GlobalVarLn.objFormSPFB2G.bClear1.Text = "Təmizləmək";
                    GlobalVarLn.objFormSPFB2G.bSendFreqForbit1.Text = "Göndərmək";

                    GlobalVarLn.objFormSPFB2G.dgvFreqForbid.Columns[0].HeaderText = "Fmin,MHs";
                    GlobalVarLn.objFormSPFB2G.dgvFreqForbid.Columns[1].HeaderText = "Fmax,MHs";
                    GlobalVarLn.objFormSPFB2G.dgvFreqSpec.Columns[0].HeaderText = "Fmin,MHs";
                    GlobalVarLn.objFormSPFB2G.dgvFreqSpec.Columns[1].HeaderText = "Fmax,MHs";

                    // ....................................................................................
                    // Tab

                    GlobalVarLn.objFormTabG.Text = "Tezliklərin kəşfiyyatı cədvəli";

                    GlobalVarLn.objFormTabG.bGetFreq.Text = "Qəbul et";
                    GlobalVarLn.objFormTabG.bClear.Text = "Təmizləmək";

                    // ....................................................................................

                }
                // English >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
                else if (GlobalVarLn.Lang == "Srb")
                {
                    // ....................................................................................
                    // Main

                    //MapForm.GlobalVarLn.objFormSPG.button4.Text = "Ввод СП";
                    this.toolStripDropDownButton1.Text = "Карта";
                    this.openMapToolStripMenuItem1.Text = "Отворити карту";
                    this.closeMapToolStripMenuItem1.Text = "Затворити карту";
                    this.openTheHeightMatrixToolStripMenuItem.Text = "Отворити матрицу висина";
                    this.ZoomIn.Text = "Повећати размеру";
                    this.ZoomOut.Text = "Смањити размеру";
                    this.ZoomInitial.Text = "Базна размера";

                    this.toolTip1.SetToolTip(this.button11, "Станице ометања");
                    this.toolTip1.SetToolTip(this.button9, "Објекти пријатељских снага");
                    this.toolTip1.SetToolTip(this.button15, "Објекти непријатељских снага");
                    this.toolTip1.SetToolTip(this.button13, "Линија фронта пријатељских снага");
                    this.toolTip1.SetToolTip(this.button14, "Линија фронта непријатељских снага");
                    this.toolTip1.SetToolTip(this.button12, "Зона одговорности");
                    this.toolTip1.SetToolTip(this.button1, "Тактичка и радиоелектронска ситуација");
                    this.toolTip1.SetToolTip(this.button6, "Маршрута");
                    this.toolTip1.SetToolTip(this.button19, "Азимут");
                    this.toolTip1.SetToolTip(this.button10, "Зона директне видљивости");
                    this.toolTip1.SetToolTip(this.bZoneSuppressAvia, "Зона ометања радио веза контроле");
                    this.toolTip1.SetToolTip(this.button7, "Зона ометања навигације");
                    this.toolTip1.SetToolTip(this.button8, "Зона лажирања (спуфинга)");
                    this.toolTip1.SetToolTip(this.button16, "Унос и редакција фреквенцијских опсега и сектора радио извиђања");
                    this.toolTip1.SetToolTip(this.button17, "Унос и редакција фреквенцијских опсега и сектора радио ометања");
                    this.toolTip1.SetToolTip(this.button18, "Унос и редакција забрањених за ометање фреквенција и фреквенцијских опсега и фреквенција посебне пажње");
                    this.toolTip1.SetToolTip(this.button5, "Пријем табеле извиђених фреквенција");
                    this.toolTip1.SetToolTip(this.button4, "Алат за сликање екрана");

                    this.Text = "Карта";

                    this.lPC.Text = "PC";
                    this.lIPaddr.Text = "IP адреса";
                    this.label13.Text = "Порт";

                    // ....................................................................................
                    // SP

                    GlobalVarLn.objFormSPG.Text = "Станице ометања (СО)";
                    GlobalVarLn.objFormSPG.button4.Text = "Унос СО";
                    GlobalVarLn.objFormSPG.button3.Text = "Избрисати СО";
                    GlobalVarLn.objFormSPG.bAccept.Text = "Сачувати";
                    GlobalVarLn.objFormSPG.button1.Text = "Учитати";
                    GlobalVarLn.objFormSPG.bClear.Text = "Обрисати";
                    GlobalVarLn.objFormSPG.bGetFreq.Text = "Прихватити координате";
                    GlobalVarLn.objFormSPG.buttonZSP.Text = "Напред";
                    GlobalVarLn.objFormSPG.button2.Text = "Назад";

                    GlobalVarLn.objFormSPG.spView1.CurrentLabel.Text = "Тренутна локација";
                    GlobalVarLn.objFormSPG.spView1.PlannedLabel.Text = "Планирана локација";
                    GlobalVarLn.objFormSPG.spView1.NameLabel.Text = "Број";
                    GlobalVarLn.objFormSPG.spView1.TypeLabel.Text = "Тип";
                    GlobalVarLn.objFormSPG.spView1.LatitudeLabel.Text = "Ширина, степ";
                    GlobalVarLn.objFormSPG.spView1.LongitudeLabel.Text = "Дужина, степ";
                    GlobalVarLn.objFormSPG.spView1.HeightLabel.Text = "H, m";

                    GlobalVarLn.objFormSPG.spView2.CurrentLabel.Text = "Тренутна локација";
                    GlobalVarLn.objFormSPG.spView2.PlannedLabel.Text = "Планирана локација";
                    GlobalVarLn.objFormSPG.spView2.NameLabel.Text = "Број";
                    GlobalVarLn.objFormSPG.spView2.TypeLabel.Text = "Тип";
                    GlobalVarLn.objFormSPG.spView2.LatitudeLabel.Text = "Ширина, степ";
                    GlobalVarLn.objFormSPG.spView2.LongitudeLabel.Text = "Дужина, степ";
                    GlobalVarLn.objFormSPG.spView2.HeightLabel.Text = "H, m";

                    // ....................................................................................
                    // OB1

                    GlobalVarLn.objFormOB1G.Text = "Објекти пријатељских снага";

                    GlobalVarLn.objFormOB1G.bAccept.Text = "Сачувати";
                    GlobalVarLn.objFormOB1G.button1.Text = "Учитати";
                    GlobalVarLn.objFormOB1G.bClear.Text = "Обрисати";
                    GlobalVarLn.objFormOB1G.button5.Text = "Унос";
                    GlobalVarLn.objFormOB1G.button3.Text = "Избрисати";
                    GlobalVarLn.objFormOB1G.buttonZOB1.Text = "Напред";
                    GlobalVarLn.objFormOB1G.button4.Text = "Назад";

                    GlobalVarLn.objFormOB1G.dataGridView1.Columns[0].HeaderText = "Ширина, степ";
                    GlobalVarLn.objFormOB1G.dataGridView1.Columns[1].HeaderText = "Дужина, степ";
                    GlobalVarLn.objFormOB1G.dataGridView1.Columns[2].HeaderText = "H, m";
                    GlobalVarLn.objFormOB1G.dataGridView1.Columns[3].HeaderText = "Број";

                    // ....................................................................................
                    // OB2

                    GlobalVarLn.objFormOB2G.Text = "Објекти непријатељских снага";

                    GlobalVarLn.objFormOB2G.bAccept.Text = "Сачувати";
                    GlobalVarLn.objFormOB2G.button1.Text = "Учитати";
                    GlobalVarLn.objFormOB2G.bClear.Text = "Обрисати";
                    GlobalVarLn.objFormOB2G.button4.Text = "Унос";
                    GlobalVarLn.objFormOB2G.button3.Text = "Избрисати";
                    GlobalVarLn.objFormOB2G.buttonZOB1.Text = "Напред";
                    GlobalVarLn.objFormOB2G.button2.Text = "Назад";

                    GlobalVarLn.objFormOB2G.dataGridView1.Columns[0].HeaderText = "Ширина, степ";
                    GlobalVarLn.objFormOB2G.dataGridView1.Columns[1].HeaderText = "Дужина, степ";
                    GlobalVarLn.objFormOB2G.dataGridView1.Columns[2].HeaderText = "H, m";
                    GlobalVarLn.objFormOB2G.dataGridView1.Columns[3].HeaderText = "Број";
                    GlobalVarLn.objFormOB2G.dataGridView1.Columns[4].HeaderText = "F, MHz";

                    // ....................................................................................
                    // LF1

                    GlobalVarLn.objFormLFG.Text = "Линија фронта пријатељских снага";

                    GlobalVarLn.objFormLFG.bAccept.Text = "Сачувати";
                    GlobalVarLn.objFormLFG.button1.Text = "Учитати";
                    GlobalVarLn.objFormLFG.bClear.Text = "Обрисати";

                    GlobalVarLn.objFormLFG.dataGridView1.Columns[0].HeaderText = "Ширина, степ";
                    GlobalVarLn.objFormLFG.dataGridView1.Columns[1].HeaderText = "Дужина, степ";
                    GlobalVarLn.objFormLFG.dataGridView1.Columns[2].HeaderText = "H, m";

                    // ....................................................................................
                    // LF2

                    GlobalVarLn.objFormLF2G.Text = "Линија фронта непријатељских снага";

                    GlobalVarLn.objFormLF2G.bAccept.Text = "Сачувати";
                    GlobalVarLn.objFormLF2G.button1.Text = "Учитати";
                    GlobalVarLn.objFormLF2G.bClear.Text = "Обрисати";

                    GlobalVarLn.objFormLF2G.dataGridView1.Columns[0].HeaderText = "Ширина, степ";
                    GlobalVarLn.objFormLF2G.dataGridView1.Columns[1].HeaderText = "Дужина, степ";
                    GlobalVarLn.objFormLF2G.dataGridView1.Columns[2].HeaderText = "H, m";

                    // ....................................................................................
                    // ZO

                    GlobalVarLn.objFormZOG.Text = "Зона одговорности";

                    GlobalVarLn.objFormZOG.bAccept.Text = "Сачувати";
                    GlobalVarLn.objFormZOG.button1.Text = "Учитати";
                    GlobalVarLn.objFormZOG.bClear.Text = "Обрисати";

                    GlobalVarLn.objFormZOG.dataGridView1.Columns[0].HeaderText = "Ширина, степ";
                    GlobalVarLn.objFormZOG.dataGridView1.Columns[1].HeaderText = "Дужина, степ";
                    GlobalVarLn.objFormZOG.dataGridView1.Columns[2].HeaderText = "H, m";

                    // ....................................................................................
                    // TRO

                    GlobalVarLn.objFormTROG.Text = "Тактичка и радиоелектронска ситуација";

                    GlobalVarLn.objFormTROG.button1.Text = "Учитати";
                    GlobalVarLn.objFormTROG.bClear.Text = "Обрисати";

                    // ....................................................................................
                    // Azimuth

                    GlobalVarLn.objFormAz1G.Text = "Азимут";

                    GlobalVarLn.objFormAz1G.button2.Text = "Прихватити";
                    GlobalVarLn.objFormAz1G.bClear.Text = "Обрисати";

                    GlobalVarLn.objFormAz1G.lMiddleHeight.Text = "Висина, m";
                    GlobalVarLn.objFormAz1G.lXRect.Text = "Ширина, степ";
                    GlobalVarLn.objFormAz1G.lYRect.Text = "Дужина, степ";
                    GlobalVarLn.objFormAz1G.label15.Text = "Показати на карти";

                    GlobalVarLn.objFormAz1G.dataGridView1.Columns[0].HeaderText = "СО";
                    GlobalVarLn.objFormAz1G.dataGridView1.Columns[1].HeaderText = "Ширина, степ";
                    GlobalVarLn.objFormAz1G.dataGridView1.Columns[2].HeaderText = "Дужина, степ";
                    GlobalVarLn.objFormAz1G.dataGridView1.Columns[3].HeaderText = "Азимут, степ";

                    // ....................................................................................
                    // ZPV

                    GlobalVarLn.objFormLineSightRangeG.Text = "Зона директне видљивости (ЗДВ)";

                    GlobalVarLn.objFormLineSightRangeG.tabPage1.Text = "ЗДВ";
                    GlobalVarLn.objFormLineSightRangeG.tabPage2.Text = "Зона детекције СО";

                    GlobalVarLn.objFormLineSightRangeG.lCenterLSR.Text = "Центар зоне";
                    GlobalVarLn.objFormLineSightRangeG.label4.Text = "Унос координата";
                    GlobalVarLn.objFormLineSightRangeG.lXRect.Text = "Ширина, степ";
                    GlobalVarLn.objFormLineSightRangeG.lYRect.Text = "Дужина, степ";
                    GlobalVarLn.objFormLineSightRangeG.lOwnHeight.Text = "H, m";
                    GlobalVarLn.objFormLineSightRangeG.lDistSightRange.Text = "Домет, m";
                    GlobalVarLn.objFormLineSightRangeG.lMiddleHeight.Text = "Просечна висина, m";
                    GlobalVarLn.objFormLineSightRangeG.grbOwnObject.Text = "Средство ометања";
                    GlobalVarLn.objFormLineSightRangeG.label17.Text = "Висина антене, m";
                    GlobalVarLn.objFormLineSightRangeG.lHeightOwnObject.Text = "Укупна висина, m";
                    GlobalVarLn.objFormLineSightRangeG.grbOpponObject.Text = "Објект ометања";
                    GlobalVarLn.objFormLineSightRangeG.label1.Text = "Висина антене, m";
                    GlobalVarLn.objFormLineSightRangeG.lHeightOpponObject.Text = "Укупна висина, m";
                    GlobalVarLn.objFormLineSightRangeG.button1.Text = "Центар ЗДВ";
                    GlobalVarLn.objFormLineSightRangeG.bAccept.Text = "Прихватити";
                    GlobalVarLn.objFormLineSightRangeG.bClear.Text = "Обрисати";

                    GlobalVarLn.objFormLineSightRangeG.lPowerOwn.Text = "Снага предајника, W (0.1-100)";
                    GlobalVarLn.objFormLineSightRangeG.lFreq.Text = "Фреквенција предјника, MHz (100-6000)";
                    GlobalVarLn.objFormLineSightRangeG.lCoeffTransmitOpponent.Text = "Коефицијент појачања (1-6)";
                    GlobalVarLn.objFormLineSightRangeG.label2.Text = "Праг осетљивости трагача правца, dBW (-140 - -100)";
                    GlobalVarLn.objFormLineSightRangeG.label3.Text = "Полупречник зоне, m";
                    GlobalVarLn.objFormLineSightRangeG.button2.Text = "Прихватити";
                    GlobalVarLn.objFormLineSightRangeG.button3.Text = "Обрисати";

                    // ....................................................................................
                    // Route

                    GlobalVarLn.objFormWayG.Text = "Маршрута";

                    GlobalVarLn.objFormWayG.label2.Text = "Количина тачака";
                    GlobalVarLn.objFormWayG.label3.Text = "Дужиина маршруте, m";
                    GlobalVarLn.objFormWayG.button4.Text = "Сачувати";
                    GlobalVarLn.objFormWayG.button5.Text = "Учитати";
                    GlobalVarLn.objFormWayG.button3.Text = "Обрисати";

                    GlobalVarLn.objFormWayG.dataGridView1.Columns[0].HeaderText = "Ширина, степ";
                    GlobalVarLn.objFormWayG.dataGridView1.Columns[1].HeaderText = "Дужина, степ";
                    GlobalVarLn.objFormWayG.dataGridView1.Columns[2].HeaderText = "L, m";

                    // ....................................................................................
                    // FormSuppression
                    // Зона подавления радиолиний управления

                    GlobalVarLn.objFormSuppressionG.Text = "Зона ометања радио веза контроле";

                    GlobalVarLn.objFormSuppressionG.tpOwnObject.Text = "Станице ометања (СО)";
                    GlobalVarLn.objFormSuppressionG.tabPage1.Text = "Објект ометања";

                    GlobalVarLn.objFormSuppressionG.lCenterLSR.Text = "СО";

                    GlobalVarLn.objFormSuppressionG.lXRect.Text = "Ширина, степ";
                    GlobalVarLn.objFormSuppressionG.lYRect.Text = "Дужина, степ";
                    GlobalVarLn.objFormSuppressionG.lOwnHeight.Text = "H, m";
                    GlobalVarLn.objFormSuppressionG.lRadiusZone.Text = "Полупречник зоне ометања,m";
                    GlobalVarLn.objFormSuppressionG.label13.Text = "Полупречник зоне неометања,m";
                    GlobalVarLn.objFormSuppressionG.label17.Text = "Висина антене, m";
                    GlobalVarLn.objFormSuppressionG.lPowerOwn.Text = "Снага, W (10-300)";
                    GlobalVarLn.objFormSuppressionG.lCoeffOwn.Text = "Коефицијент појачања (4-8)";
                    GlobalVarLn.objFormSuppressionG.lHeightOwnObject.Text = "Укупна висина, m";

                    GlobalVarLn.objFormSuppressionG.button1.Text = "СО";
                    GlobalVarLn.objFormSuppressionG.button2.Text = "Објект ометања";
                    GlobalVarLn.objFormSuppressionG.bAccept.Text = "Прихватити";
                    GlobalVarLn.objFormSuppressionG.bClear.Text = "Обрисати";

                    // GlobalVarLn.objFormSuppressionG.gbPoint1.Text = "Object of jam";

                    GlobalVarLn.objFormSuppressionG.lPt1XRect.Text = "Ширина, степ";
                    GlobalVarLn.objFormSuppressionG.lPt1YRect.Text = "Дужина, степ";
                    GlobalVarLn.objFormSuppressionG.lPt1Height.Text = "H, m";
                    GlobalVarLn.objFormSuppressionG.label21.Text = "Коеф. ометања (1-100)";
                    GlobalVarLn.objFormSuppressionG.label12.Text = "Снага, W (1-100)";
                    GlobalVarLn.objFormSuppressionG.label14.Text = "Коефицијент појачања (1-10)";
                    GlobalVarLn.objFormSuppressionG.label11.Text = "Висина антене, m";
                    GlobalVarLn.objFormSuppressionG.label27.Text = "Висина антене (пријемник), m ";

                    // ....................................................................................
                    // FormSupSpuf
                    // Зона подавления навигации

                    GlobalVarLn.objFormSupSpufG.Text = "Зона ометања навигације";

                    GlobalVarLn.objFormSupSpufG.lCenterLSR.Text = "Центар зоне";
                    GlobalVarLn.objFormSupSpufG.lOwnHeight.Text = "H, m";
                    GlobalVarLn.objFormSupSpufG.lXRect.Text = "Ширина, степ";
                    GlobalVarLn.objFormSupSpufG.lYRect.Text = "Дужина, степ";
                    GlobalVarLn.objFormSupSpufG.lDistSightRange.Text = "Полупречник,m";
                    GlobalVarLn.objFormSupSpufG.label2.Text = "Снага, W (10-100)";
                    GlobalVarLn.objFormSupSpufG.label3.Text = "Коефицијент појачања";
                    GlobalVarLn.objFormSupSpufG.label4.Text = "Коеф. ометања, dB (30-50)";
                    GlobalVarLn.objFormSupSpufG.label17.Text = "Висина (СО) антене, m";
                    GlobalVarLn.objFormSupSpufG.label11.Text = "Висина антене, m";
                    GlobalVarLn.objFormSupSpufG.button1.Text = "Центар зоне";
                    GlobalVarLn.objFormSupSpufG.bAccept.Text = "Прихватити";
                    GlobalVarLn.objFormSupSpufG.bClear.Text = "Обрисати";

                    // ....................................................................................
                    // FormSpuf
                    // Зона спуфинга

                    GlobalVarLn.objFormSpufG.Text = "Зона лажирања (спуфинга)";

                    GlobalVarLn.objFormSpufG.lCenterLSR.Text = "Центар зоне";
                    GlobalVarLn.objFormSpufG.lOwnHeight.Text = "H, m";
                    GlobalVarLn.objFormSpufG.lXRect.Text = "Ширина, степ";
                    GlobalVarLn.objFormSpufG.lYRect.Text = "Дужина, степ";
                    GlobalVarLn.objFormSpufG.lDistSightRange.Text = "Полупречник зоне лажирања,m";
                    GlobalVarLn.objFormSpufG.label4.Text = "Полупречник зоне ометања, m";
                    GlobalVarLn.objFormSpufG.label2.Text = "Снага, W (0.1-100)";
                    GlobalVarLn.objFormSpufG.label3.Text = "Коефицијент појачања";
                    GlobalVarLn.objFormSpufG.label1.Text = "Коeфициjент лажирања, dB (7-15)";
                    GlobalVarLn.objFormSpufG.label17.Text = "Висина (СО) антене, m";
                    GlobalVarLn.objFormSpufG.label11.Text = "Висина антене, m";
                    GlobalVarLn.objFormSpufG.button1.Text = "Центар зоне";
                    GlobalVarLn.objFormSpufG.bAccept.Text = "Прихватити";
                    GlobalVarLn.objFormSpufG.bClear.Text = "Обрисати";

                    // ....................................................................................
                    // Диапазоны частот и сектора радиоразведки

                    GlobalVarLn.objFormSPFBG.Text = "Фреквенцијски опсези и сектори радио извиђања (не преко 10)";

                    GlobalVarLn.objFormSPFBG.bAccept.Text = "Сачувати";
                    GlobalVarLn.objFormSPFBG.button2.Text = "Учитати";
                    GlobalVarLn.objFormSPFBG.bClear.Text = "Обрисати";
                    GlobalVarLn.objFormSPFBG.lChooseSC.Text = "Станице ометања";
                    GlobalVarLn.objFormSPFBG.label15.Text = "Унос са карте";

                    GlobalVarLn.objFormSPFBG.dataGridView1.Columns[0].HeaderText = "Fmin,MHz";
                    GlobalVarLn.objFormSPFBG.dataGridView1.Columns[1].HeaderText = "Fmax,MHz";
                    GlobalVarLn.objFormSPFBG.dataGridView1.Columns[2].HeaderText = "Аз min,степ";
                    GlobalVarLn.objFormSPFBG.dataGridView1.Columns[3].HeaderText = "Аз max,степ";

                    // ....................................................................................
                    // Диапазоны частот и сектора радиоподавления

                    GlobalVarLn.objFormSPFB1G.Text = "Фреквенцијски опсези и сектори радио ометања (не преко 10)";

                    GlobalVarLn.objFormSPFB1G.bAccept.Text = "Сачувати";
                    GlobalVarLn.objFormSPFB1G.button2.Text = "Учитати";
                    GlobalVarLn.objFormSPFB1G.bClear.Text = "Обрисати";
                    GlobalVarLn.objFormSPFB1G.lChooseSC.Text = "Станице ометања";
                    GlobalVarLn.objFormSPFB1G.label15.Text = "Унос са карте";

                    GlobalVarLn.objFormSPFB1G.dataGridView1.Columns[0].HeaderText = "Fmin,MHz";
                    GlobalVarLn.objFormSPFB1G.dataGridView1.Columns[1].HeaderText = "Fmax,MHz";
                    GlobalVarLn.objFormSPFB1G.dataGridView1.Columns[2].HeaderText = "Аз min,степ";
                    GlobalVarLn.objFormSPFB1G.dataGridView1.Columns[3].HeaderText = "Аз max,степ";

                    // ....................................................................................
                    // Запрещенные частоты и частоты особого внимания

                    GlobalVarLn.objFormSPFB2G.Text = "Забрањене фреквенције и фреквенције посебне пажње (не преко 10)";

                    GlobalVarLn.objFormSPFB2G.lChooseSC.Text = "СО";
                    GlobalVarLn.objFormSPFB2G.label1.Text = "Забрањене фреквнеције";
                    GlobalVarLn.objFormSPFB2G.label2.Text = "Фреквенције посебне пажње";

                    GlobalVarLn.objFormSPFB2G.bAccept.Text = "Сачувати";
                    GlobalVarLn.objFormSPFB2G.button2.Text = "Учитати";
                    GlobalVarLn.objFormSPFB2G.bClear.Text = "Обрисати";
                    GlobalVarLn.objFormSPFB2G.bSendFreqForbid.Text = "Послати";
                    GlobalVarLn.objFormSPFB2G.bAccept1.Text = "Сачувати";
                    GlobalVarLn.objFormSPFB2G.bLoad1.Text = "Учитати";
                    GlobalVarLn.objFormSPFB2G.bClear1.Text = "Обрисати";
                    GlobalVarLn.objFormSPFB2G.bSendFreqForbit1.Text = "Послати";

                    GlobalVarLn.objFormSPFB2G.dgvFreqForbid.Columns[0].HeaderText = "Fmin,MHz";
                    GlobalVarLn.objFormSPFB2G.dgvFreqForbid.Columns[1].HeaderText = "Fmax,MHz";
                    GlobalVarLn.objFormSPFB2G.dgvFreqSpec.Columns[0].HeaderText = "Fmin,MHz";
                    GlobalVarLn.objFormSPFB2G.dgvFreqSpec.Columns[1].HeaderText = "Fmax,MHz";

                    // ....................................................................................
                    // Tab

                    GlobalVarLn.objFormTabG.Text = "Табела извиђених фреквенција";

                    GlobalVarLn.objFormTabG.bGetFreq.Text = "Прихватити";
                    GlobalVarLn.objFormTabG.bClear.Text = "Обрисати";

                    // ....................................................................................

                }
                // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> English

                // Russian >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
                else
                {
                    // ....................................................................................
                    // Main

                    //MapForm.GlobalVarLn.objFormSPG.button4.Text = "Ввод СП";
                    this.toolStripDropDownButton1.Text = "Карта";
                    this.openMapToolStripMenuItem1.Text = "Открыть карту";
                    this.closeMapToolStripMenuItem1.Text = "Закрыть карту";
                    this.openTheHeightMatrixToolStripMenuItem.Text = "Открыть матрицу высот";
                    this.ZoomIn.Text = "Увеличить масштаб";
                    this.ZoomOut.Text = "Уменьшить масштаб";
                    this.ZoomInitial.Text = "Базовый масштаб";

                    this.toolTip1.SetToolTip(this.button11, "Станции помех");
                    this.toolTip1.SetToolTip(this.button9, "Объекты своих войск");
                    this.toolTip1.SetToolTip(this.button15, "Объекты войск противника");
                    this.toolTip1.SetToolTip(this.button13, "Линия фронта своих войск");
                    this.toolTip1.SetToolTip(this.button14, "Линия фронта войск противника");
                    this.toolTip1.SetToolTip(this.button12, "Зона ответственности");
                    this.toolTip1.SetToolTip(this.button1, "Тактическая и радиоэлектронная обстановка");
                    this.toolTip1.SetToolTip(this.button6, "Маршрут");
                    this.toolTip1.SetToolTip(this.button19, "Азимут");
                    this.toolTip1.SetToolTip(this.button10, "Зона прямой видимости");
                    this.toolTip1.SetToolTip(this.bZoneSuppressAvia, "Зона подавления радиолиний управления");
                    this.toolTip1.SetToolTip(this.button7, "Зона подавления навигации");
                    this.toolTip1.SetToolTip(this.button8, "Зона спуфинга");
                    this.toolTip1.SetToolTip(this.button16, "Ввод и редактирование диапазонов частот и секторов радиоразведки");
                    this.toolTip1.SetToolTip(this.button17, "Ввод и редактирование диапазонов частот и секторов радиоподавления");
                    this.toolTip1.SetToolTip(this.button18, "Ввод и редактирование запрещенных для подавления частот и диапазонов частот и частот особого внимания");
                    this.toolTip1.SetToolTip(this.button5, "Прием таблицы разведанных частот");
                    this.toolTip1.SetToolTip(this.button4, "Инструмент для скриншотов");

                    this.Text = "Карта";

                    this.lPC.Text = "ПК";
                    this.lIPaddr.Text = "IP адрес";
                    this.label13.Text = "Port";

                    // ....................................................................................
                    // SP

                    GlobalVarLn.objFormSPG.Text = "Станции помех (СП)";
                    GlobalVarLn.objFormSPG.button4.Text = "Ввод СП";
                    GlobalVarLn.objFormSPG.button3.Text = "Удалить СП";
                    GlobalVarLn.objFormSPG.bAccept.Text = "Сохранить";
                    GlobalVarLn.objFormSPG.button1.Text = "Загрузить";
                    GlobalVarLn.objFormSPG.bClear.Text = "Очистить";
                    GlobalVarLn.objFormSPG.bGetFreq.Text = "Принять координаты";
                    GlobalVarLn.objFormSPG.buttonZSP.Text = "Вперед";
                    GlobalVarLn.objFormSPG.button2.Text = "Назад";

                    GlobalVarLn.objFormSPG.spView1.CurrentLabel.Text = "Текущее положение";
                    GlobalVarLn.objFormSPG.spView1.PlannedLabel.Text = "Планируемое положение";
                    GlobalVarLn.objFormSPG.spView1.NameLabel.Text = "Номер";
                    GlobalVarLn.objFormSPG.spView1.TypeLabel.Text = "Тип";
                    GlobalVarLn.objFormSPG.spView1.LatitudeLabel.Text = "Широта, град";
                    GlobalVarLn.objFormSPG.spView1.LongitudeLabel.Text = "Долгота, град";
                    GlobalVarLn.objFormSPG.spView1.HeightLabel.Text = "H, м";

                    GlobalVarLn.objFormSPG.spView2.CurrentLabel.Text = "Текущее положение";
                    GlobalVarLn.objFormSPG.spView2.PlannedLabel.Text = "Планируемое положение";
                    GlobalVarLn.objFormSPG.spView2.NameLabel.Text = "Номер";
                    GlobalVarLn.objFormSPG.spView2.TypeLabel.Text = "Тип";
                    GlobalVarLn.objFormSPG.spView2.LatitudeLabel.Text = "Широта, град";
                    GlobalVarLn.objFormSPG.spView2.LongitudeLabel.Text = "Долгота, град";
                    GlobalVarLn.objFormSPG.spView2.HeightLabel.Text = "H, м";

                    // ....................................................................................
                    // OB1

                    GlobalVarLn.objFormOB1G.Text = "Объекты своих войск";

                    GlobalVarLn.objFormOB1G.bAccept.Text = "Сохранить";
                    GlobalVarLn.objFormOB1G.button1.Text = "Загрузить";
                    GlobalVarLn.objFormOB1G.bClear.Text = "Очистить";
                    GlobalVarLn.objFormOB1G.button5.Text = "Ввод";
                    GlobalVarLn.objFormOB1G.button3.Text = "Удалить";
                    GlobalVarLn.objFormOB1G.buttonZOB1.Text = "Вперед";
                    GlobalVarLn.objFormOB1G.button4.Text = "Назад";

                    GlobalVarLn.objFormOB1G.dataGridView1.Columns[0].HeaderText = "Широта, град";
                    GlobalVarLn.objFormOB1G.dataGridView1.Columns[1].HeaderText = "Долгота, град";
                    GlobalVarLn.objFormOB1G.dataGridView1.Columns[2].HeaderText = "H, м";
                    GlobalVarLn.objFormOB1G.dataGridView1.Columns[3].HeaderText = "Название";

                    // ....................................................................................
                    // OB2

                    GlobalVarLn.objFormOB2G.Text = "Объекты войск противника";

                    GlobalVarLn.objFormOB2G.bAccept.Text = "Сохранить";
                    GlobalVarLn.objFormOB2G.button1.Text = "Загрузить";
                    GlobalVarLn.objFormOB2G.bClear.Text = "Очистить";
                    GlobalVarLn.objFormOB2G.button4.Text = "Ввод";
                    GlobalVarLn.objFormOB2G.button3.Text = "Удалить";
                    GlobalVarLn.objFormOB2G.buttonZOB1.Text = "Вперед";
                    GlobalVarLn.objFormOB2G.button2.Text = "Назад";

                    GlobalVarLn.objFormOB2G.dataGridView1.Columns[0].HeaderText = "Широта, град";
                    GlobalVarLn.objFormOB2G.dataGridView1.Columns[1].HeaderText = "Долгота, град";
                    GlobalVarLn.objFormOB2G.dataGridView1.Columns[2].HeaderText = "H, м";
                    GlobalVarLn.objFormOB2G.dataGridView1.Columns[3].HeaderText = "Название";
                    GlobalVarLn.objFormOB2G.dataGridView1.Columns[4].HeaderText = "F, МГц";

                    // ....................................................................................
                    // LF1

                    GlobalVarLn.objFormLFG.Text = "Линия фронта своих войск";

                    GlobalVarLn.objFormLFG.bAccept.Text = "Сохранить";
                    GlobalVarLn.objFormLFG.button1.Text = "Загрузить";
                    GlobalVarLn.objFormLFG.bClear.Text = "Очистить";

                    GlobalVarLn.objFormLFG.dataGridView1.Columns[0].HeaderText = "Широта, град";
                    GlobalVarLn.objFormLFG.dataGridView1.Columns[1].HeaderText = "Долгота, град";
                    GlobalVarLn.objFormLFG.dataGridView1.Columns[2].HeaderText = "H, м";

                    // ....................................................................................
                    // LF2

                    GlobalVarLn.objFormLF2G.Text = "Линия фронта противника";

                    GlobalVarLn.objFormLF2G.bAccept.Text = "Сохранить";
                    GlobalVarLn.objFormLF2G.button1.Text = "Загрузить";
                    GlobalVarLn.objFormLF2G.bClear.Text = "Очистить";

                    GlobalVarLn.objFormLF2G.dataGridView1.Columns[0].HeaderText = "Широта, град";
                    GlobalVarLn.objFormLF2G.dataGridView1.Columns[1].HeaderText = "Долгота, град";
                    GlobalVarLn.objFormLF2G.dataGridView1.Columns[2].HeaderText = "H, м";

                    // ....................................................................................
                    // ZO

                    GlobalVarLn.objFormZOG.Text = "Зона ответственности";

                    GlobalVarLn.objFormZOG.bAccept.Text = "Сохранить";
                    GlobalVarLn.objFormZOG.button1.Text = "Загрузить";
                    GlobalVarLn.objFormZOG.bClear.Text = "Очистить";

                    GlobalVarLn.objFormZOG.dataGridView1.Columns[0].HeaderText = "Широта, град";
                    GlobalVarLn.objFormZOG.dataGridView1.Columns[1].HeaderText = "Долгота, град";
                    GlobalVarLn.objFormZOG.dataGridView1.Columns[2].HeaderText = "H, м";

                    // ....................................................................................
                    // TRO

                    GlobalVarLn.objFormTROG.Text = "Тактическая и радиоэлектронная обстановка";

                    GlobalVarLn.objFormTROG.button1.Text = "Загрузить";
                    GlobalVarLn.objFormTROG.bClear.Text = "Очистить";

                    // ....................................................................................
                    // Azimuth

                    GlobalVarLn.objFormAz1G.Text = "Азимут";

                    GlobalVarLn.objFormAz1G.button2.Text = "Принять";
                    GlobalVarLn.objFormAz1G.bClear.Text = "Очистить";

                    GlobalVarLn.objFormAz1G.lMiddleHeight.Text = " Высота, м";
                    GlobalVarLn.objFormAz1G.lXRect.Text = "Широта, град";
                    GlobalVarLn.objFormAz1G.lYRect.Text = "Долгота, град";
                    GlobalVarLn.objFormAz1G.label15.Text = "Показать на карте";

                    GlobalVarLn.objFormAz1G.dataGridView1.Columns[0].HeaderText = "СП";
                    GlobalVarLn.objFormAz1G.dataGridView1.Columns[1].HeaderText = "Широта, град";
                    GlobalVarLn.objFormAz1G.dataGridView1.Columns[2].HeaderText = "Долгота, град";
                    GlobalVarLn.objFormAz1G.dataGridView1.Columns[3].HeaderText = "Азимут, град";

                    // ....................................................................................
                    // ZPV

                    GlobalVarLn.objFormLineSightRangeG.Text = "Зона прямой видимостии (ЗПВ)";

                    GlobalVarLn.objFormLineSightRangeG.tabPage1.Text = "ЗПВ";
                    GlobalVarLn.objFormLineSightRangeG.tabPage2.Text = "Зоны обнаружения СП";

                    GlobalVarLn.objFormLineSightRangeG.lCenterLSR.Text = "Центр зоны";
                    GlobalVarLn.objFormLineSightRangeG.label4.Text = "Ввод координат";
                    GlobalVarLn.objFormLineSightRangeG.lXRect.Text = "Широта, град";
                    GlobalVarLn.objFormLineSightRangeG.lYRect.Text = "Долгота, град";
                    GlobalVarLn.objFormLineSightRangeG.lOwnHeight.Text = "H, м";
                    GlobalVarLn.objFormLineSightRangeG.lDistSightRange.Text = "Дальность, м";
                    GlobalVarLn.objFormLineSightRangeG.lMiddleHeight.Text = "Средняя высота, м";
                    GlobalVarLn.objFormLineSightRangeG.grbOwnObject.Text = "Средство подавления";
                    GlobalVarLn.objFormLineSightRangeG.label17.Text = "Высота антенны,м";
                    GlobalVarLn.objFormLineSightRangeG.lHeightOwnObject.Text = "Общая высота, м";
                    GlobalVarLn.objFormLineSightRangeG.grbOpponObject.Text = "Объект подавления";
                    GlobalVarLn.objFormLineSightRangeG.label1.Text = "Высота антенны,м";
                    GlobalVarLn.objFormLineSightRangeG.lHeightOpponObject.Text = "Общая высота, м";
                    GlobalVarLn.objFormLineSightRangeG.button1.Text = "Центр ЗПВ";
                    GlobalVarLn.objFormLineSightRangeG.bAccept.Text = "Принять";
                    GlobalVarLn.objFormLineSightRangeG.bClear.Text = "Очистить";

                    GlobalVarLn.objFormLineSightRangeG.lPowerOwn.Text = "Мощность передатчика, Вт (0.1-100)";
                    GlobalVarLn.objFormLineSightRangeG.lFreq.Text = "Частота передатчика, Мгц (100-6000)";
                    GlobalVarLn.objFormLineSightRangeG.lCoeffTransmitOpponent.Text = "Коэффициент усиления (1-6)";
                    GlobalVarLn.objFormLineSightRangeG.label2.Text = "Порог чувствительности пеленгатора,дБВт(-140 - -100) ";
                    GlobalVarLn.objFormLineSightRangeG.label3.Text = "Радиус зоны, м";
                    GlobalVarLn.objFormLineSightRangeG.button2.Text = "Принять";
                    GlobalVarLn.objFormLineSightRangeG.button3.Text = "Очистить";

                    // ....................................................................................
                    // Route

                    GlobalVarLn.objFormWayG.Text = "Маршрут";

                    GlobalVarLn.objFormWayG.label2.Text = "Кол-во пунктов";
                    GlobalVarLn.objFormWayG.label3.Text = "Длина маршрута, м";
                    GlobalVarLn.objFormWayG.button4.Text = "Сохранить";
                    GlobalVarLn.objFormWayG.button5.Text = "Загрузить";
                    GlobalVarLn.objFormWayG.button3.Text = "Очистить";

                    GlobalVarLn.objFormWayG.dataGridView1.Columns[0].HeaderText = "Широта, град";
                    GlobalVarLn.objFormWayG.dataGridView1.Columns[1].HeaderText = "Долгота, град";
                    GlobalVarLn.objFormWayG.dataGridView1.Columns[2].HeaderText = "Длина, м";

                    // ....................................................................................
                    // FormSuppression
                    // Зона подавления радиолиний управления

                    GlobalVarLn.objFormSuppressionG.Text = "Зона подавления радиолиний управления";

                    GlobalVarLn.objFormSuppressionG.tpOwnObject.Text = "Станция помех (СП)";
                    GlobalVarLn.objFormSuppressionG.tabPage1.Text = "Объект подавления";

                    GlobalVarLn.objFormSuppressionG.lCenterLSR.Text = "СП";

                    GlobalVarLn.objFormSuppressionG.lXRect.Text = "Широта, град";
                    GlobalVarLn.objFormSuppressionG.lYRect.Text = "Долгота, град";
                    GlobalVarLn.objFormSuppressionG.lOwnHeight.Text = "H, м";
                    GlobalVarLn.objFormSuppressionG.lRadiusZone.Text = " Радиус зоны подавления,м";
                    GlobalVarLn.objFormSuppressionG.label13.Text = " Радиус зоны неподавления,м";
                    GlobalVarLn.objFormSuppressionG.label17.Text = "Высота антенны, м";
                    GlobalVarLn.objFormSuppressionG.lPowerOwn.Text = "Мощность, Вт (10-300)";
                    GlobalVarLn.objFormSuppressionG.lCoeffOwn.Text = "Коэффициент усиления (4-8)";
                    GlobalVarLn.objFormSuppressionG.lHeightOwnObject.Text = "Общая высота, м";

                    GlobalVarLn.objFormSuppressionG.button1.Text = "СП";
                    GlobalVarLn.objFormSuppressionG.button2.Text = "Объект подавления";
                    GlobalVarLn.objFormSuppressionG.bAccept.Text = "Принять";
                    GlobalVarLn.objFormSuppressionG.bClear.Text = "Очистить";

                    //GlobalVarLn.objFormSuppressionG.gbPoint1.Text = "Объект подавления";

                    GlobalVarLn.objFormSuppressionG.lPt1XRect.Text = "Широта,град";
                    GlobalVarLn.objFormSuppressionG.lPt1YRect.Text = "Долгота,град";
                    GlobalVarLn.objFormSuppressionG.lPt1Height.Text = "H, м";
                    GlobalVarLn.objFormSuppressionG.label21.Text = "Коэфф-т подавления(1-100)";
                    GlobalVarLn.objFormSuppressionG.label12.Text = "Мощность, Вт (1-100)";
                    GlobalVarLn.objFormSuppressionG.label14.Text = "Коэфф-т усиления(1-10)";
                    GlobalVarLn.objFormSuppressionG.label11.Text = "Высота антенны, м";
                    GlobalVarLn.objFormSuppressionG.label27.Text = "Высота антенны(приемник), м";

                    // ....................................................................................
                    // FormSupSpuf
                    // Зона подавления навигации

                    GlobalVarLn.objFormSupSpufG.Text = "Зона подавления навигации";

                    GlobalVarLn.objFormSupSpufG.lCenterLSR.Text = "Центр зоны";
                    GlobalVarLn.objFormSupSpufG.lOwnHeight.Text = "H, м";
                    GlobalVarLn.objFormSupSpufG.lXRect.Text = "Широта,град";
                    GlobalVarLn.objFormSupSpufG.lYRect.Text = "Долгота,град";
                    GlobalVarLn.objFormSupSpufG.lDistSightRange.Text = "Радиус,м";
                    GlobalVarLn.objFormSupSpufG.label2.Text = "Мощность, Вт (10-100)";
                    GlobalVarLn.objFormSupSpufG.label3.Text = "Коэфф-т усиления";
                    GlobalVarLn.objFormSupSpufG.label4.Text = "Коэфф-т подавления, дБ (30-50)";
                    GlobalVarLn.objFormSupSpufG.label17.Text = "Высота антенны(СП), м";
                    GlobalVarLn.objFormSupSpufG.label11.Text = "Высота антенны, м";
                    GlobalVarLn.objFormSupSpufG.button1.Text = "Центр зоны";
                    GlobalVarLn.objFormSupSpufG.bAccept.Text = "Принять";
                    GlobalVarLn.objFormSupSpufG.bClear.Text = "Очистить";

                    // ....................................................................................
                    // FormSpuf
                    // Зона спуфинга

                    GlobalVarLn.objFormSpufG.Text = "Зона спуфинга";

                    GlobalVarLn.objFormSpufG.lCenterLSR.Text = "Центр зоны";
                    GlobalVarLn.objFormSpufG.lOwnHeight.Text = "H, м";
                    GlobalVarLn.objFormSpufG.lXRect.Text = "Широта, град";
                    GlobalVarLn.objFormSpufG.lYRect.Text = "Долгота, град";
                    GlobalVarLn.objFormSpufG.lDistSightRange.Text = " Радиус зоны спуфинга, м";
                    GlobalVarLn.objFormSpufG.label4.Text = "Радиус зоны подавления, м";
                    GlobalVarLn.objFormSpufG.label2.Text = "Мощность, Вт (0.1-100)";
                    GlobalVarLn.objFormSpufG.label3.Text = "Коэффициент усиления";
                    GlobalVarLn.objFormSpufG.label1.Text = "Коэффициент спуфинга, дБ (7-15)";
                    GlobalVarLn.objFormSpufG.label17.Text = "Высота антенны (СП), м";
                    GlobalVarLn.objFormSpufG.label11.Text = "Высота антенны, м";
                    GlobalVarLn.objFormSpufG.button1.Text = "Центр зоны";
                    GlobalVarLn.objFormSpufG.bAccept.Text = "Принять";
                    GlobalVarLn.objFormSpufG.bClear.Text = "Очистить";

                    // ....................................................................................
                    // Диапазоны частот и сектора радиоразведки

                    GlobalVarLn.objFormSPFBG.Text = "Диапазоны частот и сектора радиоразведки(не более 10)";

                    GlobalVarLn.objFormSPFBG.bAccept.Text = "Сохранить";
                    GlobalVarLn.objFormSPFBG.button2.Text = "Загрузить";
                    GlobalVarLn.objFormSPFBG.bClear.Text = "Очистить";
                    GlobalVarLn.objFormSPFBG.lChooseSC.Text = "Станция помех";
                    GlobalVarLn.objFormSPFBG.label15.Text = "Ввод с карты";

                    GlobalVarLn.objFormSPFBG.dataGridView1.Columns[0].HeaderText = "Fmin,МГц";
                    GlobalVarLn.objFormSPFBG.dataGridView1.Columns[1].HeaderText = "Fmax,МГц";
                    GlobalVarLn.objFormSPFBG.dataGridView1.Columns[2].HeaderText = "Аз. min,град";
                    GlobalVarLn.objFormSPFBG.dataGridView1.Columns[3].HeaderText = "Аз. max,град";

                    // ....................................................................................
                    // Диапазоны частот и сектора радиоподавления

                    GlobalVarLn.objFormSPFB1G.Text = "Диапазоны частот и сектора радиоподавления ( не более 10)";

                    GlobalVarLn.objFormSPFB1G.bAccept.Text = "Сохранить";
                    GlobalVarLn.objFormSPFB1G.button2.Text = "Загрузить";
                    GlobalVarLn.objFormSPFB1G.bClear.Text = "Очистить";
                    GlobalVarLn.objFormSPFB1G.lChooseSC.Text = "Станция помех";
                    GlobalVarLn.objFormSPFB1G.label15.Text = "Ввод с карты";

                    GlobalVarLn.objFormSPFB1G.dataGridView1.Columns[0].HeaderText = "Fmin,МГц";
                    GlobalVarLn.objFormSPFB1G.dataGridView1.Columns[1].HeaderText = "Fmax,МГц";
                    GlobalVarLn.objFormSPFB1G.dataGridView1.Columns[2].HeaderText = "Аз. min,град";
                    GlobalVarLn.objFormSPFB1G.dataGridView1.Columns[3].HeaderText = "Аз. max,град";

                    // ....................................................................................
                    // Запрещенные частоты и частоты особого внимания

                    GlobalVarLn.objFormSPFB2G.Text = "Запрещенные частоты и частоты особого внимания (не более 10)";

                    GlobalVarLn.objFormSPFB2G.lChooseSC.Text = "СП";
                    GlobalVarLn.objFormSPFB2G.label1.Text = "Запрещенные частоты";
                    GlobalVarLn.objFormSPFB2G.label2.Text = "Частоты особого внимания";

                    GlobalVarLn.objFormSPFB2G.bAccept.Text = "Сохранить";
                    GlobalVarLn.objFormSPFB2G.button2.Text = "Загрузить";
                    GlobalVarLn.objFormSPFB2G.bClear.Text = "Очистить";
                    GlobalVarLn.objFormSPFB2G.bSendFreqForbid.Text = "Отправить";
                    GlobalVarLn.objFormSPFB2G.bAccept1.Text = "Сохранить";
                    GlobalVarLn.objFormSPFB2G.bLoad1.Text = "Загрузить";
                    GlobalVarLn.objFormSPFB2G.bClear1.Text = "Очистить";
                    GlobalVarLn.objFormSPFB2G.bSendFreqForbit1.Text = "Отправить";

                    GlobalVarLn.objFormSPFB2G.dgvFreqForbid.Columns[0].HeaderText = "Fmin,МГц";
                    GlobalVarLn.objFormSPFB2G.dgvFreqForbid.Columns[1].HeaderText = "Fmax,МГц";
                    GlobalVarLn.objFormSPFB2G.dgvFreqSpec.Columns[0].HeaderText = "Fmin,МГц";
                    GlobalVarLn.objFormSPFB2G.dgvFreqSpec.Columns[1].HeaderText = "Fmax,МГц";

                    // ....................................................................................
                    // Tab

                    GlobalVarLn.objFormTabG.Text = "Таблица разведанных частот";

                    GlobalVarLn.objFormTabG.bGetFreq.Text = "Принять";
                    GlobalVarLn.objFormTabG.bClear.Text = "Очистить";

                    // ....................................................................................





                }
                // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> Russian
            }
        }
        // ******************************************************************************* LangInterface







        // Обработчик кнопки Button4: save screen *******************************************************

        private void button4_Click(object sender, EventArgs e)
        {
            // -----------------------------------------------------------------------------------------

            String s = ClassMapRastrButton.f_SaveScreen(
                                               // Дочерний элемент MapElementHost -> Элемент, где ложим карту
                                               MapForm.RasterMapControl, this
                                                       );
            if(s!="")
                //MessageBox.Show(this, s);
            MessageBox.Show(new Form { TopMost = true }, s, "", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            // -------------------------------------------------------------------------------------

        }
        // ************************************************** Обработчик кнопки Button4: save screen

        // Обработчик кнопки Button5: Tab **********************************************************

        private void button5_Click(object sender, EventArgs e)
        {
         // ----------------------------------------------------------------------------------------
           GlobalVarLn.fl_Open_objFormTab = 1;

           // Закрыть все другие формы
           ClassMapRastrDopFunctions.F_Close_Form(17);
         // ----------------------------------------------------------------------------------------
            if (objFormTab == null || objFormTab.IsDisposed)
            {
                // ................................................................................
                // Очистка dataGridView

                objFormTab.dgvTab.ClearSelection();
                for (int i = 0; i < GlobalVarLn.sizeTab; i++)
                {
                    objFormTab.dgvTab.Rows.Add("", "", "", "", "", "");
                }
                GlobalVarLn.fllTab = 0;
                GlobalVarLn.list_air_tab.Clear();
                // ................................................................................

                objFormTab = new FormTab(this);
                GlobalVarLn.objFormTabG = objFormTab;
                objFormTab.Show();
            }
         // ----------------------------------------------------------------------------------------
            else
            {
                objFormTab.Show();
            }
         // ----------------------------------------------------------------------------------------

        }
        // ********************************************************** Обработчик кнопки Button5: Tab

        // Обработчик кнопки Button6: Маршрут ******************************************************

        private void button6_Click(object sender, EventArgs e)
        {
            // -------------------------------------------------------------------------------------
            GlobalVarLn.fl_Open_objFormWay = 1;
            ClassMapRastrDopFunctions.F_Close_Form(8);
            // -------------------------------------------------------------------------------------
            // 1-я загрузка формы

            if (objFormWay == null || objFormWay.IsDisposed)
            {
                objFormWay = new FormWay();
                GlobalVarLn.objFormWayG = objFormWay;
                objFormWay.Show();

                ClassMapRastrClear.f_Clear_FormWay();
            }
            // -------------------------------------------------------------------------------------
            // НЕ 1-я загрузка формы

            else
            {
                objFormWay.Show();

                objFormWay.chbWay.Checked = true;
                if (objFormWay.chbWay.Checked == true)
                {
                    GlobalVarLn.blWay_stat = true;
                    GlobalVarLn.flEndWay_stat = 1;
                }

            }
            // -------------------------------------------------------------------------------------

        }
        // ****************************************************** Обработчик кнопки Button6: Маршрут

        // Обработчик кнопки Button7: Зона подавления навигации ************************************

        private void button7_Click(object sender, EventArgs e)
        {
            // -------------------------------------------------------------------------------------
            GlobalVarLn.fl_Open_objFormSupSpuf = 1;
            ClassMapRastrDopFunctions.F_Close_Form(12);
            // -------------------------------------------------------------------------------------
            // 1-я загрузка формы

            if (objFormSupSpuf == null || objFormSupSpuf.IsDisposed)
            {
                objFormSupSpuf = new FormSupSpuf();
                GlobalVarLn.objFormSupSpufG = objFormSupSpuf;
                objFormSupSpuf.Show();

                ClassMapRastrClear.f_Clear_FormSupSpuf();
            }
            // -------------------------------------------------------------------------------------
            // не 1-я загрузка формы

            else
            {
                objFormSupSpuf.Show();
            }
            // -------------------------------------------------------------------------------------

        }
        // ************************************ Обработчик кнопки Button7: Зона подавления навигации

        // Обработчик кнопки Button8: Зона спуфинга ************************************************

        private void button8_Click(object sender, EventArgs e)
        {
            // -------------------------------------------------------------------------------------
            GlobalVarLn.fl_Open_objFormSpuf = 1;
            ClassMapRastrDopFunctions.F_Close_Form(13);
            // -------------------------------------------------------------------------------------
            // 1-я загрузка формы

            if (objFormSpuf == null || objFormSpuf.IsDisposed)
            {
                objFormSpuf = new FormSpuf();
                GlobalVarLn.objFormSpufG = objFormSpuf;
                objFormSpuf.Show();

                ClassMapRastrClear.f_Clear_FormSpuf();
            }
            // -------------------------------------------------------------------------------------
            // не 1-я загрузка формы

            else
            {
                objFormSpuf.Show();
            }
            // -------------------------------------------------------------------------------------

        }
        // ************************************************ Обработчик кнопки Button8: Зона спуфинга

        // Обработчик кнопки Button10: ЗПВ *********************************************************

        private void button10_Click(object sender, EventArgs e)
        {
            // -------------------------------------------------------------------------------------
            GlobalVarLn.fl_Open_objFormLineSightRange = 1;
            ClassMapRastrDopFunctions.F_Close_Form(10);
            // -------------------------------------------------------------------------------------
            // 1-я загрузка формы

            if (objFormLineSightRange == null || objFormLineSightRange.IsDisposed)
            {
                //GlobalVarLn.objFormLineSightRangeG.WindowState = FormWindowState.Normal;

                objFormLineSightRange = new FormLineSightRange();
                GlobalVarLn.objFormLineSightRangeG = objFormLineSightRange;
                objFormLineSightRange.Show();

                ClassMapRastrClear.f_Clear_FormLineSightRange1();
                ClassMapRastrClear.f_Clear_FormLineSightRange2();
            }
            // -------------------------------------------------------------------------------------
            // не 1-я загрузка формы

            else
            {
                objFormLineSightRange.Show();
            }
            // -------------------------------------------------------------------------------------

        }
        // ********************************************************* Обработчик кнопки Button10: ЗПВ

        // Обработчик кнопки Button11:SP ***********************************************************

        private void button11_Click(object sender, EventArgs e)
        {
            // -------------------------------------------------------------------------------------
            GlobalVarLn.fl_Open_objFormSP = 1;
            ClassMapRastrDopFunctions.F_Close_Form(1);

            GlobalVarLn.blSP_stat = true;
            GlobalVarLn.flEndSP_stat = 1;
            // -------------------------------------------------------------------------------------
            // 1-я загрузка формы

            if (objFormSP == null || objFormSP.IsDisposed)
            {
                objFormSP = new FormSP();
                GlobalVarLn.objFormSPG = objFormSP;
                objFormSP.Show();

                if (GlobalVarLn.flEndTRO_stat != 1)
                {
                    GlobalVarLn.blSP_stat = true;
                    GlobalVarLn.flEndSP_stat = 1;
                    GlobalVarLn.XCenter_SP = 0;
                    GlobalVarLn.YCenter_SP = 0;
                    GlobalVarLn.HCenter_SP = 0;
                    GlobalVarLn.flCoord_SP2 = 0;

                    GlobalVarLn.ClearListJS();

                    GlobalVarLn.iZSP = 0;
                    GlobalVarLn.objFormSPG.pbSP.BackgroundImage = imageList1.Images[0];
                }
            }
            // -------------------------------------------------------------------------------------
            else
            {
                objFormSP.Show();
            }
            // -------------------------------------------------------------------------------------

        }
        // *********************************************************** Обработчик кнопки Button11:SP

        // Button13: LF ****************************************************************************

        private void button13_Click(object sender, EventArgs e)
        {
            // -------------------------------------------------------------------------------------
            GlobalVarLn.fl_Open_objFormLF = 1;
            ClassMapRastrDopFunctions.F_Close_Form(4);

            GlobalVarLn.blLF1_stat = true;
            GlobalVarLn.flEndLF1_stat = 1;
            // -------------------------------------------------------------------------------------
            // 1-я загрузка формы

            if (objFormLF == null || objFormLF.IsDisposed)
            {

                objFormLF = new FormLF();
                GlobalVarLn.objFormLFG = objFormLF;
                objFormLF.Show();

                ClassMapRastrClear.f_Clear_FormLF();
            }
            // -------------------------------------------------------------------------------------
            else
            {
                objFormLF.Show();
            }
            // -------------------------------------------------------------------------------------

        }
        // **************************************************************************** Button13: LF

        // Button 14: LF2 **************************************************************************

        private void button14_Click(object sender, EventArgs e)
        {
            // -------------------------------------------------------------------------------------
            GlobalVarLn.fl_Open_objFormLF2 = 1;
            ClassMapRastrDopFunctions.F_Close_Form(5);

            GlobalVarLn.blLF2_stat = true;
            GlobalVarLn.flEndLF2_stat = 1;
            // -------------------------------------------------------------------------------------
            // 1-я загрузка формы

            if (objFormLF2 == null || objFormLF2.IsDisposed)
            {
                objFormLF2 = new FormLF2();
                GlobalVarLn.objFormLF2G = objFormLF2;
                objFormLF2.Show();

                ClassMapRastrClear.f_Clear_FormLF2();
            }
            // -------------------------------------------------------------------------------------
            else
            {
                objFormLF2.Show();
            }

        }
        // ************************************************************************** Button 14: LF2

        // Button12: ZO ****************************************************************************

        private void button12_Click(object sender, EventArgs e)
        {
            // -------------------------------------------------------------------------------------
            GlobalVarLn.fl_Open_objFormZO = 1;
            ClassMapRastrDopFunctions.F_Close_Form(6);

            GlobalVarLn.blZO_stat = true;
            GlobalVarLn.flEndZO_stat = 1;
            // -------------------------------------------------------------------------------------
            // 1-я загрузка формы

            if (objFormZO == null || objFormZO.IsDisposed)
            {

                objFormZO = new FormZO();
                GlobalVarLn.objFormZOG = objFormZO;
                objFormZO.Show();

                ClassMapRastrClear.f_Clear_FormZO();
            }
            // -------------------------------------------------------------------------------------
            else
            {
                objFormZO.Show();
            }
            // -------------------------------------------------------------------------------------

        }
        // **************************************************************************** Button12: ZO

        // Button1: TRO ****************************************************************************

        private void button1_Click(object sender, EventArgs e)
        {
            // -------------------------------------------------------------------------------------
            GlobalVarLn.fl_Open_objFormTRO = 1;
            ClassMapRastrDopFunctions.F_Close_Form(7);

            GlobalVarLn.blTRO_stat = true;
            GlobalVarLn.flEndTRO_stat = 1;
            GlobalVarLn.blSP_stat = true;
            GlobalVarLn.flEndSP_stat = 1;
            GlobalVarLn.blLF1_stat = true;
            GlobalVarLn.flEndLF1_stat = 1;
            GlobalVarLn.blLF2_stat = true;
            GlobalVarLn.flEndLF2_stat = 1;
            GlobalVarLn.blZO_stat = true;
            GlobalVarLn.flEndZO_stat = 1;
            GlobalVarLn.blOB1_stat = true;
            GlobalVarLn.flEndOB1_stat = 1;
            GlobalVarLn.blOB2_stat = true;
            GlobalVarLn.flEndOB2_stat = 1;
            // -------------------------------------------------------------------------------------
            // 1-я загрузка формы

            if (objFormTRO == null || objFormTRO.IsDisposed)
            {
                objFormTRO = new FormTRO();
                GlobalVarLn.objFormTROG = objFormTRO;
                objFormTRO.Show();

                ClassMapRastrClear.f_Clear_FormTRO();
            }
            // -------------------------------------------------------------------------------------
            else
            {
                objFormTRO.Show();
            }

        }
        // **************************************************************************** Button1: TRO

        // Обработчик кнопки Button9: OB1 **********************************************************

        private void button9_Click(object sender, EventArgs e)
        {
            // -------------------------------------------------------------------------------------
            GlobalVarLn.fl_Open_objFormOB1 = 1;
            ClassMapRastrDopFunctions.F_Close_Form(2);

            GlobalVarLn.blOB1_stat = true;
            GlobalVarLn.flEndOB1_stat = 1;
            // -------------------------------------------------------------------------------------
            // 1-я загрузка формы

            if (objFormOB1 == null || objFormOB1.IsDisposed)
            {

                objFormOB1 = new FormOB1();
                GlobalVarLn.objFormOB1G = objFormOB1;
                objFormOB1.Show();

                ClassMapRastrClear.f_Clear_FormOB1();
            }
            // -------------------------------------------------------------------------------------
            else
            {
                objFormOB1.Show();
            }
            // -------------------------------------------------------------------------------------

        }
        // ********************************************************** Обработчик кнопки Button9: OB1

        // Обработчик кнопки Button15: OB2 *********************************************************

        private void button15_Click(object sender, EventArgs e)
        {
            // -------------------------------------------------------------------------------------
            GlobalVarLn.fl_Open_objFormOB2 = 1;
            ClassMapRastrDopFunctions.F_Close_Form(3);

            GlobalVarLn.blOB2_stat = true;
            GlobalVarLn.flEndOB2_stat = 1;
            // -------------------------------------------------------------------------------------
            // 1-я загрузка формы

            if (objFormOB2 == null || objFormOB2.IsDisposed)
            {

                objFormOB2 = new FormOB2();
                GlobalVarLn.objFormOB2G = objFormOB2;
                objFormOB2.Show();

                ClassMapRastrClear.f_Clear_FormOB2();
            }
            // -------------------------------------------------------------------------------------
            else
            {
                objFormOB2.Show();
            }
            // -------------------------------------------------------------------------------------
        }
        // ********************************************************* Обработчик кнопки Button15: OB2

        // Обработчик кнопки : Зона подавления радиолиний управления *******************************

        private void bZoneSuppressAvia_Click(object sender, EventArgs e)
        {
            // -------------------------------------------------------------------------------------
            GlobalVarLn.fl_Open_objFormSuppression = 1;
            ClassMapRastrDopFunctions.F_Close_Form(11);
            // -------------------------------------------------------------------------------------
            // 1-я загрузка формы

            if (objFormSuppression == null || objFormSuppression.IsDisposed)
            {
                objFormSuppression = new FormSuppression();
                GlobalVarLn.objFormSuppressionG = objFormSuppression;
                objFormSuppression.Show();

                ClassMapRastrClear.f_Clear_FormSuppression();
            }
            else
            {
                objFormSuppression.Show();
            }

        }
        // ******************************* Обработчик кнопки : Зона подавления радиолиний управления

        // Button16 :Диапазоны частот и секторов радиоразведки *************************************

        private void button16_Click(object sender, EventArgs e)
        {
            // -------------------------------------------------------------------------------------
            GlobalVarLn.fl_Open_objFormSPFB = 1;
            ClassMapRastrDopFunctions.F_Close_Form(14);
            GlobalVarLn.flF_f1 = 1;
            // -------------------------------------------------------------------------------------
            // 1-я загрузка формы

            if (objFormSPFB == null || objFormSPFB.IsDisposed)
            {
                objFormSPFB = new FormSPFB();
                GlobalVarLn.objFormSPFBG = objFormSPFB;
                objFormSPFB.Show();

                ClassMapRastrClear.f_Clear_FormSPFB();
            }
            // -------------------------------------------------------------------------------------
            else
            {
                objFormSPFB.Show();
            }
            // -------------------------------------------------------------------------------------
        }
        // ************************************* Button16 :Диапазоны частот и секторов радиоразведки

        // Button17 :Диапазоны частот и секторов РП ************************************************

        private void button17_Click(object sender, EventArgs e)
        {
            // -------------------------------------------------------------------------------------
            GlobalVarLn.fl_Open_objFormSPFB1 = 1;
            ClassMapRastrDopFunctions.F_Close_Form(15);
            GlobalVarLn.flF_f2 = 1;
            // -------------------------------------------------------------------------------------
            // 1-я загрузка формы

            if (objFormSPFB1 == null || objFormSPFB1.IsDisposed)
            {

                objFormSPFB1 = new FormSPFB1();
                GlobalVarLn.objFormSPFB1G = objFormSPFB1;
                objFormSPFB1.Show();

                ClassMapRastrClear.f_Clear_FormSPFB1();
            }
            // -------------------------------------------------------------------------------------
            else
            {
                objFormSPFB1.Show();
            }
            // -------------------------------------------------------------------------------------
        }
        // ************************************************ Button17 :Диапазоны частот и секторов РП

        // Button18: запрещенные частоты ***********************************************************

        private void button18_Click(object sender, EventArgs e)
        {
            // -------------------------------------------------------------------------------------
            GlobalVarLn.fl_Open_objFormSPFB2 = 1;
            ClassMapRastrDopFunctions.F_Close_Form(16);

            GlobalVarLn.flF_f3 = 1;
            GlobalVarLn.flF1_f3 = 1;
            // -------------------------------------------------------------------------------------
            // 1-я загрузка формы

            if (objFormSPFB2 == null || objFormSPFB2.IsDisposed)
            {
                objFormSPFB2 = new FormSPFB2(this);
                GlobalVarLn.objFormSPFB2G = objFormSPFB2;
                objFormSPFB2.Show();

                ClassMapRastrClear.f_Clear_FormSPFB2_1();
                ClassMapRastrClear.f_Clear_FormSPFB2_2();
            }
            // -------------------------------------------------------------------------------------
            else
            {
                objFormSPFB2.Show();
            }
        }
        // *********************************************************** Button18: запрещенные частоты

        // Обработчик кнопки : Azimut1 *************************************************************

        private void button19_Click(object sender, EventArgs e)
        {
            // -------------------------------------------------------------------------------------
            GlobalVarLn.fl_Open_objFormAz1 = 1;
            ClassMapRastrDopFunctions.F_Close_Form(9);

            GlobalVarLn.blOB_az1 = true;
            GlobalVarLn.flEndOB_az1 = 1;
            // -------------------------------------------------------------------------------------
            // 1-я загрузка формы

            if (objFormAz1 == null || objFormAz1.IsDisposed)
            {
                objFormAz1 = new FormAz1();
                GlobalVarLn.objFormAz1G = objFormAz1;
                objFormAz1.Show();

                ClassMapRastrClear.f_Clear_FormAz1();
            }
            // -------------------------------------------------------------------------------------
            else
            {
                objFormAz1.Show();
            }
            // -------------------------------------------------------------------------------------

        }
        // ************************************************************* Обработчик кнопки : Azimut1


        // CONNECTION ******************************************************************************

        // -----------------------------------------------------------------------------------------
        private void bConnectPC_Click(object sender, EventArgs e)
        {
            bool blConnect = false;

            if (bConnectPC.BackColor == System.Drawing.Color.Red)
            {
                blConnect = true;
                GlobalVarLn.flllsp = 1;
            
            }

            InitConnectionPC(blConnect);
        }
        // -----------------------------------------------------------------------------------------
        // INIT CONNECTION

        private void InitConnectionPC(bool blConnect)
        {
            String s1;
            int nn;

            if (blConnect)
            {
                if (clientPC != null)
                    clientPC = null;

                // sob
                //clientPC = new ClientPC(3);
                clientPC = new ClientPC((byte)GlobalVarLn.AdressOwn);

                // GPS
                // sob
                clientPC.OnConfirmCoord += clientPC_OnConfirmCoord;
                GlobalVarLn.clientPCG = clientPC;

                // connect
                clientPC.OnConnect += new ClientPC.ConnectEventHandler(ShowConnectPC);

                // disconnect 
                clientPC.OnDisconnect += new ClientPC.ConnectEventHandler(ShowDisconnectPC);

                // sob
                clientPC.OnConfirmReconFWS += new ClientPC.ConfirmReconFWSEventHandler(clientPC_OnConfirmReconFWS);

                s1 = tbIP.Text;
                nn = Convert.ToInt32(tbPort.Text);
                //clientPC.Connect("127.0.0.1", 9101);
                clientPC.Connect(s1, nn);

            }
            else
            {
                clientPC.Disconnect();

                clientPC = null;
                GlobalVarLn.flllsp = 0;

            }
        }
        // -----------------------------------------------------------------------------------------

        private void clientPC_OnConfirmReconFWS(object sender, byte bAddress, byte bCodeError, TReconFWS[] tReconFWS)
        {
            if (UpdateTableReconFWS != null)
                UpdateTableReconFWS(tReconFWS);
        }
        // -----------------------------------------------------------------------------------------
        /// <summary>
        /// если подключено -- зеленая лампочка
        /// </summary>
        /// <param name="sender"></param>
        /// 
        private void ShowConnectPC(object sender)
        {
            if (bConnectPC.InvokeRequired)
            {
                //0206
                bConnectPC.Invoke((MethodInvoker)(delegate()
                {
                    bConnectPC.BackColor = System.Drawing.Color.Green;

                }));
 
            }
            else
            {
                ;
                //0206
                bConnectPC.BackColor = System.Drawing.Color.Green;
            }
        }
        // -----------------------------------------------------------------------------------------
        /// <summary>
        /// если не подключено -- краскная лампочка
        /// </summary>
        /// <param name="sender"></param>
        /// 
        private void ShowDisconnectPC(object sender)
        {
            if (bConnectPC.InvokeRequired)
            {
                ;
                //0206
/*
                bConnectPC.Invoke((MethodInvoker)(delegate()
                {
                    bConnectPC.BackColor = Color.Red;
                }));
 */
            }
            else
            {
                //0206
                bConnectPC.BackColor = System.Drawing.Color.Red;
            }
        }
        // -----------------------------------------------------------------------------------------

        // ****************************************************************************** CONNECTION

        // CLOSE_MapForm ***************************************************************************
        private void MapForm_FormClosing(object sender, FormClosingEventArgs e)
        {

/*
            if (MapIsOpenned == true)
            {
                RasterMapControl.CloseMap();
            }
            if (HeightMatrixIsOpenned == true)
            {
                RasterMapControl.CloseDted();
                HeightMatrixIsOpenned = false;
            }
            iniRW.write_map_scl((int)RasterMapControl.Resolution);
*/

            string s = "";
            s = ClassMapRastrMenu.f_CloseMapMenu(
                                     // Дочерний элемент MapElementHost -> Элемент, где ложим карту
                                     MapForm.RasterMapControl,
                                     ref GlobalVarLn.MapOpened,
                                     ref GlobalVarLn.MatrixOpened
                                              );

            if (s != "")
                MessageBox.Show(s);


        }
        // *************************************************************************** CLOSE_MapForm

        // Центровка по СП1 ************************************************************************

        private void toolStripButton1_Click(object sender, EventArgs e)
        {
            string s = "";
            s = ClassMapRastrMenu.f_CenterMapTo(
                                     // Дочерний элемент MapElementHost -> Элемент, где ложим карту
                                     MapForm.RasterMapControl,
                                     GlobalVarLn.listJS[0].CurrentPosition.y,
                                     GlobalVarLn.listJS[0].CurrentPosition.x
                                              );
            if (s != "")
                MessageBox.Show(s);


            //Mapsui.Geometries.Point wgs84Points = new Mapsui.Geometries.Point();
            //Location location = new Location(27.111, 54.111);
            //wgs84Points = Mercator.FromLonLat(location.ToPoint().X, location.ToPoint().Y);

            //ClassMapRastrMenu.f_CenterMapTo.NavigateTo(wgs84Points);
        }
        // ************************************************************************ Центровка по СП1

        // Центровка по СП2 ************************************************************************

        private void toolStripButton2_Click(object sender, EventArgs e)
        {
            string s = "";
            s = ClassMapRastrMenu.f_CenterMapTo(
                                     // Дочерний элемент MapElementHost -> Элемент, где ложим карту
                                     MapForm.RasterMapControl,
                                     GlobalVarLn.listJS[1].CurrentPosition.y,
                                     GlobalVarLn.listJS[1].CurrentPosition.x
                                              );
            if (s != "")
                MessageBox.Show(s);
        }
        // ************************************************************************ Центровка по СП2

        // --------------------------------------------------------------------------------------------
        private void toolStripDropDownButton1_Click(object sender, EventArgs e)
        {
            ;
        }

        private void mapCompositionToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ;
        }
        private void button2_Click_2(object sender, EventArgs e)
        {
            ;
        }

        private void toolStrip1_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {
            ;
        }
        private void button2_Click(object sender, EventArgs e)
        {
            ;
        }

        private void составКартыToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ;
        } 

        private void cmStripMap_Opening(object sender, CancelEventArgs e)
        {
            ;
        }

        private void contextMenuStrip1_Opening(object sender, CancelEventArgs e)
        {
            ;
        }

        private void menuStrip1_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {
            ;
        }
        private void button2_Click_1(object sender, EventArgs e)
        {
            ;
        }

       
        // --------------------------------------------------------------------------------------------


    } // Class
} // namespace
