﻿namespace GrozaMap
{
    partial class FormSPFB2
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.saveFileDialog1 = new System.Windows.Forms.SaveFileDialog();
            this.dgvFreqForbid = new System.Windows.Forms.DataGridView();
            this.Column2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.button2 = new System.Windows.Forms.Button();
            this.bSendFreqForbid = new System.Windows.Forms.Button();
            this.lChooseSC = new System.Windows.Forms.Label();
            this.cbChooseSC = new System.Windows.Forms.ComboBox();
            this.button1 = new System.Windows.Forms.Button();
            this.bClear = new System.Windows.Forms.Button();
            this.bAccept = new System.Windows.Forms.Button();
            this.dgvFreqSpec = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.bLoad1 = new System.Windows.Forms.Button();
            this.bSendFreqForbit1 = new System.Windows.Forms.Button();
            this.bClear1 = new System.Windows.Forms.Button();
            this.bAccept1 = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.dgvFreqForbid)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvFreqSpec)).BeginInit();
            this.SuspendLayout();
            // 
            // dgvFreqForbid
            // 
            this.dgvFreqForbid.AllowUserToAddRows = false;
            this.dgvFreqForbid.AllowUserToDeleteRows = false;
            this.dgvFreqForbid.BackgroundColor = System.Drawing.SystemColors.ControlLightLight;
            this.dgvFreqForbid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvFreqForbid.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column2,
            this.Column1});
            this.dgvFreqForbid.Location = new System.Drawing.Point(128, 23);
            this.dgvFreqForbid.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.dgvFreqForbid.Name = "dgvFreqForbid";
            this.dgvFreqForbid.RowHeadersWidth = 51;
            this.dgvFreqForbid.Size = new System.Drawing.Size(312, 276);
            this.dgvFreqForbid.TabIndex = 235;
            // 
            // Column2
            // 
            this.Column2.HeaderText = "Fmin, MHz";
            this.Column2.MinimumWidth = 6;
            this.Column2.Name = "Column2";
            this.Column2.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.Column2.Width = 85;
            // 
            // Column1
            // 
            this.Column1.HeaderText = "Fmax, MHz";
            this.Column1.MinimumWidth = 6;
            this.Column1.Name = "Column1";
            this.Column1.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.Column1.Width = 85;
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(4, 191);
            this.button2.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(116, 28);
            this.button2.TabIndex = 238;
            this.button2.Text = "Load";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // bSendFreqForbid
            // 
            this.bSendFreqForbid.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.bSendFreqForbid.Location = new System.Drawing.Point(4, 262);
            this.bSendFreqForbid.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.bSendFreqForbid.Name = "bSendFreqForbid";
            this.bSendFreqForbid.Size = new System.Drawing.Size(116, 28);
            this.bSendFreqForbid.TabIndex = 237;
            this.bSendFreqForbid.Text = "Send";
            this.bSendFreqForbid.UseVisualStyleBackColor = true;
            this.bSendFreqForbid.Click += new System.EventHandler(this.bSendFreqForbid_Click);
            // 
            // lChooseSC
            // 
            this.lChooseSC.AutoSize = true;
            this.lChooseSC.Location = new System.Drawing.Point(27, 21);
            this.lChooseSC.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lChooseSC.Name = "lChooseSC";
            this.lChooseSC.Size = new System.Drawing.Size(24, 17);
            this.lChooseSC.TabIndex = 232;
            this.lChooseSC.Text = "JS";
            // 
            // cbChooseSC
            // 
            this.cbChooseSC.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbChooseSC.DropDownWidth = 150;
            this.cbChooseSC.FormattingEnabled = true;
            this.cbChooseSC.Items.AddRange(new object[] {
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            ""});
            this.cbChooseSC.Location = new System.Drawing.Point(5, 44);
            this.cbChooseSC.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.cbChooseSC.Name = "cbChooseSC";
            this.cbChooseSC.Size = new System.Drawing.Size(64, 24);
            this.cbChooseSC.TabIndex = 231;
            this.cbChooseSC.SelectedIndexChanged += new System.EventHandler(this.cbChooseSC_SelectedIndexChanged);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(167, 422);
            this.button1.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(133, 28);
            this.button1.TabIndex = 236;
            this.button1.Text = "Читать все";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Visible = false;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // bClear
            // 
            this.bClear.Location = new System.Drawing.Point(4, 226);
            this.bClear.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.bClear.Name = "bClear";
            this.bClear.Size = new System.Drawing.Size(116, 28);
            this.bClear.TabIndex = 234;
            this.bClear.Text = "Clear";
            this.bClear.UseVisualStyleBackColor = true;
            this.bClear.Click += new System.EventHandler(this.bClear_Click);
            // 
            // bAccept
            // 
            this.bAccept.Location = new System.Drawing.Point(4, 155);
            this.bAccept.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.bAccept.Name = "bAccept";
            this.bAccept.Size = new System.Drawing.Size(116, 28);
            this.bAccept.TabIndex = 233;
            this.bAccept.Text = "Save";
            this.bAccept.UseVisualStyleBackColor = true;
            this.bAccept.Click += new System.EventHandler(this.bAccept_Click);
            // 
            // dgvFreqSpec
            // 
            this.dgvFreqSpec.AllowUserToAddRows = false;
            this.dgvFreqSpec.AllowUserToDeleteRows = false;
            this.dgvFreqSpec.BackgroundColor = System.Drawing.SystemColors.ControlLightLight;
            this.dgvFreqSpec.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvFreqSpec.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn1,
            this.dataGridViewTextBoxColumn2});
            this.dgvFreqSpec.Location = new System.Drawing.Point(574, 28);
            this.dgvFreqSpec.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.dgvFreqSpec.Name = "dgvFreqSpec";
            this.dgvFreqSpec.RowHeadersWidth = 51;
            this.dgvFreqSpec.Size = new System.Drawing.Size(312, 268);
            this.dgvFreqSpec.TabIndex = 239;
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.HeaderText = "Fmin, MHz";
            this.dataGridViewTextBoxColumn1.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            this.dataGridViewTextBoxColumn1.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn1.Width = 85;
            // 
            // dataGridViewTextBoxColumn2
            // 
            this.dataGridViewTextBoxColumn2.HeaderText = "Fmax, MHz";
            this.dataGridViewTextBoxColumn2.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            this.dataGridViewTextBoxColumn2.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn2.Width = 85;
            // 
            // bLoad1
            // 
            this.bLoad1.Location = new System.Drawing.Point(454, 188);
            this.bLoad1.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.bLoad1.Name = "bLoad1";
            this.bLoad1.Size = new System.Drawing.Size(110, 28);
            this.bLoad1.TabIndex = 243;
            this.bLoad1.Text = "Load";
            this.bLoad1.UseVisualStyleBackColor = true;
            this.bLoad1.Click += new System.EventHandler(this.bLoad1_Click);
            // 
            // bSendFreqForbit1
            // 
            this.bSendFreqForbit1.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.bSendFreqForbit1.Location = new System.Drawing.Point(454, 260);
            this.bSendFreqForbit1.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.bSendFreqForbit1.Name = "bSendFreqForbit1";
            this.bSendFreqForbit1.Size = new System.Drawing.Size(110, 28);
            this.bSendFreqForbit1.TabIndex = 242;
            this.bSendFreqForbit1.Text = "Send";
            this.bSendFreqForbit1.UseVisualStyleBackColor = true;
            this.bSendFreqForbit1.Click += new System.EventHandler(this.bSendFreqForbit1_Click);
            // 
            // bClear1
            // 
            this.bClear1.Location = new System.Drawing.Point(454, 224);
            this.bClear1.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.bClear1.Name = "bClear1";
            this.bClear1.Size = new System.Drawing.Size(110, 28);
            this.bClear1.TabIndex = 241;
            this.bClear1.Text = "Clear";
            this.bClear1.UseVisualStyleBackColor = true;
            this.bClear1.Click += new System.EventHandler(this.bClear1_Click);
            // 
            // bAccept1
            // 
            this.bAccept1.Location = new System.Drawing.Point(454, 153);
            this.bAccept1.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.bAccept1.Name = "bAccept1";
            this.bAccept1.Size = new System.Drawing.Size(110, 28);
            this.bAccept1.TabIndex = 240;
            this.bAccept1.Text = "Save";
            this.bAccept1.UseVisualStyleBackColor = true;
            this.bAccept1.Click += new System.EventHandler(this.bAccept1_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(128, 4);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(288, 17);
            this.label1.TabIndex = 244;
            this.label1.Text = "Restricted frequencies and frequency bands";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(573, 5);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(347, 17);
            this.label2.TabIndex = 245;
            this.label2.Text = "Frequencies and frequency bands of special attention";
            // 
            // FormSPFB2
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(923, 304);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.bLoad1);
            this.Controls.Add(this.bSendFreqForbit1);
            this.Controls.Add(this.bClear1);
            this.Controls.Add(this.bAccept1);
            this.Controls.Add(this.dgvFreqSpec);
            this.Controls.Add(this.dgvFreqForbid);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.bSendFreqForbid);
            this.Controls.Add(this.lChooseSC);
            this.Controls.Add(this.cbChooseSC);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.bClear);
            this.Controls.Add(this.bAccept);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Location = new System.Drawing.Point(900, 100);
            this.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FormSPFB2";
            this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
            this.Text = "Restricted frequencies and frequencies of special attention (not more than 10)";
            this.TopMost = true;
            this.Activated += new System.EventHandler(this.FormSPFB2_Activated);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FormSPFB2_FormClosing);
            this.Load += new System.EventHandler(this.FormSPFB2_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgvFreqForbid)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvFreqSpec)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.SaveFileDialog saveFileDialog1;
        public System.Windows.Forms.DataGridView dgvFreqForbid;
        public System.Windows.Forms.ComboBox cbChooseSC;
        private System.Windows.Forms.Button button1;
        public System.Windows.Forms.DataGridView dgvFreqSpec;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column2;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        public System.Windows.Forms.Button button2;
        public System.Windows.Forms.Button bSendFreqForbid;
        public System.Windows.Forms.Label lChooseSC;
        public System.Windows.Forms.Button bClear;
        public System.Windows.Forms.Button bAccept;
        public System.Windows.Forms.Button bLoad1;
        public System.Windows.Forms.Button bSendFreqForbit1;
        public System.Windows.Forms.Button bClear1;
        public System.Windows.Forms.Button bAccept1;
        public System.Windows.Forms.Label label1;
        public System.Windows.Forms.Label label2;
    }
}