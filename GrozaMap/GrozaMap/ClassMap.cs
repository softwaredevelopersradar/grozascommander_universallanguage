﻿
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Runtime.InteropServices;
using System.Windows.Forms;

using System.IO;

using System.Windows.Input;
using Mapsui.Geometries;
using Mapsui.Styles;
using Mapsui.Projection;
using WpfMapControl;
using Bitmap = System.Drawing.Bitmap;
using Point = System.Drawing.Point;

namespace GrozaMap
{

    public class ClassMap
    {

        // VARS *******************************************************************************

        //public static double LAMBDA=300000;

        // Координаты -------------------------------------------------------------------------

        // Число угловых секунд в радиане
        private double ro;

        // Эллипсоид Красовского
        private double aP;  // Большая полуось
        private double a1P; // Сжатие
        private double e2P; // Квадрат эксцентриситета

        // Эллипсоид WGS84 (GRS80, эти два эллипсоида сходны по большинству параметров)
        private double aW;  // Большая полуось
        private double a1W; // Сжатие
        private double e2W; // Квадрат эксцентриситета

        // Вспомогательные значения для преобразования эллипсоидов
        private double a;
        private double da;
        private double e2;
        private double de2;

        // Угловые элементы трансформирования, в секундах
        private double wx;
        private double wy;
        private double wz;

        // Дифференциальное различие масштабов
        private double ms;

        // Флаг НУ
        private int fl_first2_Coord;
        // ------------------------------------------------------------------------Координаты

        // Пеленг PelPelPelPelPelPelPelPelPelPelPelPelPelPelPelPelPelPelPelPelPelP

        // Флаг НУ
        private int fl_first_Pel;

        // Вспомогательные флаги
        //private int fl1_Pel;

        // Индекс фиктивной точки (1,2, ...)
        private uint IndFP_Pel;
        // Индекс i-й фиктивной точки в массивах(0,...)
        private uint IndMas_Pel;

        // Радиус Земли (шар)
        private double REarth_Pel;
        // Радиус Земли  в точке стояния пеленгатора с учетом текущей широты 
        private double REarthP_Pel;

        // Координаты фиктивной точки в опорной геоцентрической СК
        // (Поворот СК1 на угол=долготе пеленгатора)
        private double XiG_Pel;
        private double YiG_Pel;
        private double ZiG_Pel;

        // Угловые координаты фиктивной точки в опорной геоцентрической СК
        private double Ri_Pel;

        // Широта и долгота стояния пеленгатора
        private double LatP_Pel_tmp;
        private double LongP_Pel_tmp;

        // Количество фиктивных точек в плоскости пеленгования пеленгатора
        private uint NumbFikt_Pel_tmp;

        // Max дальность отображения пеленга
        private double Mmax_Pel_tmp;

        // Пеленг
        private double Theta_Pel_tmp;

        // Координаты пеленгатора в опорной геоцентрической СК
        private double XP_Pel_tmp;
        private double YP_Pel_tmp;
        private double ZP_Pel_tmp;

        // ----------------------------------------------------------------
        // !!! VAR2

        private double Lat_Pel_N;  //Lat(n)
        private double Lat_Pel_N1;  //Lat(n-1)
        private double Long_Pel_N;  //Long(n)
        private double Long_Pel_N1;  //Long(n-1)
        private double V_Pel;

        private int fl_var2;
        private int fl1_var2;
        private int fl2_var2;

        // ----------------------------------------------------------------

        // PelPelPelPelPelPelPelPelPelPelPelPelPelPelPelPelPelPelPelPelPelP Пеленг

        // ******************************************************************************* VARS

        // Конструктор *************************************************************************

        public ClassMap()
        {

            // Координаты ---------------------------------------------------------------------
            //Pi = 3.14159265358979;

            // Число угловых секунд в радиане
            ro = 206264.8062;

            // Эллипсоид Красовского
            aP = 6378245;  // Большая полуось
            a1P = 0; // Сжатие
            e2P = 0; // Квадрат эксцентриситета

            // Эллипсоид WGS84 (GRS80, эти два эллипсоида сходны по большинству параметров)
            aW = 6378137;  // Большая полуось
            a1W = 0; // Сжатие
            e2W = 0; // Квадрат эксцентриситета

            // Вспомогательные значения для преобразования эллипсоидов
            a = 0;
            da = 0;
            e2 = 0;
            de2 = 0;

            // Линейные элементы трансформирования, в метрах
            // ГОСТ 51794_2001
            //dx = 23.92;
            //dy = -141.27;
            //dz = -80.9;
            //wx = 0;
            //wy = 0.35;
            //wz = 0.82;
            //ms = -0.000012;

            // ГОСТ 51794_2008
            //dx = 25;
            //dy = -141;
            //dz = -80;
            //wx=0; 
            //wy = -0.35;
            //wz = -0.66;
            //ms = 0;
            //dx = 28;
            //dy = -130;
            //dz = -95;
            wx = 0;
            wy = 0;
            wz = 0;
            ms = 0;

            //dx = 28;
            //dy = -130;
            //dz = -95;

            // Флаг НУ
            fl_first2_Coord = 0;
            // ---------------------------------------------------------------------Координаты

            // Пеленг PelPelPelPelPelPelPelPelPelPelPelPelPelPelPelPelPelPelPelPelPelP
            // Флаг НУ
            fl_first_Pel = 0;

            // Вспомогательные флаги
            //fl1_Pel = 0;

            // Индекс фиктивной точки (1,2, ...)
            IndFP_Pel = 1;
            // Индекс i-й фиктивной точки в массивах(0,...)
            IndMas_Pel = 0;

            // Радиус Земли (шар)
            REarth_Pel = 6378245;
            // Радиус Земли  в точке стояния пеленгатора с учетом текущей широты 
            REarthP_Pel = 0;
            // Радиус Земли  в (i-1)-й фиктивной точке с учетом текущей широты 
            //REarthF_Pel=0;

            // Координаты фиктивной точки в опорной геоцентрической СК
            // (Поворот СК1 на угол=долготе пеленгатора)
            XiG_Pel = 0;
            YiG_Pel = 0;
            ZiG_Pel = 0;

            // Угловые координаты фиктивной точки в опорной геоцентрической СК
            Ri_Pel = 0;

            // Широта и долгота стояния пеленгатора
            LatP_Pel_tmp = 0;
            LongP_Pel_tmp = 0;

            // Количество фиктивных точек в плоскости пеленгования пеленгатора
            NumbFikt_Pel_tmp = 0;

            // Max дальность отображения пеленга
            Mmax_Pel_tmp = 0;

            // Пеленг
            Theta_Pel_tmp = 0;

            // Счетчик
            //sch_Pel = 0;

            // Координаты пеленгатора в опорной геоцентрической СК
            XP_Pel_tmp = 0;
            YP_Pel_tmp = 0;
            ZP_Pel_tmp = 0;

            // ----------------------------------------------------------------
            // !!! VAR2

            Lat_Pel_N = 0;  //Lat(n)
            Lat_Pel_N1 = 0;  //Lat(n-1)
            Long_Pel_N = 0;  //Long(n)
            Long_Pel_N1 = 0;  //Long(n-1)
            V_Pel = 0;

            fl_var2 = 0;
            fl1_var2 = 0;
            fl2_var2 = 0;

            // ----------------------------------------------------------------

            // PelPelPelPelPelPelPelPelPelPelPelPelPelPelPelPelPelPelPelPelPelP Пеленг


        } // Конструктор
        // ************************************************************************* Конструктор

        // *************************************************************************************
        // Module
        // *************************************************************************************
        public double module(double a)
        {
            if (a >= 0) return (a);
            else return (-a);

        } // P/P module
        // *************************************************************************************

        // ***********************************************************************
        // Преобразование dd.ddddd (grad) -> DD MM SS 
        // (дробное значение градусов -> в градусы(целое значение), минуты(целое значение),
        // секунды(дробное значение))

        // Входные параметры:
        // Grad_Dbl_Coord - градусы в дробном виде

        // Выходные параметры:
        // Grad_I_Coord - градусы (целое)
        // Min_Coord - минуты (целое)
        // Sec_Coord - секунды (дробное)
        // ***********************************************************************
        public void f_Grad_GMS
            (

                // Входные параметры (grad)
                double Grad_Dbl_Coord,

                // Выходные параметры 
                ref int Grad_I_Coord,
                ref int Min_Coord,
                ref double Sec_Coord

            )
        {

            Grad_I_Coord = (int)(Grad_Dbl_Coord);

            Min_Coord = (int)((module(Grad_Dbl_Coord) - module(Grad_I_Coord)) * 60);

            Sec_Coord = ((module(Grad_Dbl_Coord) - module(Grad_I_Coord)) * 60 - Min_Coord) * 60;

        } // Функция f_Grad_GMS
        //************************************************************************

        // ***********************************************************************
        // Расчет приращения по долготе при преобразованиях координат WGS84 (эллипсоид)
        // <-> эллипсоид Красовского(Пулково-42)   (преобразования Молоденского) в
        //  угл.сек

        // Входные параметры:
        // Lat_Coord_8442 - широта (град)
        // Long_Coord_8442 - долгота (град)
        // H_Coord_8442 - высота (км)
        // dX_Coord,dY_Coord,dZ_Coord - !!! это д.б.глобальные переменные, в которых
        // хранится DATUM (м) 

        // Выходные параметры:
        // dLong_Coord - приращение по долготе, !!! угловые секунды
        // ***********************************************************************
        public void f_dLong
            (

                // Входные параметры (внутри функции переводятся в рад и м)
                double Lat_Coord_8442,   // широта   (grad)
                double Long_Coord_8442,  // долгота  (grad)
                double H_Coord_8442,     // высота   (km)

                // DATUM
                double dX_Coord,
                double dY_Coord,
                double dZ_Coord,

                // Выходные параметры 
                ref double dLong_Coord   // приращение по долготе, угл.сек

            )
        {

            double N;
            double Lat_Coord_8442_tmp;
            double Long_Coord_8442_tmp;
            double H_Coord_8442_tmp;


            // Initial conditions ****************************************************
            // Initial conditions

            if (fl_first2_Coord == 0)
            {

                // Эллипсоид Красовского
                a1P = 1 / 298.3;         // Сжатие
                e2P = 2 * a1P - a1P * a1P; // Квадрат эксцентриситета

                // Эллипсоид WGS84 (GRS80, эти два эллипсоида сходны по большинству параметров)
                a1W = 1 / 298.257223563; // Сжатие
                e2W = 2 * a1W - a1W * a1W;  // Квадрат эксцентриситета

                // Вспомогательные значения для преобразования эллипсоидов
                a = (aP + aW) / 2;
                e2 = (e2P + e2W) / 2;
                da = aW - aP;
                de2 = e2W - e2P;
                // ...............................................................

                fl_first2_Coord = 1;
                // ...............................................................

            }
            // **************************************************** Initial conditions

            // ...................................................................
            // Перевод в радианы и м

            H_Coord_8442_tmp = H_Coord_8442 * 1000; // km->m

            // grad->rad
            Lat_Coord_8442_tmp = (Lat_Coord_8442 * Math.PI) / 180;
            Long_Coord_8442_tmp = (Long_Coord_8442 * Math.PI) / 180;
            // ...................................................................

            // Радиус кривизны первого вертикала
            N = a / Math.Sqrt(1 - e2 * Math.Sin(Lat_Coord_8442_tmp) * Math.Sin(Lat_Coord_8442_tmp));

            dLong_Coord = ro / ((N + H_Coord_8442_tmp) * Math.Cos(Lat_Coord_8442_tmp)) *
                         (-dX_Coord * Math.Sin(Long_Coord_8442_tmp) + dY_Coord * Math.Cos(Long_Coord_8442_tmp)) +
                        Math.Tan(Lat_Coord_8442_tmp) * (1 - e2) * (wx * Math.Cos(Long_Coord_8442_tmp) + wy * Math.Sin(Long_Coord_8442_tmp)) - wz;


        } // Функция f_dLong
        //************************************************************************

        // ***********************************************************************
        // Расчет приращения по широте при преобразованиях координат WGS84 (эллипсоид)
        // <-> эллипсоид Красовского(Пулково-42)   (преобразования Молоденского) в
        //  угл.сек

        // Входные параметры:
        // Lat_Coord_8442 - широта (град)
        // Long_Coord_8442 - долгота (град)
        // H_Coord_8442 - высота (км)
        // dX_Coord,dY_Coord,dZ_Coord - !!! это д.б.глобальные переменные, в которых
        // хранится DATUM (м) 

        // Выходные параметры:
        // dLat_Coord - приращение по широте, !!! угловые секунды
        // ***********************************************************************
        public void f_dLat
            (

                // Входные параметры (внутри функции переводятся в рад и м)
                double Lat_Coord_8442,   // широта (grad)
                double Long_Coord_8442,  // долгота (grad)
                double H_Coord_8442,     // высота (km)

                // DATUM
                double dX_Coord,
                double dY_Coord,
                double dZ_Coord,

                // Выходные параметры 
                ref double dLat_Coord    // приращение по широте, угл.сек

            )
        {

            double N, M;
            double Lat_Coord_8442_tmp;
            double Long_Coord_8442_tmp;
            double H_Coord_8442_tmp;

            // Initial conditions ****************************************************
            // Initial conditions

            if (fl_first2_Coord == 0)
            {
                // ...................................................................

                // Эллипсоид Красовского
                a1P = 1 / 298.3;                 // Сжатие
                //e2P = 2 * a1P - a1P*a1P;       // Квадрат эксцентриситета
                e2P = 2 * a1P - Math.Pow(a1P, 2); // Квадрат эксцентриситета


                // Эллипсоид WGS84 (GRS80, эти два эллипсоида сходны по большинству параметров)
                a1W = 1 / 298.257223563; // Сжатие
                e2W = 2 * a1W - a1W * a1W;  // Квадрат эксцентриситета

                // Вспомогательные значения для преобразования эллипсоидов
                a = (aP + aW) / 2;
                e2 = (e2P + e2W) / 2;
                da = aW - aP;
                de2 = e2W - e2P;
                // ...............................................................

                fl_first2_Coord = 1;
                // ...............................................................

            }
            // **************************************************** Initial conditions

            // ...................................................................
            // Перевод в радианы и м

            H_Coord_8442_tmp = H_Coord_8442 * 1000; // km->m

            // grad->rad
            Lat_Coord_8442_tmp = (Lat_Coord_8442 * Math.PI) / 180;
            Long_Coord_8442_tmp = (Long_Coord_8442 * Math.PI) / 180;
            // ...................................................................

            // Радиус кривизны первого вертикала
            N = a / Math.Sqrt(1 - e2 * Math.Sin(Lat_Coord_8442_tmp) * Math.Sin(Lat_Coord_8442_tmp));

            // Радиус кривизны меридиана
            M = a * (1 - e2) *
                (1 / (Math.Sqrt(1 - e2 * Math.Sin(Lat_Coord_8442_tmp) * Math.Sin(Lat_Coord_8442_tmp)) *
                    Math.Sqrt(1 - e2 * Math.Sin(Lat_Coord_8442_tmp) * Math.Sin(Lat_Coord_8442_tmp)) *
                    Math.Sqrt(1 - e2 * Math.Sin(Lat_Coord_8442_tmp) * Math.Sin(Lat_Coord_8442_tmp))));

            dLat_Coord = (ro / (M + H_Coord_8442_tmp)) *
                          ((N / a) * e2 * Math.Sin(Lat_Coord_8442_tmp) * Math.Cos(Lat_Coord_8442_tmp) * da +
                          ((N * N) / (a * a) + 1) * N * Math.Sin(Lat_Coord_8442_tmp) * Math.Cos(Lat_Coord_8442_tmp) * (de2 / 2) -
                          dX_Coord * Math.Cos(Long_Coord_8442_tmp) * Math.Sin(Lat_Coord_8442_tmp) -
                          dY_Coord * Math.Sin(Long_Coord_8442_tmp) * Math.Sin(Lat_Coord_8442_tmp) +
                          dZ_Coord * Math.Cos(Lat_Coord_8442_tmp)) -
                          wx * Math.Sin(Long_Coord_8442_tmp) * (1 + e2 * Math.Cos(2 * Lat_Coord_8442_tmp)) +
                          wy * Math.Cos(Long_Coord_8442_tmp) * (1 + e2 * Math.Cos(2 * Lat_Coord_8442_tmp)) -
                          ro * ms * e2 * Math.Sin(Lat_Coord_8442_tmp) * Math.Cos(Lat_Coord_8442_tmp);


        } // Функция f_dLat
        //************************************************************************

        // ***********************************************************************
        // Перевод координат WGS84(эллипсоид) -> эллипсоид Красовского(Пулково-42)

        // Входные параметры:
        // Lat_Coord_8442 - широта (град)
        // Long_Coord_8442 - долгота (град)
        // H_Coord_8442 - высота (км)
        // Lat_Coord - приращение по широте, !!! угловые секунды
        // Long_Coord - приращение по долготе, !!! угловые секунды

        // Выходные параметры:
        // Lat_Coord_Vyx_8442 - широта (град)
        // Long_Coord_Vyx_8442 - долгота (град)

        // ***********************************************************************
        public void f_WGS84_SK42_Lat_Long
               (

                   // Входные параметры (grad,km)
                   double Lat_Coord_8442,   // широта
                   double Long_Coord_8442,  // долгота
                   double H_Coord_8442,     // высота
                   double dLat_Coord,       // приращение по долготе,угл.сек
                   double dLong_Coord,      // приращение по долготе,угл.сек

                   // Выходные параметры (grad)
                   ref double Lat_Coord_Vyx_8442,   // широта
                   ref double Long_Coord_Vyx_8442   // долгота

               )
        {

            // grad
            //Lat_Coord_Vyx_8442 = (Lat_Coord_8442 * 180) / Math.PI - dLat_Coord / 3600; 
            //Long_Coord_Vyx_8442 = (Long_Coord_8442 * 180) / Math.PI - dLong_Coord / 3600;
            Lat_Coord_Vyx_8442 = Lat_Coord_8442 - dLat_Coord / 3600;
            Long_Coord_Vyx_8442 = Long_Coord_8442 - dLong_Coord / 3600;

        } // Функция f_WGS84_SK42_Lat_Long
        //************************************************************************

        // ***********************************************************************
        // Преобразование геодезических координат (широта, долгота, высота) 
        // эллипсоида Красовского (СК42) в плоские прямоугольные координаты в
        // проекции Гаусса-Крюгера

        // Входные параметры:
        // Lat_Coord_Kr - широта (град)
        // Long_Coord_Kr - долгота (град)

        // Выходные параметры:
        // X_Coord_Kr, Y_Coord_Kr - плоские прямоугольные координаты (км)
        // ***********************************************************************
        public void f_SK42_Krug
            (

                // Входные параметры (!!! град)
                double Lat_Coord_Kr,   // широта
                double Long_Coord_Kr,  // долгота
            //double H_Coord_Kr,   // высота (m)

                // Выходные параметры (км)
                ref double X_Coord_Kr,
                ref double Y_Coord_Kr

            )
        {

            int No_tmp;
            double Lo_tmp, Bo_tmp;

            // Вспомогательные величины
            double Xa_tmp, Xb_tmp, Xc_tmp, Xd_tmp;
            double Ya_tmp, Yb_tmp, Yc_tmp;
            double sin2_B, sin4_B, sin6_B;

            // No ********************************************************************
            // Номер шестиградусной зоны в проекци Гаусса-Крюгера (1,2,...)
            // !!! Long - в град

            No_tmp = (int)(6 + Long_Coord_Kr) / 6;

            // ******************************************************************** No

            // Lo ********************************************************************
            // Расстояние от определяемой точки до осевого меридиана зоны, рад

            Lo_tmp = (Long_Coord_Kr - (3 + 6 * (No_tmp - 1))) / 57.29577951;

            // ******************************************************************** Lo

            // Bo ********************************************************************
            // Переводим широту в рад

            Bo_tmp = (Lat_Coord_Kr * Math.PI) / 180;


            sin2_B = Math.Pow(Math.Sin(Bo_tmp), 2);
            sin4_B = Math.Pow(Math.Sin(Bo_tmp), 4);
            sin6_B = Math.Pow(Math.Sin(Bo_tmp), 6);

            // ******************************************************************** Bo

            // X *********************************************************************

            Xa_tmp = Math.Pow(Lo_tmp, 2) *
                     (109500 - 574700 * sin2_B + 863700 * sin4_B -
                      398600 * sin6_B);

            Xb_tmp = Math.Pow(Lo_tmp, 2) *
                     (278194 - 830174 * sin2_B + 572434 * sin4_B -
                      16010 * sin6_B + Xa_tmp);

            Xc_tmp = Math.Pow(Lo_tmp, 2) *
                    (672483.4 - 811219.9 * sin2_B + 5420 * sin4_B -
                     10.6 * sin6_B + Xb_tmp);

            Xd_tmp = Math.Pow(Lo_tmp, 2) *
                    (1594561.25 + 5336.535 * sin2_B + 26.79 * sin4_B +
                    0.149 * sin6_B + Xc_tmp);

            X_Coord_Kr = 6367558.4968 * Bo_tmp -
                         Math.Sin(Bo_tmp * 2) *
                         (16002.89 + 66.9607 * sin2_B + 0.3515 * sin4_B - Xd_tmp);

            // ********************************************************************* X

            // Y *********************************************************************

            Ya_tmp = Math.Pow(Lo_tmp, 2) *
                    (79690 - 866190 * sin2_B + 1730360 * sin4_B -
                     945460 * sin6_B);

            Yb_tmp = Math.Pow(Lo_tmp, 2) *
                    (270806 - 1523417 * sin2_B + 1327645 * sin4_B -
                     21701 * sin6_B + Ya_tmp);

            Yc_tmp = Math.Pow(Lo_tmp, 2) *
                     (1070204.16 - 2136826.66 * sin2_B + 17.98 * sin4_B -
                      11.99 * sin6_B + Yb_tmp);

            Y_Coord_Kr = (5 + 10 * No_tmp) * 100000 +
                         Lo_tmp * Math.Cos(Bo_tmp) *
                         (6378245 + 21346.1415 * sin2_B + 107.159 * sin4_B +
                          0.5977 * sin6_B + Yc_tmp);

            // ********************************************************************* Y

            // .......................................................................
            // перевод в км

            X_Coord_Kr = X_Coord_Kr / 1000;
            Y_Coord_Kr = Y_Coord_Kr / 1000;
            // .......................................................................


        } // Функция f_SK42_Krug
        //************************************************************************

        // ***********************************************************************
        // Отрисовка траектории N фиктивных точек по пеленгу
        //
        // Входные параметры: 
        // Theta_Pel(град) - пеленг,
        // Mmax_Pel(км) - максимальная дальность отображения пеленга,
        // NumberFikt_Pel - количество фиктивных точек
        // LatP_Pel,LongP_Pel (град) - широта и долгота стояния пеленгатора 

        //
        // Выходные параметры: 
        // массив mas_Pel(R(км),Latitude(град),Longitude(град)) ->
        // дальность, широта, долгота
        // массив mas_Pel_XYZ (км) -> координаты в опорной геоцентрической СК
        // XP_Pel,YP_Pel,ZP_Pel (км) -координаты пеленгатора в опорной геоцентрической СК
        // XFN_Pel,YFN_Pel,ZFN_Pel (км) -координаты N-й фиктивной точки в опорной 
        // геоцентрической СК

        // Широта -90(юг)...+90(север)
        // Долгота -180(на запад от Гринвича)...+180(на восток от Гринвича)
        // Пеленг 0...360(по часовой стрелке)
        // ***********************************************************************
        public void f_Peleng
            (

                 // Пеленг
                 double Theta_Pel,
            // Max дальность отображения пеленга
                 double Mmax_Pel,
            // Количество фиктивных точек в плоскости пеленгования пеленгатора
                 uint NumbFikt_Pel,

                 // Широта и долгота стояния пеленгатора
                 double LatP_Pel,
                 double LongP_Pel,

                 // Координаты пеленгатора в опорной геоцентрической СК
                 ref double XP_Pel,
                 ref double YP_Pel,
                 ref double ZP_Pel,

                 // Координаты N-й фиктивной точки в опорной геоцентрической СК
                 ref double XFN_Pel,
                 ref double YFN_Pel,
                 ref double ZFN_Pel,

                 // Координаты фиктивных точек
                 ref double[] mas_Pel,
                 ref double[] mas_Pel_XYZ

                 // ForOtl
            // Time of model
            //double *pT,
            // Discrete of time
            //double *pdt,
            //double *pmas_Otl_Pel        


            )
        {

            // .........................................................................
            //double yyy;
            //int jjj;
            //int lngp_i, lngi_i, lngpi_i;
            //double lngp_d, lngi_d, lngpi_d;


            //jjj = 0;
            //lngp_i = 0;
            //lngp_d = 0;
            //lngi_i = 0;
            //lngi_d = 0;
            //lngpi_i = 0;
            //lngpi_d = 0;
            // .........................................................................


            // Initial conditions ****************************************************
            // Initial conditions

            if (fl_first_Pel == 0)
            {

                // ...............................................................
                // Радиус Земли  в точке стояния пеленгатора с учетом текущей широты 

                // m
                //REarthP_Pel = REarth_Pel*(1.-0.003352*sin(*pLatP_Pel)*sin(*pLatP_Pel));
                REarthP_Pel = REarth_Pel; // Шар
                // ...............................................................

                fl_first_Pel = 1;
                // ...............................................................
                // Широта и долгота стояния пеленгатора

                LatP_Pel_tmp = (LatP_Pel * Math.PI) / 180;   // grad->rad
                LongP_Pel_tmp = (LongP_Pel * Math.PI) / 180;
                // ...............................................................
                // Количество фиктивных точек в плоскости пеленгования пеленгатора

                NumbFikt_Pel_tmp = NumbFikt_Pel;
                // ...............................................................
                // Max дальность отображения пеленга

                Mmax_Pel_tmp = Mmax_Pel * 1000; // km-> m
                // ...............................................................
                // Координаты пеленгатора в опорной геоцентрической СК

                // m
                XP_Pel_tmp = REarthP_Pel * Math.Cos(LatP_Pel_tmp) * Math.Cos(LongP_Pel_tmp);
                YP_Pel_tmp = REarthP_Pel * Math.Cos(LatP_Pel_tmp) * Math.Sin(LongP_Pel_tmp);
                ZP_Pel_tmp = REarthP_Pel * Math.Sin(LatP_Pel_tmp);

                // km
                XP_Pel = XP_Pel_tmp / 1000;
                YP_Pel = YP_Pel_tmp / 1000;
                ZP_Pel = ZP_Pel_tmp / 1000;
                // ...............................................................
                // Пеленг

                // grad -> rad
                Theta_Pel_tmp = (Theta_Pel * Math.PI) / 180;
                // ...............................................................

                // ...............................................................
                // VAR2

                V_Pel = Mmax_Pel_tmp / NumbFikt_Pel;
                Lat_Pel_N1 = LatP_Pel_tmp;
                Long_Pel_N1 = LongP_Pel_tmp;
                fl_var2 = 0;
                fl1_var2 = 0;
                fl2_var2 = 0;
                // ...............................................................

            }
            // **************************************************** Initial conditions

            while (IndMas_Pel < (NumbFikt_Pel))
            {

/*
                
                        // VAR1 *******************************************************************
                        //yyy = Math.Acos((Math.Sin(LATi_Pel) -Math.Sin(LatP_Pel_tmp)*Math.Cos(Alpha))/
                        //(Math.Cos(LatP_Pel_tmp)*Math.Sin(Alpha)));
                        //yyy = Math.Acos(Math.Sin(LatP_Pel)/Math.Cos(LATi_Pel)); 

                        // Mi ********************************************************************
                        // Расстояние по поверхности земли от точки стояния пеленгатора до текущей 
                        // фиктивной точки (Mi-дуга болшьшого круга с центральным углом Mi/Rз радиан)

                        Mi_Pel = (((double)IndFP_Pel) * Mmax_Pel_tmp) / ((double)(NumbFikt_Pel_tmp));

                        // Центральный угол дуги Mi
                        Alpha = Mi_Pel / REarthP_Pel;
                        // ******************************************************************** Mi

                        // ***********************************************************************
                        if (Alpha >= Math.PI / 2)
                        {

                            jjj += 1;

                            // A2
                            yyy = Math.Acos(Math.Sin(LatP_Pel_tmp) / Math.Cos(LATi_Pel)); // Pel=45grad
                            //yyy = Math.Acos(Math.Sin(LATi_Pel) / Math.Cos(LatP_Pel)); // Pel=45grad
                            //yyy = Math.Asin((1 / Math.Tan(Math.PI / 2 - LatP_Pel)) /
                            //(1 / Math.Tan(Theta_Pel_tmp))); // Pel=45grad

                            NumbFikt_Pel_tmp = NumbFikt_Pel_tmp - IndFP_Pel;
                            IndFP_Pel = 1;
                            Mmax_Pel_tmp = Mmax_Pel_tmp - Mi_Pel;
                            LatP_Pel_tmp = LATi_Pel;
                            LongP_Pel_tmp = LONGi_Pel;
                            //Theta_Pel_tmp = Math.PI - Theta_Pel_tmp;
                            Mi_Pel = (((double)IndFP_Pel) * Mmax_Pel_tmp) / ((double)(NumbFikt_Pel_tmp));
                            Alpha = Mi_Pel / REarthP_Pel;

                            //Theta_Pel_tmp = yyy;

                            // Pel<90 and !=0
                            if ((Theta_Pel < Math.PI / 2) && (Theta_Pel != 0))
                                Theta_Pel_tmp = Math.PI - yyy;  // Pel=45grad

                            // Pel=0
                            else if (Theta_Pel == 0)
                            {
                                //if (jjj<2)
                                //    Theta_Pel_tmp = 0;
                                //else
                                //    Theta_Pel_tmp = Math.PI;

                                lngp_i = (int)(LongP_Pel * 1000);
                                lngp_d = ((double)lngp_i)/1000;
                                lngi_i = (int)(LONGi_Pel * 1000);
                                lngi_d = ((double)lngi_i) / 1000;
                                lngpi_i = (int)(Math.PI * 1000);
                                lngpi_d = ((double)lngpi_i) / 1000;


                               if(lngi_d == lngp_d )
                                  Theta_Pel_tmp = 0;
                               else if (lngi_d == (lngp_d+lngpi_d))
                                   Theta_Pel_tmp = Math.PI;
                               else
                                   Theta_Pel_tmp = 0;
                            }

                            // Pel=90
                            else if (Theta_Pel == Math.PI/2)
                            {
                                Theta_Pel_tmp = Math.PI / 2;
                            }

                            //Theta_Pel_tmp = Math.PI + yyy;   // Pel=315grad
                            //else
                            //Theta_Pel_tmp = yyy;    // Pel=135grad
                            //Theta_Pel_tmp = Math.PI / 2 + Theta_Pel_tmp; 


                        } // Alpha >= Math.PI / 2
                        // ***********************************************************************

                        // Di ********************************************************************
                        // Расстояние по прямой от точки стояния пеленгатора до пересечения прямой
                        // (проходящей через текущую фиктивную точку и начало координат)и плоскости,
                        // касательной в точке стояния пеленгатора

                        //Di_Pel = REarthP_Pel * Math.Tan(Alpha);
                        //Di_Pel = REarthP_Pel * module(Math.Tan(Mi_Pel / REarthP_Pel));
                        Di_Pel = REarthP_Pel * module(Math.Tan(Alpha));

                        // ******************************************************************** Di

                        // XiYiZi ****************************************************************
                        // Координаты точки пересечения прямой (проходящей через текущую фиктивную
                        // точку и начало координат) и плоскости касательной в точке стояния 
                        // пеленгатора в СК пеленгатора

                        Xi_Pel = REarthP_Pel;
                        Yi_Pel = Di_Pel * Math.Sin(Theta_Pel_tmp);
                        Zi_Pel = Di_Pel * Math.Cos(Theta_Pel_tmp);

                        // **************************************************************** XiYiZi

                        // Xi0Yi0Zi0 *************************************************************
                        // Координаты точки пересечения прямой (проходящей через текущую фиктивную
                        // точку и начало координат) и плоскости касательной в точке стояния 
                        // пеленгатора в СК1 (Поворот СКП на угол=широте пеленгатора)

                        Xi0_Pel = Xi_Pel * Math.Cos(LatP_Pel_tmp) - Zi_Pel * Math.Sin(LatP_Pel_tmp);
                        Yi0_Pel = Yi_Pel;
                        Zi0_Pel = Xi_Pel * Math.Sin(LatP_Pel_tmp) + Zi_Pel * Math.Cos(LatP_Pel_tmp);

                        // ************************************************************* Xi0Yi0Zi0

                        // XiG1YiG1ZiG1 **********************************************************
                        // Координаты точки пересечения прямой (проходящей через текущую фиктивную
                        // точку и начало координат) и плоскости касательной в точке стояния 
                        // пеленгатора в опорной геоцентрической СК (Поворот СК1 на угол=долготе 
                        // пеленгатора)

                        // !!!рабочий вариант
                        XiG1_Pel = Xi0_Pel * Math.Cos(LongP_Pel_tmp) - Yi0_Pel * Math.Sin(LongP_Pel_tmp);
                        YiG1_Pel = Xi0_Pel * Math.Sin(LongP_Pel_tmp) + Yi0_Pel * Math.Cos(LongP_Pel_tmp);

                        //XiG1_Pel = Xi0_Pel * Math.Cos(LongP_Pel_tmp) + Yi0_Pel * Math.Sin(LongP_Pel_tmp);
                        //YiG1_Pel = -Xi0_Pel * Math.Sin(LongP_Pel_tmp) + Yi0_Pel * Math.Cos(LongP_Pel_tmp);
                        ZiG1_Pel = Zi0_Pel;

                        // ********************************************************** XiG1YiG1ZiG1

                        // RiLATiLONGi ***********************************************************
                        // Угловые координаты фиктивной точки в опорной геоцентрической СК

                        Ri1_Pel = Math.Sqrt(XiG1_Pel * XiG1_Pel + YiG1_Pel * YiG1_Pel + ZiG1_Pel * ZiG1_Pel);

                        LATi_Pel = Math.Asin(ZiG1_Pel / Ri1_Pel);

                        Def_Longitude_180(
                        //Def_Longitude(

                                      XiG1_Pel,
                                      YiG1_Pel,
                                      Ri1_Pel,
                                      LATi_Pel,
                                      ref LONGi_Pel
                                     );


                        XiG_Pel = REarthP_Pel * Math.Cos(LATi_Pel) * Math.Cos(LONGi_Pel);
                        YiG_Pel = REarthP_Pel * Math.Cos(LATi_Pel) * Math.Sin(LONGi_Pel);
                        ZiG_Pel = REarthP_Pel * Math.Sin(LATi_Pel);
                        Ri_Pel = Math.Sqrt(XiG_Pel * XiG_Pel + YiG_Pel * YiG_Pel + ZiG_Pel * ZiG_Pel);

                        // *********************************************************** RiLATiLONGi

                        // ******************************************************************* VAR1
                
*/





                // VAR2 ******************************************************************
                // !!!VAR2

                // ......................................................................
                // LAT

                if ((fl_var2 == 0) && (fl2_var2 == 0))
                    Lat_Pel_N = Lat_Pel_N1 + (V_Pel * Math.Cos(Theta_Pel_tmp)) / REarthP_Pel;

                // Перешли через северный полюс
                else if ((fl_var2 == 1) && (fl2_var2 == 0) && (Math.Cos(Theta_Pel_tmp) >= 0))
                    Lat_Pel_N = Lat_Pel_N1 - (V_Pel * Math.Cos(Theta_Pel_tmp)) / REarthP_Pel;
                else if ((fl_var2 == 1) && (fl2_var2 == 0) && (Math.Cos(Theta_Pel_tmp) < 0))
                    Lat_Pel_N = Lat_Pel_N1 + (V_Pel * Math.Cos(Theta_Pel_tmp)) / REarthP_Pel;

                // Перешли через южный полюс 
                else if ((fl2_var2 == 1) && (fl_var2 == 0) && (Math.Cos(Theta_Pel_tmp) >= 0))
                    Lat_Pel_N = Lat_Pel_N1 + (V_Pel * Math.Cos(Theta_Pel_tmp)) / REarthP_Pel;
                else if ((fl2_var2 == 1) && (fl_var2 == 0) && (Math.Cos(Theta_Pel_tmp) < 0))
                    Lat_Pel_N = Lat_Pel_N1 - (V_Pel * Math.Cos(Theta_Pel_tmp)) / REarthP_Pel;


                // Перешли через северный полюс
                if (
                    (fl_var2 == 0) &&
                    (Lat_Pel_N > 0) &&
                    (Lat_Pel_N >= Math.PI / 2)
                   )
                {
                    Lat_Pel_N = Math.PI / 2;

                    fl_var2 = 1;
                    fl2_var2 = 0;
                    fl1_var2 = 0;

                }

                // Перешли через южный полюс
                if (
                    (fl2_var2 == 0) &&
                    (Lat_Pel_N < 0) &&
                    (Lat_Pel_N <= -Math.PI / 2)
                   )
                {
                    Lat_Pel_N = -Math.PI / 2;
                    //Theta_Pel = 0;

                    fl2_var2 = 1;
                    fl_var2 = 0;
                    fl1_var2 = 0;

                }

                // .......................................................................
                // LONG

                // Перешли через полюс
                if (
                    (fl1_var2 == 0) &&
                    ((fl_var2 == 1) || (fl2_var2 == 1))
                   )
                {
                    if (Long_Pel_N1 >= 0)
                        Long_Pel_N1 = Long_Pel_N1 + Math.PI;
                    else if (Long_Pel_N1 < 0)
                        Long_Pel_N1 = Long_Pel_N1 - Math.PI;

                    if (Long_Pel_N1 > Math.PI)
                        Long_Pel_N1 = -(2 * Math.PI - Long_Pel_N1);
                    else if (Long_Pel_N1 < -Math.PI)
                        Long_Pel_N1 = 2 * Math.PI + Long_Pel_N1;


                    fl1_var2 = 1;
                }


                // Обычный расчет
                Long_Pel_N = Long_Pel_N1 +
                            (V_Pel * Math.Sin(Theta_Pel_tmp)) / (REarthP_Pel * Math.Cos(Lat_Pel_N1));

                if (Long_Pel_N > Math.PI)
                    Long_Pel_N = -(2 * Math.PI - Long_Pel_N);
                else if (Long_Pel_N < -Math.PI)
                    Long_Pel_N = 2 * Math.PI + Long_Pel_N;
                // .......................................................................

                // ........................................................................
                XiG_Pel = REarthP_Pel * Math.Cos(Lat_Pel_N) * Math.Cos(Long_Pel_N);
                YiG_Pel = REarthP_Pel * Math.Cos(Lat_Pel_N) * Math.Sin(Long_Pel_N);
                ZiG_Pel = REarthP_Pel * Math.Sin(Lat_Pel_N);
                Ri_Pel = Math.Sqrt(XiG_Pel * XiG_Pel + YiG_Pel * YiG_Pel + ZiG_Pel * ZiG_Pel);
                // ........................................................................

                Lat_Pel_N1 = Lat_Pel_N;
                Long_Pel_N1 = Long_Pel_N;
                // ****************************************************************** VAR2


                // Массивы ***************************************************************
                // Занесение в массивы

                // Ri,LATi,LONGi (км, град)
                mas_Pel[IndMas_Pel * 3] = Ri_Pel / 1000;

                // VAR1
                //mas_Pel[IndMas_Pel * 3 + 1] = (LATi_Pel*180)/Math.PI;
                //mas_Pel[IndMas_Pel * 3 + 2] = (LONGi_Pel*180)/Math.PI;

                // VAR2
                mas_Pel[IndMas_Pel * 3 + 1] = (Lat_Pel_N * 180) / Math.PI;
                mas_Pel[IndMas_Pel * 3 + 2] = (Long_Pel_N * 180) / Math.PI;


                // XYZ в опорной геоцентрической СК (км)
                mas_Pel_XYZ[IndMas_Pel * 3] = XiG_Pel / 1000;
                mas_Pel_XYZ[IndMas_Pel * 3 + 1] = YiG_Pel / 1000;
                mas_Pel_XYZ[IndMas_Pel * 3 + 2] = ZiG_Pel / 1000;

                // *************************************************************** Массивы

                // Вывод для отладки ****************************************************

                //*(pmas_Otl_Pel+IndMas_Pel) = Di_Pel;

                // **************************************************** Вывод для отладки`

                // ***********************************************************************
                // Переход к следующей фиктивной точке

                IndFP_Pel += 1;
                IndMas_Pel += 1;
                // ***********************************************************************

            }; // WHILE

            // km
            XFN_Pel = XiG_Pel / 1000;
            YFN_Pel = YiG_Pel / 1000;
            ZFN_Pel = ZiG_Pel / 1000;

        } // Функция f_Peleng
        //************************************************************************

        // *************************************************************************
        // Расчет азимута ИРИ относительно СП
        // dY,dX - разность координат между ИРИ и СП на плоскости XOY (dX=Xири-Xсп...)
        // Условно считаем Y - направление на север

        // Возврат: азимут в градусах (0...360 по часовой стрелке)
        // *************************************************************************

        public double f_Def_Azimuth(
                                   double dY,
                                   double dX
                                      )
        {
            double Beta, Beta_tmp;

            Beta = 0;
            Beta_tmp = 0;
            // ------------------------------------------------------------------------
            if (dY != 0)
                Beta_tmp = Math.Atan(module(dX) / module(dY));
            // -------------------------------------------------------------------------
            if ((dX == 0) && (dY >= 0))
                Beta = 0;
            // -------------------------------------------------------------------------
            else if ((dX == 0) && (dY < 0))
                Beta = Math.PI;
            // ------------------------------------------------------------------------
            else if ((dY == 0) && (dX > 0))
                Beta = Math.PI / 2;
            // -------------------------------------------------------------------------
            else if ((dY == 0) && (dX < 0))
                Beta = 3 * Math.PI / 2;
            // -------------------------------------------------------------------------
            else if ((dY > 0) && (dX > 0))
                Beta = Beta_tmp;
            // -------------------------------------------------------------------------
            else if ((dY > 0) && (dX < 0))
                Beta = 2 * Math.PI - Beta_tmp;
            // -------------------------------------------------------------------------
            else if ((dY < 0) && (dX > 0))
                Beta = Math.PI - Beta_tmp;
            // -------------------------------------------------------------------------
            else if ((dY < 0) && (dX < 0))
                Beta = Math.PI + Beta_tmp;
            // -------------------------------------------------------------------------
            // Перевод в градусы

            Beta = (Beta * 180) / Math.PI;
            // -------------------------------------------------------------------------

            return Beta;

        } // P/P f_Def_Azimuth
        // *************************************************************************

        // ***********************************************************************
        // Расчет долготы
        // [0...180] к востоку от Гринвича
        // ]0...-180[ к западу от Гринвича
        // Широта -90(юг)...+90(север)
        // ***********************************************************************

        public void Def_Longitude_180(
                            double X,
                            double Y,
                            double R,
                            double Lat, // rad
                            ref double Long

                                  )
        {
            double Long1;

            // -------------------------------------------------------------------
            if (module(R * Math.Cos(Lat)) > 1E-10)
                Long1 = Math.Asin(module(Y) / (R * Math.Cos(Lat)));
            else
                Long1 = Math.Asin(module(Y) / 1E-10);

            // -------------------------------------------------------------------
            if ((Y == 0) && (X > 0))
                Long = 0;
            // -------------------------------------------------------------------
            else if ((Y == 0) && (X < 0))
                Long = Math.PI;
            // -------------------------------------------------------------------
            else if ((Y > 0) && (X > 0))
                Long = Long1;
            // -------------------------------------------------------------------
            else if ((Y < 0) && (X > 0))
                Long = -Long1;
            // -------------------------------------------------------------------
            else if ((Y > 0) && (X < 0))
                Long = Math.PI - Long1;
            // ------------------------------------------------------------------
            else // X<0 Y<0
                Long = -(Math.PI - Long1);
            // -------------------------------------------------------------------

        } // Функция Def_Longitude_180
        // ***********************************************************************

        // ***********************************************************************
        // Расчет долготы
        // 0...360 к востоку от Гринвича
        // ***********************************************************************

        public void Def_Longitude(
                            double X,
                            double Y,
                            double R,
                            double Lat,
                            ref double Long

                                  )
        {
            double Long1;

            // -------------------------------------------------------------------
            if (module(R * Math.Cos(Lat)) > 1E-10)
                Long1 = Math.Asin(module(Y) / (R * Math.Cos(Lat)));
            else
                Long1 = Math.Asin(module(Y) / 1E-10);

            // -------------------------------------------------------------------
            if ((Y == 0) && (X > 0))
                Long = 0;
            // -------------------------------------------------------------------
            else if ((Y == 0) && (X < 0))
                Long = Math.PI;
            // -------------------------------------------------------------------
            else if ((Y > 0) && (X > 0))
                Long = Long1;
            // -------------------------------------------------------------------
            else if ((Y < 0) && (X > 0))
                Long = 2 * Math.PI - Long1;
            // -------------------------------------------------------------------
            else if ((Y > 0) && (X < 0))
                Long = Math.PI - Long1;
            // ------------------------------------------------------------------
            else // X<0 Y<0
                Long = Math.PI + Long1;
            // -------------------------------------------------------------------

        } // Функция Def_Longitude
        // ***********************************************************************

        // ***********************************************************************
        // Расчет сектора
        // Beta1 - азимут начальной точки (grad)
        // 0...360 по часовой стрелке
        // ***********************************************************************

        public double f_Sect(
                            double Beta1,
                            double Beta2
                                  )
        {
            double Sect;
            Sect = 0;
            // -------------------------------------------------------------------
            if (Beta1 == Beta2)
                Sect = 0;
            // -------------------------------------------------------------------
            else if (Beta1==0)
                Sect=Beta2;
            // -------------------------------------------------------------------
            else if (Beta2<Beta1)
                Sect=360-(Beta1-Beta2);
            // -------------------------------------------------------------------
            else // Beta2>Beta1
                Sect=Beta2-Beta1;
            // -------------------------------------------------------------------

            return Sect;

        } // Функция Def_Longitude
        // ***********************************************************************

        // ОТРИСОВКА DRAWDRAWDRAWDRAWDRAWDRAWDRAWDRAWDRAWDRAWDRAWDRAWDRAWDRAWDRAWD


        // ***********************************************************************
        // Удалить все самолеты с этим номером
        // ***********************************************************************
        public static void f_DelAirPlane(
                                  String sNum_air
                                       )
        {
            int iij = 0;

            // FOR1: Убрать все с этим номером
            for (iij = (GlobalVarLn.Number_air - 1); iij >= 0; iij--)
            {

                if (String.Compare(GlobalVarLn.list_air[iij].sNum, sNum_air) == 0)
                {
                    GlobalVarLn.list_air.Remove(GlobalVarLn.list_air[iij]);
                    GlobalVarLn.Number_air -= 1;
                }
            } // FOR1

            if (GlobalVarLn.Number_air==0) // Самолетов не осталось
            {
                GlobalVarLn.fl_AirPlane = 0;
            }

            // Перерисовать карту
            //GlobalVarLn.axMapScreenGlobal.Repaint();


        } // Функция f_DelAirPlane
        // ***********************************************************************

        // ***********************************************************************
        // Удалить все самолеты с этим номером
        // ***********************************************************************
        public static void f_DelAirPlaneAll()
        {
            int iij = 0;

            if (GlobalVarLn.list_air.Count != 0)
            {
                // FOR1: Убрать все 
                for (iij = (GlobalVarLn.Number_air - 1); iij >= 0; iij--)
                {

                        GlobalVarLn.list_air.Remove(GlobalVarLn.list_air[iij]);
                        GlobalVarLn.Number_air -= 1;

                } // FOR1

                GlobalVarLn.Number_air = 0;
                GlobalVarLn.fl_AirPlane = 0;
                // Перерисовать карту
                //GlobalVarLn.axMapScreenGlobal.Repaint();

            } // IF GlobalVarLn.list_air.Count != 0


        } // Функция f_DelAirPlaneAll
        // ***********************************************************************

        // *************************************************************************
        // Расчет азимута ИРИ относительно СП
        // dY,dX - разность координат между ИРИ и СП на плоскости XOY (dX=Xири-Xсп...)
        // Условно считаем Y - направление на север

        // Возврат: азимут в градусах (0...360 по часовой стрелке)
        // *************************************************************************

        public static double f_Def_Azimuth1(
                                   double dY,
                                   double dX
                                      )
        {
            double Beta, Beta_tmp;

            Beta = 0;
            Beta_tmp = 0;
            // ------------------------------------------------------------------------
            if (dY != 0) 
                Beta_tmp = Math.Atan(module1(dX) / module1(dY));

            // -------------------------------------------------------------------------
            if ((dX == 0) && (dY >= 0))
                Beta = 0;
            // -------------------------------------------------------------------------
            else if ((dX == 0) && (dY < 0))
                Beta = Math.PI;
            // ------------------------------------------------------------------------
            else if ((dY == 0) && (dX > 0))
                Beta = Math.PI / 2;
            // -------------------------------------------------------------------------
            else if ((dY == 0) && (dX < 0))
                Beta = 3 * Math.PI / 2;
            // -------------------------------------------------------------------------
            else if ((dY > 0) && (dX > 0))
                Beta = Beta_tmp;
            // -------------------------------------------------------------------------
            else if ((dY > 0) && (dX < 0))
                Beta = 2 * Math.PI - Beta_tmp;
            // -------------------------------------------------------------------------
            else if ((dY < 0) && (dX > 0))
                Beta = Math.PI - Beta_tmp;
            // -------------------------------------------------------------------------
            else if ((dY < 0) && (dX < 0))
                Beta = Math.PI + Beta_tmp;
            // -------------------------------------------------------------------------
            // Перевод в градусы

            Beta = (Beta * 180) / Math.PI;
            // -------------------------------------------------------------------------

            return Beta;

        } // P/P f_Def_Azimuth
        // *************************************************************************

        // *************************************************************************************
        // Module
        // *************************************************************************************
        public static double module1(double a)
        {
            if (a >= 0) return (a);
            else return (-a);

        } // P/P module
        // *************************************************************************************

        // DRAWDRAWDRAWDRAWDRAWDRAWDRAWDRAWDRAWDRAWDRAWDRAWDRAWDRAWDRAWD ОТРИСОВКА



    } // class ClassMap

} // namespace MapApplication
